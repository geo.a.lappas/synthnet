from tkinter.simpledialog import askstring

import os
import json
import tkinter as tk
import subprocess
import webbrowser

# TODO make 5-fold CV a choice button
# Also all inputDir and targetDir + validInputDir should be chosen by clicking
# Method, loss, should be chosen by list
# All the rest should not be visible to the user

def get_keys_from_dict():
    # Define the keys that the user can edit
    editable_keys = [
        "inputDir",
        "targetDir",
        "method",
        "loss",
        "modelDir",
        "nbrEpochs",
        "model",
        "datasetName",
        "maxSize",
        "validInputDir",
        "validTargetDir",
        "cv",
        "foldLst",
        "retrain"
    ]

    return editable_keys

def create_text_widget(app):
    # Define the custom font
    text = tk.Text(app, bg='white')
    text.pack(fill='both', expand=True, padx=20, pady=20)

    return text

# Function to open a JSON file with an argument
def open_json_file(text, editable_keys):
    file_path = os.path.join(os.getcwd(), "config_train.json")

    if file_path:
        with open(file_path, 'r') as file:
            data = json.load(file)

            text.delete('1.0', tk.END)  # Clear the text widget
            for key in editable_keys:
                if key in data:
                    text.insert(tk.END, f"{key}: {data[key]}\n")

# Function to close the current window and open a new one
def close_and_open_new_window(text, app, editable_keys):
    file_path = os.path.join(os.getcwd(), "config_train.json")

    if file_path:
        with open(file_path, 'r') as existing_file:
            existing_data = json.load(existing_file)

            # Update the JSON data with the edited values
            edited_text = text.get("1.0", tk.END)
            edited_data = {}
            for line in edited_text.strip().split('\n'):
                key, value = line.split(': ', 1)
                edited_data[key] = value

            for key in editable_keys:
                if key in edited_data:
                    existing_data[key] = edited_data[key]

            # Save the updated JSON data
            with open(file_path, 'w') as file:
                json.dump(existing_data, file, indent=4)

    print('JSON file config saved')

    app.destroy()  # Close the current window

    # Calculate the width based on the length of the title
    title = "One step away!"
    window_width = len(title) * 30  # Adjust the multiplier as needed

    # Create a new main application window
    new_app = tk.Tk()
    new_app.title(title)
    new_app.geometry(f"{window_width}x200")  # Set the window size
    # Set the background color of the GUI to black
    new_app.configure(bg='black')

    # Create a label and a button in the new window
    label = tk.Label(new_app, text="Select mode", fg='white', bg='black')
    label.pack()

    # Create the "Train Model" button
    train_button = tk.Button(new_app, text="Train Model", command=lambda: train_model(label))
    train_button.pack(fill='both', expand=True, padx=20, pady=20)  # Expand to fill available space

    # Create the "Test Model" button
    test_button = tk.Button(new_app, text="Test Model", command=lambda: test_model(label))
    test_button.pack(fill='both', expand=True, padx=20, pady=20)  # Expand to fill available space

    # Create the "Tensorboard plot" button
    plot_button = tk.Button(new_app, text="Plot tensorboard", command=lambda: plot_tensorboard(label))
    plot_button.pack(fill='both', expand=True, padx=20, pady=20)  # Expand to fill available space

    # Configure button positions to be relative to the window size
    new_app.grid_columnconfigure(0, weight=1)
    new_app.grid_rowconfigure(0, weight=1)

    # Start the main event loop
    new_app.mainloop()

def plot_tensorboard(label):
    label.config(text="Tensorboard was selected")

    # Prompt the user for the path using a pop-up window
    path = askstring("Enter Tensorboard Log Directory", "Enter the path to the Tensorboard log directory:")

    if path is None:
        label.config(text="Operation canceled")
        return

    try:
        # Run your model training script using subprocess - modelLog is the path to the log directory
        subprocess.run(["tensorboard", '--logdir', path])
        # Open the Tensorboard web interface in Firefox
        firefox_path = "/usr/bin/firefox"  # Adjust this path to your Firefox executable
        url = "http://localhost:6006/"  # Replace with your Tensorboard URL
        subprocess.run([firefox_path, url])
    except Exception as e:
        label.config(text="Tensorboard plot failed: " + str(e))

# Function for model training
def train_model(label):
    label.config(text="Model training was selected")

    try:
        # Run your model training script using subprocess
        subprocess.run(["python", os.path.join(os.getcwd(), "actions/train_3D_Synthesis.py")])
    except Exception as e:
        label.config(text="Model Training Failed: " + str(e))

def test_model(label):
    label.config(text="Model inference was selected")

    try:
        # Run your model inference script using subprocess
        subprocess.run(["python", os.path.join(os.getcwd(), "actions/test_3D_Synthesis.py")])
    except Exception as e:
        label.config(text="Model Testing Failed: " + str(e))

def call_GUI():
    # Create the main application window
    app = tk.Tk()
    app.title("Welcome to ActiveMSNet")

    # Set the background color of the GUI to black
    app.configure(bg='black')

    # Create a label widget with a white foreground color
    label = tk.Label(app, text="Adjust JSON config", fg='white', bg='black')
    label.pack()
    label.pack(pady=20)  # Add padding to the label

    # Get the keys you want the use to be able to edit
    json_dict = get_keys_from_dict()

    # 1st window - adjust JSON config
    text = create_text_widget(app)
    open_json_file(text, json_dict)

    # 2nd window - train/test the model
    open_new_window_button = tk.Button(app, text="Save JSON file & select mode", command=lambda: close_and_open_new_window(text, app, json_dict))
    open_new_window_button.pack()

    app.mainloop()

if __name__ == "__main__":
    call_GUI()
