from numba import cuda
from helpers.getDatasetSize import call_find_min_max_indices
from models.modelPrep import *
from helpers.checkNumber import *

import os
import warnings

os.environ["CUDA_VISIBLE_DEVICES"] = "0, 1"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
warnings.filterwarnings("ignore")

import tensorflow as tf

devices = tf.config.list_physical_devices('GPU')

for itr in range(len(devices)):
    tf.config.experimental.set_memory_growth(devices[itr], True)

device = cuda.get_current_device()

def mainTrain(gpuLst=devices):
    # Load the configuration from the JSON file in the current directory
    config_file_path = os.path.join(os.getcwd(), 'config_train.json')
    with open(config_file_path, 'r') as f:
        config = json.load(f)

    # Derive values for 'modelDir'
    modelDir = config.get('modelDir', {})
    model = config.get('model', {})
    datasetName = config.get('datasetName', {})
    method = config.get('method', {})
    loss = config.get('loss', {})

    model_dir_template = os.path.join(modelDir, model, datasetName, method, loss)

    # Extract the datagenParams and netParams dictionaries from the config
    datagenParams = config.get('datagenParams', {})
    datagenParamsValid = config.get('datagenParamsValid', {})
    modelParams = config.get('modelParams', {})

    # select the method
    if method == 'image':
        datagenParams['patch'] = False
    elif method == 'patch':
        datagenParams['patch'] = True
        # datagenParams['patchSize'] = 32

    # select the method
    if method == 'image':
        datagenParamsValid['patch'] = False
    elif method == 'patch':
        datagenParamsValid['patch'] = True
        # datagenParamsValid['patchSize'] = 32

    # adjust config params for CV
    if config.get('cv', {}) == False:
        config.pop('foldLst', None)

    # update the datagenParams keys
    # 1. find the max size per dimension and select that as template
    maxSize = config.get('maxSize', {})
    inputLst = config.get('inputDir', {})
    targetLst = config.get('targetDir', {})

    if method == 'patch':
        # we need to do that for both training and valid datasets
        datagenParams['batchSize'] = find_largest_divisor_with_remainder(len(os.listdir(inputLst[0])))  # this is fixed
    else:
        datagenParams['batchSize'] = 6 # max batch is currently supported

    if config.get('validInputDir', {}):
        if method == 'patch':
            inputValidLst = config.get('validInputDir', {})
            datagenParamsValid['batchSize'] = find_largest_divisor_with_remainder(len(os.listdir(inputValidLst[0]))) # this is fixed
        else:
            datagenParamsValid['batchSize'] = 1
    else:
        datagenParamsValid['batchSize'] = 6 # max batch is currently supported

    print('Searching for the sizes of the input data to optimize training')

    if maxSize == 0:
        # runs only for the first time
        for itrInput in range(len(inputLst)):
            _, maxIndex = call_find_min_max_indices(inputLst[itrInput])

            if maxIndex >= maxSize:
                maxSize = maxIndex

        for itrInput in range(len(targetLst)):
            _, maxIndex = call_find_min_max_indices(targetLst[itrInput])

            if maxIndex >= maxSize:
                maxSize = maxIndex

        # in case external dataset is provided
        if config.get('validInputDir', {}):
            inputValidLst = config.get('validInputDir', {})

            for itrInput in range(len(inputValidLst)):
                _, maxIndex = call_find_min_max_indices(inputValidLst[itrInput])

                if maxIndex >= maxSize:
                    maxSize = maxIndex

        if config.get('validTargetDir', {}):
            targetValidLst = config.get('validTargetDir', {})

            for itrInput in range(len(targetValidLst)):
                _, maxIndex = call_find_min_max_indices(targetValidLst[itrInput])

                if maxIndex >= maxSize:
                    maxSize = maxIndex

        # update params
        if maxSize % 2 != 0:
            maxSize += 1

        config['maxSize'] = times_is_divisible(maxSize, modelParams['num_layers'])

        with open(config_file_path, 'w') as json_file:
            json.dump(config, json_file, indent=4)

    datagenParams['dim'] = tuple([maxSize] * 3)
    datagenParams['nbrInChannel'] = len(inputLst)
    datagenParams['nbrOutChannel'] = len(targetLst)

    datagenParamsValid['dim'] = tuple([maxSize] * 3)
    datagenParamsValid['nbrInChannel'] = len(inputLst)
    datagenParamsValid['nbrOutChannel'] = len(targetLst)

    if method == 'image':
        modelParams['input_shape'] = (*datagenParams['dim'], datagenParams['nbrInChannel'])
    elif method == 'patch':
        modelParams['input_shape'] = (*tuple([datagenParams['patchSize']] * 3), datagenParams['nbrInChannel'])

    # getting info for externalDataset
    if config['validInputDir'] and not config['validTargetDir'] or not config['validInputDir'] and config['validTargetDir']:
        raise AssertionError('When validInputDir or validTargetDir is set, the other cannot be empty')

    if config['validInputDir'] and config['validTargetDir'] and config['cv']:
        raise AssertionError('validInputDir/validTargetDir and cv parameters cannot be enabled at the same time. Set externalDataset True for center-validation or cv True for 5-fold CV')

    with open(config_file_path, 'w') as json_file:
        json.dump(config, json_file, indent=4)

    config['modelDir'] = model_dir_template.format(**config)

    # Before start the ML flow remove the datagenParams and netParams from the config
    config.pop('maxSize', None)
    config.pop('datagenParams', None)
    config.pop('datagenParamsValid', None)
    config.pop('modelParams', None)
    config.pop('method', None)
    config.pop('datasetName', None)

    # Call the function with the configuration dictionary - train or retrain available
    if config.get('retrain'):
        # this is transfer learning but what about fine-tuning?
        print('Retrain the model')

        config.pop('retrain', None)
        continueTrainModel3D(datagenParams=datagenParams, datagenParamsValid=datagenParamsValid, modelParams=modelParams, gpuLst=gpuLst, **config)
    else:
        config.pop('retrain', None)
        trainModel3D(datagenParams=datagenParams, datagenParamsValid=datagenParamsValid, modelParams=modelParams, gpuLst=gpuLst, **config)

if __name__ == "__main__":
    # TODO - 1.hyper-param search, and 2. save in json (bestModel)
    mainTrain()