import os
from numba import cuda
from actions.train_3D_Synthesis import mainTrain
from actions.test_3D_Synthesis import mainTest

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import warnings
warnings.filterwarnings("ignore")

import tensorflow as tf

devices = tf.config.list_physical_devices('GPU')
for itr in range(len(devices)):
    tf.config.experimental.set_memory_growth(devices[itr], True)

device = cuda.get_current_device()

if __name__ == "__main__":
    # 0. adjust config_train and config_test json files
    # TODO performs hyperparam search
    # 1. train or retrain your model
    mainTrain()
    # 2. validate your model
    mainTest()
    # TODO get quantitative metrics
