import tensorflow as tf
import keras.backend as K

epsilon = 1e-5
smooth = 1

def dsc(y_true, y_pred):
    smooth = 1.
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    score = (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
    return score


def dice_loss(y_true, y_pred):
    loss = 1 - dsc(y_true, y_pred)
    return loss


def bce_dice_loss(y_true, y_pred):
    loss = tf.keras.losses.binary_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)
    return loss


def confusion(y_true, y_pred):
    smooth = 1
    y_pred_pos = K.clip(y_pred, 0, 1)
    y_pred_neg = 1 - y_pred_pos
    y_pos = K.clip(y_true, 0, 1)
    y_neg = 1 - y_pos
    tp = K.sum(y_pos * y_pred_pos)
    fp = K.sum(y_neg * y_pred_pos)
    fn = K.sum(y_pos * y_pred_neg)
    prec = (tp + smooth) / (tp + fp + smooth)
    recall = (tp + smooth) / (tp + fn + smooth)
    return prec, recall


def tp(y_true, y_pred):
    smooth = 1
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))
    y_pos = K.round(K.clip(y_true, 0, 1))
    tp = (K.sum(y_pos * y_pred_pos) + smooth) / (K.sum(y_pos) + smooth)
    return tp


def tn(y_true, y_pred):
    smooth = 1
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))
    y_pred_neg = 1 - y_pred_pos
    y_pos = K.round(K.clip(y_true, 0, 1))
    y_neg = 1 - y_pos
    tn = (K.sum(y_neg * y_pred_neg) + smooth) / (K.sum(y_neg) + smooth)
    return tn


def tversky(y_true, y_pred):
    y_true_pos = K.flatten(y_true)
    y_pred_pos = K.flatten(y_pred)
    true_pos = K.sum(y_true_pos * y_pred_pos)
    false_neg = K.sum(y_true_pos * (1 - y_pred_pos))
    false_pos = K.sum((1 - y_true_pos) * y_pred_pos)
    alpha = 0.7
    return (true_pos + smooth) / (true_pos + alpha * false_neg + (1 - alpha) * false_pos + smooth)


def tversky_loss(y_true, y_pred):
    return 1 - tversky(y_true, y_pred)


def focal_tversky(y_true, y_pred):
    pt_1 = tversky(y_true, y_pred)
    gamma = 0.75
    return K.pow((1 - pt_1), gamma)


# Computing Precision
def precision(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


# Computing Sensitivity
def sensitivity(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    return true_positives / (possible_positives + K.epsilon())


# Computing Specificity
def specificity(y_true, y_pred):
    true_negatives = K.sum(K.round(K.clip((1 - y_true) * (1 - y_pred), 0, 1)))
    possible_negatives = K.sum(K.round(K.clip(1 - y_true, 0, 1)))
    return true_negatives / (possible_negatives + K.epsilon())


# Define the PSNR loss function
def psnr_loss(y_true, y_pred):
    psnr = tf.image.psnr(y_true, y_pred, max_val=1.0)
    psnr_norm = psnr / tf.norm(psnr)

    return psnr_norm

def SSIM(y_true, y_pred):
    SSIM_loss = tf.reduce_mean(tf.image.ssim(y_true, y_pred, max_val=1.0))

    return 1 - SSIM_loss

def SSIM_att(y_true, y_pred):
    SSIM_global_loss = 1 - (tf.reduce_mean(tf.image.ssim(y_true, y_pred, max_val=1.0)))

    # Get binary mask of low and high signal regions
    highSignal_indices = tf.where(tf.greater(y_true, 0.7))
    lowSignal_indices = tf.where(tf.less(y_true, 0.3))

    # Gather the elements at the specified indices
    selectedHighValues_y_true = tf.gather_nd(y_true, highSignal_indices)
    selectedHighValues_y_pred = tf.gather_nd(y_pred, highSignal_indices)
    selectedLowValues_y_true = tf.gather_nd(y_true, lowSignal_indices)
    selectedLowValues_y_pred = tf.gather_nd(y_pred, lowSignal_indices)

    # Create a placeholder tensor with the same shape as y_true
    output_shape = tf.shape(y_true)
    output_y_true = tf.zeros(output_shape, dtype=y_true.dtype)
    output_y_pred = tf.zeros(output_shape, dtype=y_pred.dtype)

    # Scatter the gathered values into the output tensor using indices
    outputHighSignal_y_true_reshaped = tf.tensor_scatter_nd_update(output_y_true, highSignal_indices,
                                                                   selectedHighValues_y_true)
    outputHighSignal_y_pred_reshaped = tf.tensor_scatter_nd_update(output_y_pred, highSignal_indices,
                                                                   selectedHighValues_y_pred)
    outputLowSignal_y_true_reshaped = tf.tensor_scatter_nd_update(output_y_true, lowSignal_indices,
                                                                  selectedLowValues_y_true)
    outputLowSignal_y_pred_reshaped = tf.tensor_scatter_nd_update(output_y_pred, lowSignal_indices,
                                                                  selectedLowValues_y_pred)

    # SSIM hyper- and hypo-intensities attention
    loss_ssim_highSignal = SSIM(outputHighSignal_y_true_reshaped, outputHighSignal_y_pred_reshaped)
    loss_ssim_lowSignal = SSIM(outputLowSignal_y_true_reshaped, outputLowSignal_y_pred_reshaped)

    # loss_l1 averaged between hyper- and hypo- intensities and equal contribution of both SSIM and L1
    SSIM_local_loss = ((loss_ssim_highSignal + loss_ssim_lowSignal) / 2)

    total_loss = (.8 * SSIM_global_loss) + (.2 * SSIM_local_loss)

    return total_loss


def MAE(y_true, y_pred):
    mae = tf.keras.losses.MeanAbsoluteError(reduction=tf.keras.losses.Reduction.NONE)
    loss_l1 = mae(y_true, y_pred)

    return loss_l1


def MSE(y_true, y_pred):
    mse = tf.keras.losses.MeanSquaredError(reduction=tf.keras.losses.Reduction.NONE)
    loss_l2 = mse(y_true, y_pred)

    return loss_l2


def SSIM_MAE(y_true, y_pred):
    scaling_l1 = .8  # was .9
    scaling_SSIM = .2  #

    # MAE
    mae = tf.keras.losses.MeanAbsoluteError(reduction=tf.keras.losses.Reduction.NONE)
    loss_l1 = mae(y_true, y_pred)

    # SSIM
    loss_ssim = SSIM(y_true, y_pred)

    # # stochastic loss - define the range for random integer generation
    # lowT = 0  # Lower bound (inclusive)
    # highT = 10  # Upper bound (exclusive) - nbr of patches
    #
    # if highT < 10:
    #     nbrAffected = 5
    # else:
    #     nbrAffected = 10

    # # Generate a random integer between low and high - fixed number of patches to apply loca loss
    # randomRange = list(np.random.choice(np.arange(lowT, highT), size=nbrAffected, replace=False))
    #
    # # convert the list of patch indices to a TF constant
    # randomRange = tf.constant(randomRange)
    #
    # # get the corresponding patches
    # y_true_local = tf.gather(y_true, randomRange)
    # y_pred_local = tf.gather(y_pred, randomRange)
    #
    # # local loss based on a number of patches
    # loss_l1_local = mae(y_true_local, y_pred_local)

    # total loss
    total_loss = (scaling_l1 * loss_l1) + (scaling_SSIM * loss_ssim)

    return total_loss


def getCertainRegions(curTensor, mode: str = 'less', valueMin: float = .3, valueMax: float = None):
    # Create a placeholder tensor with the same shape as y_true
    outputTensor = tf.zeros(tf.shape(curTensor), dtype=curTensor.dtype)

    # Get binary mask of the selected signal regions
    if mode == 'less':
        signalIndices = tf.where(tf.less_equal(curTensor, valueMin))
    elif mode == 'greater':
        signalIndices = tf.where(tf.greater_equal(curTensor, valueMin))

    elif mode == 'between' and valueMax is not None:
        # Get binary mask of the selected signal regions
        signalIndices = tf.where(tf.greater(curTensor, valueMin) & tf.less(curTensor, valueMax))

    # Gather the elements at the specified indices
    selectedValuesTensor = tf.gather_nd(curTensor, signalIndices)

    # Scatter the gathered values into the output tensor using indices
    outputTensor = tf.tensor_scatter_nd_update(outputTensor, signalIndices, selectedValuesTensor)

    return outputTensor


def SSIM_MAE_att2(y_true, y_pred, scaling_l1_global: float = .7, scaling_l1_local: float = .7,
                  scaling_local_loss: float = .8):
    """
    Parameters
    ----------
    scaling_l1_local : object
    """
    # create the local loss for hyper- and hypo-intensities attention
    lowSignalRegions_y_true = getCertainRegions(curTensor=y_true, mode='less', valueMin=.3, valueMax=None)
    lowSignalRegions_y_pred = getCertainRegions(curTensor=y_pred, mode='less', valueMin=.3, valueMax=None)

    highSignalRegions_y_true = getCertainRegions(curTensor=y_true, mode='greater', valueMin=.7, valueMax=None)
    highSignalRegions_y_pred = getCertainRegions(curTensor=y_pred, mode='greater', valueMin=.7, valueMax=None)

    mae = tf.keras.losses.MeanAbsoluteError(reduction=tf.keras.losses.Reduction.NONE)
    local_loss_low_signal = scaling_l1_local * mae(lowSignalRegions_y_true, lowSignalRegions_y_pred) + (
                1 - scaling_local_loss) * SSIM(lowSignalRegions_y_true, lowSignalRegions_y_pred)
    local_loss_high_signal = scaling_l1_local * mae(highSignalRegions_y_true, highSignalRegions_y_pred) + (
                1 - scaling_local_loss) * SSIM(highSignalRegions_y_true, highSignalRegions_y_pred)
    local_loss = (local_loss_low_signal + local_loss_high_signal) / 2

    # create the global loss for the rest regions
    backgroundRegions_y_true = getCertainRegions(curTensor=y_true, mode='between', valueMin=.3, valueMax=.7)
    backgroundRegions_y_pred = getCertainRegions(curTensor=y_pred, mode='between', valueMin=.3, valueMax=.7)
    global_loss = scaling_l1_global * mae(backgroundRegions_y_true, backgroundRegions_y_pred) + (
                1 - scaling_l1_global) * SSIM(backgroundRegions_y_true, backgroundRegions_y_pred)

    # sum them up
    total_loss = (scaling_local_loss * local_loss) + (1 - scaling_local_loss) * global_loss

    return total_loss


def SSIM_MAE_att(y_true, y_pred):
    scaling_l1_global = .7
    scaling_SSIM_global = .3

    # MAE global
    mae = tf.keras.losses.MeanAbsoluteError(reduction=tf.keras.losses.Reduction.NONE)
    global_l1_loss = mae(y_true, y_pred)

    # SSIM global
    loss_ssim = SSIM(y_true, y_pred)

    # global loss
    global_loss = (scaling_l1_global * global_l1_loss) + (scaling_SSIM_global * loss_ssim)

    # Get binary mask of low and high signal regions
    highSignal_indices = tf.where(tf.greater(y_true, .7))
    lowSignal_indices = tf.where(tf.less(y_true, .3))

    # Gather the elements at the specified indices
    selectedHighValues_y_true = tf.gather_nd(y_true, highSignal_indices)
    selectedHighValues_y_pred = tf.gather_nd(y_pred, highSignal_indices)
    selectedLowValues_y_true = tf.gather_nd(y_true, lowSignal_indices)
    selectedLowValues_y_pred = tf.gather_nd(y_pred, lowSignal_indices)

    # Create a placeholder tensor with the same shape as y_true
    output_shape = tf.shape(y_true)
    output_y_true = tf.zeros(output_shape, dtype=y_true.dtype)
    output_y_pred = tf.zeros(output_shape, dtype=y_pred.dtype)

    # Scatter the gathered values into the output tensor using indices
    outputHighSignal_y_true_reshaped = tf.tensor_scatter_nd_update(output_y_true, highSignal_indices,
                                                                   selectedHighValues_y_true)
    outputHighSignal_y_pred_reshaped = tf.tensor_scatter_nd_update(output_y_pred, highSignal_indices,
                                                                   selectedHighValues_y_pred)
    outputLowSignal_y_true_reshaped = tf.tensor_scatter_nd_update(output_y_true, lowSignal_indices,
                                                                  selectedLowValues_y_true)
    outputLowSignal_y_pred_reshaped = tf.tensor_scatter_nd_update(output_y_pred, lowSignal_indices,
                                                                  selectedLowValues_y_pred)

    # MAE hyper- and hypo-intensities attention
    loss_l1_highSignal = mae(outputHighSignal_y_true_reshaped, outputHighSignal_y_pred_reshaped)
    loss_l1_lowSignal = mae(outputLowSignal_y_true_reshaped, outputLowSignal_y_pred_reshaped)

    # SSIM hyper- and hypo-intensities attention
    loss_ssim_highSignal = SSIM(outputHighSignal_y_true_reshaped, outputHighSignal_y_pred_reshaped)
    loss_ssim_lowSignal = SSIM(outputLowSignal_y_true_reshaped, outputLowSignal_y_pred_reshaped)

    # loss_l1 averaged between hyper- and hypo- intensities and equal contribution of both SSIM and L1
    local_loss = .5 * ((loss_l1_highSignal + loss_l1_lowSignal) / 2) + .5 * (
                (loss_ssim_highSignal + loss_ssim_lowSignal) / 2)

    total_loss = .2 * global_loss + (.8 * local_loss)

    return total_loss


def histogram_loss(y_true, y_pred, bins=256, epsilon=1e-12):
    # Compute histograms of pixel values for y_true and y_pred
    hist_true = tf.histogram_fixed_width(y_true, value_range=(0.0, 1.0), nbins=bins)
    hist_pred = tf.histogram_fixed_width(y_pred, value_range=(0.0, 1.0), nbins=bins)

    # Normalize histograms to have unit area
    hist_true = hist_true / K.sum(hist_true) + epsilon
    hist_pred = hist_pred / K.sum(hist_pred) + epsilon

    # Compute the Jensen-Shannon divergence between the histograms
    epsilon = K.epsilon()
    hist_mean = 0.5 * (hist_true + hist_pred)
    hist_jsd = (0.5 * K.sum(hist_true * K.log(hist_true / hist_mean + epsilon))) + (
                0.5 * K.sum(hist_pred * K.log(hist_pred / hist_mean + epsilon)))

    # Return the histogram loss
    return hist_jsd
