from keras.layers import Flatten, Dense, Input, MaxPooling3D
from keras.models import Model
from models.modelBasics import Unet, ResUnet, AttUnet
from models.modelUtils import ConvP3D
import tensorflow as tf

class GAN(Model):
    def __init__(self, model:str, latent_dim: int, depth: int, height: int, width: int, nbrFilters:int, nbrChannels: int, nbrLayers: int, dropout_rate:float):
        super(GAN, self).__init__()

        self.latent_dim = latent_dim
        self.depth = depth
        self.height = height
        self.width = width
        self.nbrFilters = nbrFilters
        self.nbrChannels = nbrChannels
        self.nbrLayers = nbrLayers
        self.dropout_rate = dropout_rate

        # Build the generator and discriminator models using the input tensor
        generator = self.generator(model)
        discriminator = self.discriminator()
        discriminator.trainable = False

        # Create the GAN model
        self.GAN = tf.keras.Sequential([generator, discriminator])

    def generator(self, model):
        if model == 'Unet':
            generator = Unet(self.depth, self.height, self.width, self.nbrFilters, self.nbrChannels, self.nbrLayers, self.dropout_rate)
            print('Unet was chosen as generator')
        if model == 'ResUnet':
            generator = ResUnet(self.depth, self.height, self.width, self.nbrFilters, self.nbrChannels, self.nbrLayers, self.dropout_rate)
            print('ResUnet was chosen as generator')
        if model == 'AttUnet':
            generator = AttUnet(self.depth, self.height, self.width, self.nbrFilters, self.nbrChannels, self.nbrLayers, self.dropout_rate)
            print('AttUnet was chosen as generator')

        return generator

    def discriminator(self):
        inputs = Input(shape=(self.depth, self.height, self.width, self.nbrChannels))

        self.encodeLayers = []

        for itr in range(0, self.nbrLayers, 1):
            if itr == 0:
                curConv = ConvP3D(inputs, self.nbrFilters)
            else:
                poolLayer = MaxPooling3D(pool_size=(2, 2, 2))(self.encodeLayers[-1])
                curConv = ConvP3D(poolLayer, self.nbrFilters * (2 ** (itr)))

            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))

            self.encodeLayers.append(curConv)

        outLayer = Dense(units=self.latent_dim)(Flatten()(self.encodeLayers[-1]))
        self.EncodeShape = outLayer.shape

        model = Model(inputs=inputs, outputs=outLayer, name='Discriminator')

        return model

    def call(self, inputs):
        GAN_output = self.GAN(inputs)

        return GAN_output

