from keras.layers import Lambda, Flatten, Dense, UpSampling3D, Input, MaxPooling3D, Dropout, LeakyReLU, Conv3DTranspose
from keras.models import Model
from models.modelUtils import *
from kerastuner import HyperModel
from kerastuner.tuners import RandomSearch
from kerastuner.engine.hyperparameters import HyperParameters

import numpy as np

class Autoencoder(tf.keras.Model):
    def __init__(self, latent_dim: int, depth: int, height: int, width: int, nbrFilters:int, nbrChannels: int, nbrLayers: int):
        super(Autoencoder, self).__init__()

        self.latent_dim = latent_dim
        self.depth = depth
        self.height = height
        self.width = width
        self.nbrFilters = nbrFilters
        self.nbrChannels = nbrChannels
        self.nbrLayers = nbrLayers

        self.encoder_model = self.encoder()
        self.decoder_model = self.decoder()

    def encoder(self):
        inputs = Input(shape=(self.depth, self.height, self.width, self.nbrChannels))

        self.encodeLayers = []

        for itr in range(0, self.nbrLayers, 1):
            if itr == 0:
                curConv = ConvP3D(inputs, self.nbrFilters)
            else:
                poolLayer = MaxPooling3D(pool_size=(2, 2, 2))(self.encodeLayers[-1])
                curConv = ConvP3D(poolLayer, self.nbrFilters * (2 ** (itr)))

            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))

            self.encodeLayers.append(curConv)

        outLayer = Dense(units=self.latent_dim)(Flatten()(self.encodeLayers[-1]))
        self.EncodeShape = outLayer.shape

        model = Model(inputs=inputs, outputs=outLayer, name='Encoder-AE')

        self.count_trainable_params(model)

        return model

    def decoder(self):
        inputs = Input(shape=(self.latent_dim,))
        x = Dense(units=self.nbrFilters * 4 * 4 * 4)(inputs)
        x = Reshape((4, 4, 4, self.nbrFilters))(x)

        self.decodeLayers = []

        for itr in range(self.nbrLayers - 1, -1, -1):
            if itr == self.nbrLayers - 1:
                curConv = Conv3D(self.nbrFilters * (2 ** (itr)), 2, activation='relu', padding='same', kernel_initializer='he_normal')(x)

            else:
                curConv = UpSampling3D(size=(2, 2, 2))(self.decodeLayers[-1])
                curConv = Conv3D(self.nbrFilters * (2 ** (itr)), 2, activation='relu', padding='same', kernel_initializer='he_normal')(curConv)

            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))
            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))
            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))

            self.decodeLayers.append(curConv)

        outLayer = Conv3D(self.nbrChannels, 1, activation='tanh')(self.decodeLayers[-1])

        model = Model(inputs=inputs, outputs=outLayer, name='Decoder-AE')

        self.count_trainable_params(model)

        return model

    def count_trainable_params(self, model):
        trainable_params = int(np.sum([K.count_params(w) for w in model.trainable_weights]))
        non_trainable_params = int(np.sum([K.count_params(w) for w in model.non_trainable_weights]))
        total_params = trainable_params + non_trainable_params

        print("Model Parameters:", "{:,}".format(total_params))
        print("Trainable Parameters:", "{:,}".format(trainable_params))
        print("Non-Trainable Parameters:", "{:,}".format(non_trainable_params))


    def call(self, inputs):
        encoded = self.encoder_model(inputs)
        decoded = self.decoder_model(encoded)

        return decoded
class VAE(tf.keras.Model):
    def __init__(self, latent_dim: int, depth: int, height: int, width: int, nbrFilters: int, nbrChannels: int, nbrLayers: int):
        super(VAE, self).__init__()

        self.latent_dim = latent_dim
        self.depth = depth
        self.height = height
        self.width = width
        self.nbrFilters = nbrFilters
        self.nbrChannels = nbrChannels
        self.nbrLayers = nbrLayers

        self.encoder_model, self.encoder_mean_model, self.encoder_logvar_model = self.encoder()
        self.decoder_model = self.decoder()

    def encoder(self):
        inputs = Input(shape=(self.depth, self.height, self.width, self.nbrChannels))

        self.encodeLayers = []

        for itr in range(0, self.nbrLayers, 1):
            if itr == 0:
                curConv = ConvP3D(inputs, self.nbrFilters)
            else:
                poolLayer = MaxPooling3D(pool_size=(2, 2, 2))(self.encodeLayers[-1])
                curConv = ConvP3D(poolLayer, self.nbrFilters * (2 ** (itr)))

            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))

            self.encodeLayers.append(curConv)

        flatten = Flatten()(self.encodeLayers[-1])
        z_mean = Dense(units=self.latent_dim)(flatten)
        z_log_var = Dense(units=self.latent_dim)(flatten)

        def sampling(args):
            z_mean, z_log_var = args
            batch = K.shape(z_mean)[0]
            dim = K.int_shape(z_mean)[1]
            epsilon = K.random_normal(shape=(batch, dim))
            return z_mean + K.exp(0.5 * z_log_var) * epsilon

        z = Lambda(sampling)([z_mean, z_log_var])

        model = Model(inputs=inputs, outputs=z, name='Encoder-VAE')
        model_mean = Model(inputs=inputs, outputs=z_mean)
        model_log_var = Model(inputs=inputs, outputs=z_log_var)

        return model, model_mean, model_log_var

    def decoder(self):
        inputs = Input(shape=(self.latent_dim,))
        x = Dense(units=self.nbrFilters * 4 * 4 * 4)(inputs)
        x = Reshape((4, 4, 4, self.nbrFilters))(x)

        self.decodeLayers = []

        for itr in range(self.nbrLayers - 1, -1, -1):
            if itr == self.nbrLayers - 1:
                curConv = Conv3D(self.nbrFilters * (2 ** (itr)), 2, activation='relu', padding='same', kernel_initializer='he_normal')(x)

            else:
                curConv = UpSampling3D(size=(2, 2, 2))(self.decodeLayers[-1])
                curConv = Conv3D(self.nbrFilters * (2 ** (itr)), 2, activation='relu', padding='same', kernel_initializer='he_normal')(curConv)

            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))
            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))
            curConv = ConvP3D(curConv, self.nbrFilters * (2 ** (itr)))

            self.decodeLayers.append(curConv)

        outLayer = Conv3D(self.nbrChannels, 1, activation='tanh')(self.decodeLayers[-1])

        model = Model(inputs=inputs, outputs=outLayer, name='Decoder-VAE')

        self.count_trainable_params(model)

        return model

    def count_trainable_params(self, model):
        trainable_params = int(np.sum([K.count_params(w) for w in model.trainable_weights]))
        non_trainable_params = int(np.sum([K.count_params(w) for w in model.non_trainable_weights]))
        total_params = trainable_params + non_trainable_params

        print("Model Parameters:", "{:,}".format(total_params))
        print("Trainable Parameters:", "{:,}".format(trainable_params))
        print("Non-Trainable Parameters:", "{:,}".format(non_trainable_params))

    def call(self, inputs):
        encoded = self.encoder_model(inputs)
        decoded = self.decoder_model(encoded)

        return decoded
class Unet(tf.keras.Model):
    def __init__(self, depth: int, height: int, width: int, nbrFilters: int, nbrChannels: int, nbrClass:int, nbrLayers: int, dropout_rate:float):
        super(Unet, self).__init__()

        self.depth = depth
        self.height = height
        self.width = width
        self.nbrFilters = nbrFilters
        self.nbrChannels = nbrChannels
        self.nbrClass = nbrClass
        self.nbrLayers = nbrLayers
        self.dropout_rate = dropout_rate

        self.unet = self.buildModel()

    def buildModel(self):
        inputs = Input(shape=(self.depth, self.height, self.width, self.nbrChannels))
        x = inputs

        # Encoder
        encode_layers = []

        for itr in range(self.nbrLayers):
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))

            encode_layers.append(x)

            x = MaxPooling3D(pool_size=(2, 2, 2))(x)

        self.encoder = x

        x = Dropout(self.dropout_rate)(x)

        # bottleneck layer
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))

        x = Dropout(self.dropout_rate)(x)

        # Decoder
        for itr in reversed(range(self.nbrLayers)):
            x = UpSampling3D(size=(2, 2, 2))(x)
            x = Concatenate(axis=-1)([encode_layers[itr], x])
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))

        outLayer = Conv3D(self.nbrClass, 1, activation='tanh')(x)

        model = Model(inputs=inputs, outputs=outLayer, name='Generator')

        self.count_trainable_params(model)

        return model

    def count_trainable_params(self, model):
        trainable_params = int(np.sum([K.count_params(w) for w in model.trainable_weights]))
        non_trainable_params = int(np.sum([K.count_params(w) for w in model.non_trainable_weights]))
        total_params = trainable_params + non_trainable_params

        print("Model Parameters:", "{:,}".format(total_params))
        print("Trainable Parameters:", "{:,}".format(trainable_params))
        print("Non-Trainable Parameters:", "{:,}".format(non_trainable_params))

    def call(self, inputs):
            model = self.unet(inputs)

            return model

class AutoHP_Unet(HyperModel):
    def __init__(self, depth: int, height: int, width: int, nbrChannels: int, nbrClass:int, loss, metrics):
        super(AutoHP_Unet, self).__init__()

        self.depth = depth
        self.height = height
        self.width = width
        self.nbrChannels = nbrChannels
        self.nbrClass = nbrClass
        self.loss = loss
        self.metrics = metrics

    def build(self, hp):
        # Define hyperparameters - manually

        self.nbrLayers = hp.Int('num_layers', min_value=3, max_value=6, step=1)
        self.nbrFilters = hp.Int('num_filters_base', min_value=16, max_value=64, step=16)
        self.dropout_rate = hp.Float('dropout_rate', min_value=0.0, max_value=0.5, step=0.1)

        inputs = Input(shape=(self.depth, self.height, self.width, self.nbrChannels))
        x = inputs

        # Encoder
        encode_layers = []

        for itr in range(self.nbrLayers):
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))

            encode_layers.append(x)

            x = MaxPooling3D(pool_size=(2, 2, 2))(x)

        self.encoder = x

        x = Dropout(self.dropout_rate)(x)

        # bottleneck layer
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))

        x = Dropout(self.dropout_rate)(x)

        # Decoder
        for itr in reversed(range(self.nbrLayers)):
            x = UpSampling3D(size=(2, 2, 2))(x)
            x = Concatenate(axis=-1)([encode_layers[itr], x])
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))

        outLayer = Conv3D(self.nbrClass, 1, activation='tanh')(x)

        model = Model(inputs=inputs, outputs=outLayer, name='Generator')

        # Define optimizer and loss function
        optimizer = hp.Fixed('optimizer', value='adam')
        model.compile(optimizer=optimizer, loss=self.loss, metrics=self.metrics)

        self.count_trainable_params(model)

        return model

    def count_trainable_params(self, model):
        trainable_params = int(np.sum([K.count_params(w) for w in model.trainable_weights]))
        non_trainable_params = int(np.sum([K.count_params(w) for w in model.non_trainable_weights]))
        total_params = trainable_params + non_trainable_params

        print("Model Parameters:", "{:,}".format(total_params))
        print("Trainable Parameters:", "{:,}".format(trainable_params))
        print("Non-Trainable Parameters:", "{:,}".format(non_trainable_params))

class ResUnet(tf.keras.Model):
    def __init__(self, depth: int, height: int, width: int, nbrFilters: int, nbrClass:int, nbrChannels: int, nbrLayers: int, dropout_rate: float):
        super(ResUnet, self).__init__()

        self.depth = depth
        self.height = height
        self.width = width
        self.nbrFilters = nbrFilters
        self.nbrChannels = nbrChannels
        self.nbrClass = nbrClass
        self.nbrLayers = nbrLayers
        self.dropout_rate = dropout_rate

        self.resunet = self.buildModel()

    def buildModel(self):
        inputs = Input(shape=(self.depth, self.height, self.width, self.nbrChannels))
        x = inputs

        encode_layers = []

        # Encoder
        for itr in range(self.nbrLayers):
            residual = x
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = Concatenate(axis=-1)([x, residual])

            encode_layers.append(x)

            x = MaxPooling3D(pool_size=(2, 2, 2))(x)

        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = Dropout(self.dropout_rate)(x)

        # Decoder
        for itr in reversed(range(self.nbrLayers)):
            x = UpSampling3D(size=(2, 2, 2))(x)
            x = Concatenate(axis=-1)([encode_layers[itr], x])
            residual = x
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = Concatenate(axis=-1)([x, residual])

        outLayer = Conv3D(self.nbrClass, 1, activation='tanh')(x)
        model = Model(inputs=inputs, outputs=outLayer, name='Generator')

        self.count_trainable_params(model)

        return model

    def count_trainable_params(self, model):
        trainable_params = int(np.sum([K.count_params(w) for w in model.trainable_weights]))
        non_trainable_params = int(np.sum([K.count_params(w) for w in model.non_trainable_weights]))
        total_params = trainable_params + non_trainable_params

        print("Model Parameters:", "{:,}".format(total_params))
        print("Trainable Parameters:", "{:,}".format(trainable_params))
        print("Non-Trainable Parameters:", "{:,}".format(non_trainable_params))

    def call(self, inputs):
        model = self.resunet(inputs)

        return model

class AttUnet(tf.keras.Model):
    def __init__(self, depth: int, height: int, width: int, nbrFilters: int, nbrClass:int, nbrChannels: int, nbrLayers: int,
                 dropout_rate: float):
        super(AttUnet, self).__init__()

        self.depth = depth
        self.height = height
        self.width = width
        self.nbrFilters = nbrFilters
        self.nbrClass = nbrClass
        self.nbrChannels = nbrChannels
        self.nbrLayers = nbrLayers
        self.dropout_rate = dropout_rate

        self.unet = self.buildModel()

    def buildModel(self):
        inputs = Input(shape=(self.depth, self.height, self.width, self.nbrChannels))
        x = inputs

        # Encoder
        encode_layers = []

        for itr in range(self.nbrLayers):
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))

            encode_layers.append(x)

            x = MaxPooling3D(pool_size=(2, 2, 2))(x)

        self.encoder = x

        x = Dropout(self.dropout_rate)(x)

        # bottleneck layer
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))
        x = ConvP3D(x, self.nbrFilters * (2 ** self.nbrLayers))

        x = Dropout(self.dropout_rate)(x)

        # Decoder
        for itr in reversed(range(self.nbrLayers)):
            x = UpSampling3D(size=(2, 2, 2))(x)
            x = AttentionConcatLayer(curConv=x, curSkipConnection=encode_layers[itr])
            x = Concatenate(axis=-1)([x, encode_layers[itr]])
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))
            x = ConvP3D(x, self.nbrFilters * (2 ** itr))

        outLayer = Conv3D(self.nbrClass, 1, activation='tanh')(x)

        model = Model(inputs=inputs, outputs=outLayer, name='Generator')

        self.count_trainable_params(model)

        return model

    def count_trainable_params(self, model):
        trainable_params = int(np.sum([K.count_params(w) for w in model.trainable_weights]))
        non_trainable_params = int(np.sum([K.count_params(w) for w in model.non_trainable_weights]))
        total_params = trainable_params + non_trainable_params

        print("Model Parameters:", "{:,}".format(total_params))
        print("Trainable Parameters:", "{:,}".format(trainable_params))
        print("Non-Trainable Parameters:", "{:,}".format(non_trainable_params))

    def call(self, inputs):
        model = self.unet(inputs)

        return model