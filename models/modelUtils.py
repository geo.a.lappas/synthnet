from keras.layers import Conv3D, BatchNormalization, Activation
import keras.backend as K
import tensorflow as tf
from keras.layers import Reshape, Concatenate, add, multiply, concatenate

def AttentionGateLayer(inTensor1, inTensor2, nbrIntermediateFilters):
    """Attention gate. Compresses both inputs to n_intermediate_filters filters before processing.
       Implemented as proposed by Oktay et al. in their Attention U-net, see: https://arxiv.org/abs/1804.03999.
    """
    # calculate the convolutions on the intermediate filters
    inTensor1_conv = Conv3D(nbrIntermediateFilters, kernel_size=1, strides=1, padding="same", kernel_initializer="he_normal")(inTensor1)
    inTensor2_conv = Conv3D(nbrIntermediateFilters, kernel_size=1, strides=1, padding="same", kernel_initializer="he_normal")(inTensor2)

    activationFunction = Activation("relu")(add([inTensor1_conv, inTensor2_conv]))

    outTensor_conv = Conv3D(filters=1, kernel_size=1, strides=1, padding="same", kernel_initializer="he_normal")(activationFunction)
    activationFunction = Activation("sigmoid")(outTensor_conv)

    return multiply([inTensor1, activationFunction])

def AttentionConcatLayer(curConv, curSkipConnection):
    """Performs concatenation of upsampled conv_below with attention gated version of skip-connection
    """
    convShapeFilter = curConv.get_shape().as_list()[-1]
    attentionOutLayer = AttentionGateLayer(curSkipConnection, curConv, convShapeFilter)

    return concatenate([curConv, attentionOutLayer])

def residual_block(x, filters):
    res = Conv3D(filters, 3, activation='relu', padding='same', kernel_initializer='he_normal')(x)
    res = Conv3D(filters, 3, activation=None, padding='same', kernel_initializer='he_normal')(res)

    return tf.keras.layers.add([x, res])

def ConvP3D(x_in, out_filters, strides=(1, 1, 1), use_attention=False):
    x = Conv3D(out_filters, kernel_size=(3, 3, 3), padding='same', strides=strides, kernel_initializer='he_normal')(x_in)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Conv3D(out_filters, kernel_size=(3, 3, 3), padding='same', strides=strides, kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    if use_attention:
        x = SelfAttention(x, out_filters)

    return x

def SelfAttention(x, channels):
    # Helper function to add self-attention layer after a convolutional block
    x = SelfAttention3DLayer(channels)(x)

    return x

class SelfAttention3DLayer(tf.keras.layers.Layer):
    def __init__(self, channels, compression=2, activation='relu', **kwargs):
        super(SelfAttention3DLayer, self).__init__(**kwargs)
        self.channels = channels
        self.compression = compression
        self.activation = Activation(activation)

    def build(self, input_shape):
        self.theta = Conv3D(self.channels // self.compression, 1, use_bias=False, kernel_initializer='he_normal')
        self.phi = Conv3D(self.channels // self.compression, 1, use_bias=False, kernel_initializer='he_normal')
        self.g = Conv3D(self.channels // self.compression, 1, use_bias=False, kernel_initializer='he_normal')
        self.o = Conv3D(self.channels, 1, use_bias=False, kernel_initializer='he_normal')
        super(SelfAttention3DLayer, self).build(input_shape)

    def call(self, x):
        batch_size, height, width, depth, num_channels = x.shape.as_list()
        theta = self.theta(x)
        phi = self.phi(x)
        g = self.g(x)

        theta = Reshape((height * width * depth, self.channels // self.compression))(theta)
        phi = Reshape((height * width * depth, self.channels // self.compression))(phi)
        g = Reshape((height * width * depth, self.channels // self.compression))(g)

        theta_phi = K.batch_dot(theta, phi, axes=[2, 2])
        theta_phi = self.activation(theta_phi)
        attention = K.batch_dot(theta_phi, g, axes=[2, 1])

        attention = Reshape((height, width, depth, self.channels // self.compression))(attention)
        attention = self.o(attention)
        attention = Concatenate(axis=-1)([attention, x])
        return attention

    def compute_output_shape(self, input_shape):
        return input_shape[:-1] + (input_shape[-1] + self.channels,)

