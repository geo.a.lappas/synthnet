from preprocessing.utils import *
from preprocessing.callbacksModule1 import *
from preprocessing.dataGenTranslator import DataGenerator
from preprocessing.getParamsTranslator import dataSplit
from models.modelBasics import *
from models.losses import *
from postprocessing.utilsNiftiProcessing import *
from helpers.patchSampling import create_patches2
from preprocessing.callbackPredictions import plotMetricsOnEpochEnd
from kerastuner.tuners import RandomSearch

import gc
import os

def updateParams(datagenParams, modelParams, inputDir, targetDir):
    datagenParams['dim'] = tuple(datagenParams['dim'])
    modelParams['input_shape'] = tuple(modelParams['input_shape'])

    if len(inputDir) == 1 and len(targetDir) > 1:
        print('Single input and multi-modal target found')
    if len(inputDir) > 1 and len(targetDir) == 1:
        print('Multi-modal input and single target found')
    if len(inputDir) > 1 and len(targetDir) > 1:
        print('Multi-modal input and multi-modal target found')

    # updating dictionaries
    datagenParams['nbrInChannel'] = len(inputDir)
    datagenParams['nbrOutChannel'] = len(targetDir)
    modelParams['num_classes'] = datagenParams['nbrOutChannel']

    if datagenParams['patch']:
        modelParams['input_shape'] = (*[datagenParams['patchSize']] * len(datagenParams['dim']), datagenParams['nbrInChannel'])
    else:
        modelParams['input_shape'] = datagenParams['dim'] + (datagenParams['nbrInChannel'],)

    return datagenParams, modelParams

def getMetrics():

    # metricsLst = [tf.keras.metrics.MeanAbsoluteError(), SSIM]
    metricsLst = [SSIM, psnr_loss]

    return metricsLst

def getLosses(loss_name):
    if loss_name == 'SSIM':
        curLoss = SSIM
    elif loss_name == 'SSIM_att':
        curLoss = SSIM_att
    elif loss_name == 'MAE':
        curLoss = MAE
    elif loss_name == 'MSE':
        curLoss = MSE
    elif loss_name == 'SSIM_MAE':
        curLoss = SSIM_MAE
    elif loss_name == 'SSIM_MAE_att':
        curLoss = SSIM_MAE_att2
    else:
        curLoss = tf.keras.losses.MeanAbsoluteError()

    return curLoss

def createFullDataLst(partitionInput, partitionTarget, multiFactor, modelDir, modelParams, datagenParams, mode='partial',
                      trainFlag=False, modelDirFlag=None, logDirFlag=None):
    # merge all the data from the several channels together - train data
    partitionInputSum, partitionTargetSum = [], []

    for itrChannels in range(multiFactor):
        partitionInputSum.append(partitionInput[itrChannels][0])
        partitionTargetSum.append(partitionTarget[itrChannels][0])

    if trainFlag:
        if modelDirFlag is None:
            # Set the corresponding directories
            modelDirTemp, logDir, xaiDir = makeModelDir(modelDir, modelParams, datagenParams, partitionInputSum, 'All')

            modelDirFold = os.path.join(modelDirTemp, 'Fold_' + 'All')
            if not os.path.exists(modelDirFold):
                os.mkdir(modelDirFold)

        else:
            modelDirFold = modelDirFlag
            logDir = logDirFlag

    if trainFlag:
        if mode == 'partial':
            curTrainFoldInput_train = [d['train'] for d in partitionInputSum]
            curTrainFoldTarget_train = [d['train'] for d in partitionTargetSum]
            curTrainFoldInput_valid = [d['valid'] for d in partitionInputSum]
            curTrainFoldTarget_valid = [d['valid'] for d in partitionTargetSum]

            return [curTrainFoldInput_train, curTrainFoldTarget_train], [curTrainFoldInput_valid, curTrainFoldTarget_valid], [modelDirFold, logDir]

        if mode == 'full':
            curTrainFoldInput_train = [d['train'] for d in partitionInputSum]
            curTrainFoldTarget_train = [d['train'] for d in partitionTargetSum]

        return [curTrainFoldInput_train, curTrainFoldTarget_train], [modelDirFold, logDir]

    else:
        if mode == 'partial' or mode == 'full':
            curFoldInput_test = [d['test'] for d in partitionInputSum]
            curFoldTarget_test = [d['test'] for d in partitionTargetSum]

        return [curFoldInput_test, curFoldTarget_test]

def createDatasets(inputDir:list, targetDir:list, cv: bool = False, trainFlag:bool = False):
    # split your datasets - calculate folds
    minChannel, maxChannel = min(len(inputDir), len(targetDir)), max(len(inputDir), len(targetDir))
    multiFactor = maxChannel // minChannel
    partitionInput, partitionTarget = [], []

    for itrChannel in range(multiFactor):
        # if one of the channels is one then everything is fine
        if len(inputDir) > 1 and len(targetDir) == 1:
            tempTarDir = targetDir * len(inputDir)
            del targetDir
            targetDir = tempTarDir
            del tempTarDir

        if len(inputDir) > 1 and len(targetDir) == 1:
            tempInDir = inputDir * len(targetDir)
            del inputDir
            inputDir = tempInDir
            del tempInDir

        partitionInputTemp, partitionTargetTemp = dataSplit(inputDir=inputDir[itrChannel], targetDir=targetDir[itrChannel], trainFlag=trainFlag, CV=cv, singleDatasetValid=False)
        partitionInput.append(partitionInputTemp); partitionTarget.append(partitionTargetTemp)

    return partitionInput, partitionTarget

def savePredictions(targetLst: list, predLst: list, predictionDir: str):
    if len(targetLst) != len(predLst):
        raise ValueError("Input, target and prediction lists have different length")

    for itrCase in range(len(targetLst)):
        # get affine matrices
        _, affineTar = readNifti2Numpy(targetLst[itrCase])

        # post-process the arrays and saves them into nifti
        modelPred = removeDims(curArray=predLst[itrCase].copy())

        # save nifti
        predDir = os.path.join(predictionDir, 'predictions')
        if not os.path.exists(predDir):
            os.mkdir(predDir)

        convertNumpyArray2Nifti(numpyArr=modelPred, affineArrLst=affineTar, fileName='AI_' + modelPred[itrCase], dir2SaveData=predDir)

# prepare the datasets for per epoch check
def wrapPrepare(multiFactor, partitionInputSum, partitionTargetSum, partitionTestInputSum, partitionTestTargetSum, datagenParams, cv, external, train):
    # get some examples from training, validation and test set to show your model predictions during training
    inputTrainData, targetTrainData, inputValidData, targetValidData, inputTestData, targetTestData = [], [], [], [], [], []

    for itrChannels in range(multiFactor):
        if cv and not external:
            if train:
                inTrain, tarTrain = partitionInputSum[itrChannels]['train'][0], partitionTargetSum[itrChannels]['train'][0]
                inValid, tarValid = partitionInputSum[itrChannels]['valid'][0], partitionTargetSum[itrChannels]['valid'][0]
                trainIn, trainTar = prepareData4Test2(currentInput=inTrain, currentTarget=tarTrain, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])
                validIn, validTar = prepareData4Test2(currentInput=inValid, currentTarget=tarValid, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])

                # get the data to plot
                inputTrainData.append(trainIn); targetTrainData.append(trainTar)
                inputValidData.append(validIn); targetValidData.append(validTar)

            else:
                inTest, tarTest = partitionTestInputSum[itrChannels]['test'][0], partitionTestTargetSum[itrChannels]['test'][0]
                testIn, testTar = prepareData4Test2(currentInput=inTest, currentTarget=tarTest, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])

                inputTestData.append(testIn); targetTestData.append(testTar)

        elif not cv and external:
            if train:
                inTrain, tarTrain = partitionInputSum[itrChannels][0], partitionTargetSum[itrChannels][0]
                inValid, tarValid = partitionTestInputSum[itrChannels][-1], partitionTestTargetSum[itrChannels][-1]
                trainIn, trainTar = prepareData4Test2(currentInput=inTrain, currentTarget=tarTrain, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])
                validIn, validTar = prepareData4Test2(currentInput=inValid, currentTarget=tarValid, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])

                # get the data to plot
                inputTrainData.append(trainIn); targetTrainData.append(trainTar)
                inputValidData.append(validIn); targetValidData.append(validTar)

            inTest, tarTest = partitionTestInputSum[itrChannels][0], partitionTestTargetSum[itrChannels][0]
            testIn, testTar = prepareData4Test2(currentInput=inTest, currentTarget=tarTest, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])

            inputTestData.append(testIn); targetTestData.append(testTar)

        elif not cv and not external:
            print('Model predictions plot (only) will use the same for validation and test sets')

            if train:
                inTrain, tarTrain = partitionInputSum[itrChannels][0], partitionTargetSum[itrChannels][0]
                inValid, tarValid = partitionTestInputSum[itrChannels][-1], partitionTestTargetSum[itrChannels][-1]
                trainIn, trainTar = prepareData4Test2(currentInput=inTrain, currentTarget=tarTrain, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])
                validIn, validTar = prepareData4Test2(currentInput=inValid, currentTarget=tarValid, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])

                # get the data to plot
                inputTrainData.append(trainIn); targetTrainData.append(trainTar)
                inputValidData.append(validIn); targetValidData.append(validTar)

            inTest, tarTest = partitionTestInputSum[itrChannels][-1], partitionTestTargetSum[itrChannels][-1]
            testIn, testTar = prepareData4Test2(currentInput=inTest, currentTarget=tarTest, dim=datagenParams['dim'], patch=datagenParams['patch'], patchSize=datagenParams['patchSize'], denoising=datagenParams['denoising'])

            inputTestData.append(testIn); targetTestData.append(testTar)

    return [inputTrainData, targetTrainData], [inputValidData, targetValidData], [inputTestData, targetTestData]

def loadWeightsAndRetrain(modelPath, modelDir, modelName, loss_name, modelParams, trainGen, validGen, nbrEpochs, tfCallbacks, gpuLst):
    deviceLst= []

    # for itrGpu in range(len(gpuLst)):
    #     deviceLst.append("/gpu:" + str(itrGpu))
    #
    # mirrored_strategy = tf.distribute.MirroredStrategy(devices=deviceLst)
    #
    # with mirrored_strategy.scope():
    curLoss = getLosses(loss_name)

    # metrics definition
    metrics = getMetrics()

    if modelName == 'Unet':
        model = Unet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                     width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                     nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                     nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

    elif modelName == 'ResUnet':
        model = ResUnet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                     width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                     nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                     nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

    elif modelName == 'AttUnet':
        model = AttUnet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                     width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                     nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                     nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

    elif modelName == 'Autoencoder':
        model = Autoencoder(latent_dim=1024, depth=modelParams['input_shape'][0],
                            height=modelParams['input_shape'][1],
                            width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                            nbrFilters=modelParams['filters'], nbrLayers=modelParams['num_layers'])
    elif modelName == 'VAE':
        model = VAE(latent_dim=1024, depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                    width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                    nbrFilters=modelParams['filters'], nbrLayers=modelParams['num_layers'])

    # Compile the model with the best set of hyperparameters
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-2, epsilon=0.1), loss=curLoss, metrics=metrics)

    print('Model.compile is complete')

    model.built = True

    # load pre-trained weights
    model.load_weights(modelPath + "Unet3D_model.ckpt")
    print('Model weights loaded')
    K.set_value(model.optimizer.learning_rate, 0.001)

    # Train the model using the best set of hyperparameters.
    model.fit(x=trainGen, epochs=nbrEpochs, validation_data=validGen, callbacks=tfCallbacks, verbose=1, use_multiprocessing=True, workers=8, shuffle=True)

    return model

def loadWeightsAndPredict(modelName, loss_name, modelParams, modelPath, dataToValidate, gpuLst):
    deviceLst = []

    # for itrGpu in range(len(gpuLst)):
    #     deviceLst.append("/gpu:" + str(itrGpu))
    #
    # mirrored_strategy = tf.distribute.MirroredStrategy(devices=deviceLst)

    # with mirrored_strategy.scope():
    curLoss = getLosses(loss_name)

    # metrics definition
    metrics = getMetrics()

    if modelName == 'Unet':
        model = Unet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                     width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                     nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                     nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

    elif modelName == 'ResUnet':
        model = ResUnet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                        width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                        nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                        nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

    elif modelName == 'AttUnet':
        model = AttUnet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                        width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                        nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                        nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

    elif modelName == 'Autoencoder':
        model = Autoencoder(latent_dim=1024, depth=modelParams['input_shape'][0],
                            height=modelParams['input_shape'][1],
                            width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                            nbrFilters=modelParams['filters'], nbrLayers=modelParams['num_layers'])
    elif modelName == 'VAE':
        model = VAE(latent_dim=1024, depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                    width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                    nbrFilters=modelParams['filters'], nbrLayers=modelParams['num_layers'])

    # Compile the model with the best set of hyperparameters
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-2, epsilon=0.1), loss=curLoss,
                  metrics=metrics)
    print('Model.compile is complete')

    model.built = True

    # load pre-trained weights
    model.load_weights(modelPath + "Unet3D_model.ckpt")
    print('Model weights loaded')

    # TODO fix this for patches - standard patch size
    if dataToValidate.ndim >= 5:
        predictionLst = []

        for itrCases in range(dataToValidate.shape[0]):
            predictionLstCase = []

            for minibatch in range(0, dataToValidate[itrCases].shape[0], 32):
                if minibatch < dataToValidate[itrCases].shape[0] - 32:
                    predictionLstCase.append(model.predict_on_batch(dataToValidate[itrCases][minibatch:minibatch + 32]))
                else:
                    predictionLstCase.append(model.predict_on_batch(dataToValidate[itrCases][minibatch:]))

            predictionLst.append(predictionLstCase)

        predictionLst = np.squeeze([np.concatenate(itr, axis=0) for itr in predictionLst], axis=-1)

    else:
        predictionLst = []

        for itrCases in range(dataToValidate.shape[0]):
            predictionLst.append(model.predict(np.expand_dims(dataToValidate[itrCases], axis=0), verbose=0))

        predictionLst = np.squeeze(np.array(predictionLst), axis=-1)

    print('Model predictions are complete')

    return predictionLst

def modelCompileAndFit(modelName, loss_name, trainGen, validGen, nbrEpochs, tfCallbacks, modelParams, gpuLst):
    deviceLst = []

    for itrGpu in range(len(gpuLst)):
        deviceLst.append("/gpu:" + str(itrGpu))

    mirrored_strategy = tf.distribute.MirroredStrategy(devices=deviceLst)

    with mirrored_strategy.scope():
        curLoss = getLosses(loss_name)

        # metrics definition
        metrics = getMetrics()

        if modelName == 'Unet':
            model = Unet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                         width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                         nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                         nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

        elif modelName == 'ResUnet':
            model = ResUnet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                            width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                            nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                            nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

        elif modelName == 'AttUnet':
            model = AttUnet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                            width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                            nbrClass=modelParams['num_classes'], nbrFilters=modelParams['filters'],
                            nbrLayers=modelParams['num_layers'], dropout_rate=modelParams['dropout'])

        elif modelName == 'Autoencoder':
            model = Autoencoder(latent_dim=1024, depth=modelParams['input_shape'][0],
                                height=modelParams['input_shape'][1],
                                width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                                nbrFilters=modelParams['filters'], nbrLayers=modelParams['num_layers'])
        elif modelName == 'VAE':
            model = VAE(latent_dim=1024, depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1],
                        width=modelParams['input_shape'][2], nbrChannels=modelParams['input_shape'][-1],
                        nbrFilters=modelParams['filters'], nbrLayers=modelParams['num_layers'])

        # Compile the model with the best set of hyperparameters
        model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-2, epsilon=0.1), loss=curLoss, metrics=metrics)
        print('Model.compile is complete')

        # Train the model using the best set of hyperparameters.
        model.fit(x=trainGen, epochs=nbrEpochs, validation_data=validGen, callbacks=tfCallbacks, verbose=1, use_multiprocessing=True, workers=8, shuffle=True)

def modelCompileAndHpSearch(loss_name, modelName, trainGen, validGen, modelParams, modelDir, gpuLst):
    deviceLst = []

    for itrGpu in range(len(gpuLst)):
        deviceLst.append("/gpu:" + str(itrGpu))

    mirrored_strategy = tf.distribute.MirroredStrategy(devices=deviceLst)

    with mirrored_strategy.scope():
        curLoss = getLosses(loss_name)

        # metrics definition
        metrics = getMetrics()

        if modelName == 'Unet':
            # Currently supporting only U-net
            # Define a function that builds the model with the hyperparameters
            hyperModel = AutoHP_Unet(depth=modelParams['input_shape'][0], height=modelParams['input_shape'][1], width=modelParams['input_shape'][2],
                                     nbrChannels=modelParams['input_shape'][-1], nbrClass=modelParams['num_classes'], loss=curLoss, metrics=metrics)

        # Define a tuner and perform hyperparameter search
        tuner = RandomSearch(hyperModel, objective='val_loss', max_trials=50, directory=modelDir, project_name='hp_search_Model3D')

        # Search for best hyperparameters
        tuner.search(trainGen, validation_data=validGen, epochs=20, batch_size=2)

        # Get the best model and summarize it
        best_model = tuner.get_best_models(num_models=1)[0]

        return best_model

def hpSearchModel(inputDir: list, targetDir:list, modelDir: str, foldLst: list = None, model: str = 'CNN', loss: str = 'BCE', nbrEpochs: int = 150, cv: bool = False, validInputDir: list = None, validTargetDir: list = None, datagenParams: dict = None, datagenParamsValid: dict = None, modelParams: dict = None, gpuLst: list = None):
    # update datagenparams and modelparams based on input
    datagenParams, modelParams = updateParams(datagenParams, modelParams, inputDir, targetDir)
    datagenParamsValid, _ = updateParams(datagenParamsValid, modelParams, inputDir, targetDir)

    # split your datasets - calculate folds
    multiFactor = max(len(inputDir), len(targetDir)) // min(len(inputDir), len(targetDir))

    partitionInput, partitionTarget = createDatasets(inputDir, targetDir, trainFlag=True, cv=False)
    partitionInputValid, partitionTargetValid = createDatasets(validInputDir, validTargetDir, trainFlag=True, cv=False)

    trainLst, others = createFullDataLst(partitionInput, partitionTarget, multiFactor, modelDir, modelParams, datagenParams, trainFlag=True, mode='full', modelDirFlag=None, logDirFlag=None)
    testLst, _ = createFullDataLst(partitionInputValid, partitionTargetValid, multiFactor, modelDir, modelParams, datagenParams, trainFlag=True, mode='full', modelDirFlag=others[0], logDirFlag=others[-1])

    curTrainFoldInput_train, curTrainFoldTarget_train = trainLst[0], trainLst[-1]
    curTrainFoldInput_valid, curTrainFoldTarget_valid = testLst[0], testLst[-1]
    modelDirFold, logDir = others[0], others[-1]

    # create the generators
    trainGen = DataGenerator(curTrainFoldInput_train, curTrainFoldTarget_train, **datagenParams)
    validGen = DataGenerator(curTrainFoldInput_valid, curTrainFoldTarget_valid, **datagenParamsValid)

    bestModel = modelCompileAndHpSearch(modelName=model, loss_name=loss, trainGen=trainGen, validGen=validGen, modelDir=modelDirFold, modelParams=modelParams, gpuLst=gpuLst)

    return bestModel
def trainModel3D(inputDir: list, targetDir:list, modelDir: str, foldLst: list = None, model: str = 'CNN', loss: str = 'BCE', nbrEpochs: int = 150, cv: bool = False, validInputDir: list = None, validTargetDir: list = None, datagenParams: dict = None, datagenParamsValid: dict = None, modelParams: dict = None, gpuLst: list = None):
    # update datagenparams and modelparams based on input
    datagenParams, modelParams = updateParams(datagenParams, modelParams, inputDir, targetDir)
    datagenParamsValid, _ = updateParams(datagenParamsValid, modelParams, inputDir, targetDir)

    # split your datasets - calculate folds
    multiFactor = max(len(inputDir), len(targetDir)) // min(len(inputDir), len(targetDir))

    if cv and not validInputDir:
        partitionInput, partitionTarget = createDatasets(inputDir, targetDir, trainFlag=True, cv=True)

        for itrFold in sorted(foldLst):
            gc.collect()

            # merge all the data from the several channels together
            partitionInputSum, partitionTargetSum = [], []

            for itrChannels in range(multiFactor):
                partitionInputSum.append(partitionInput[itrChannels][itrFold])
                partitionTargetSum.append(partitionTarget[itrChannels][itrFold])

            # Set the corresponding directories
            modelDirTemp, logDir, xaiDir = makeModelDir(modelDir, modelParams, datagenParams, partitionInputSum, (itrFold + 1))

            modelDirFold = os.path.join(modelDirTemp, 'Fold' + str(itrFold + 1))
            if not os.path.exists(modelDirFold):
                os.mkdir(modelDirFold)

            # create the datasets for plotting
            tmpTrainLst, tmpValidLst, tmpTestLst = wrapPrepare(multiFactor, partitionInputSum, partitionTargetSum, partitionInputSum, partitionTargetSum, datagenParams, cv=True, external=False, train=True)

            curTrainFoldInput_train = [d['train'] for d in partitionInputSum]
            curTrainFoldTarget_train = [d['train'] for d in partitionTargetSum]
            curTrainFoldInput_valid = [d['valid'] for d in partitionInputSum]
            curTrainFoldTarget_valid = [d['valid'] for d in partitionTargetSum]

            # create the generators
            trainGen = DataGenerator(curTrainFoldInput_train, curTrainFoldTarget_train, **datagenParams)
            validGen = DataGenerator(curTrainFoldInput_valid, curTrainFoldTarget_valid, **datagenParamsValid)

            # callbacks
            callbackSummary1 = getCallback(modelDir=modelDirFold, logDir=logDir)

            print('External dataset is not provided, validation partition will be used for plotting')

            callbackPred = plotMetricsOnEpochEnd(train_data=[tmpTrainLst[0], tmpTrainLst[-1]],
                                                 valid_data=[tmpValidLst[0], tmpValidLst[-1]],
                                                 test_data=[tmpValidLst[0], tmpValidLst[-1]],
                                                 dir2Save=str(xaiDir),
                                                 idx1Slice=60, idx2Slice=70, idx3Slice=80,
                                                 patchFlag=datagenParams['patch'],
                                                 batchSize=datagenParams['batchSize'],
                                                 origSize=datagenParams['dim'][0],
                                                 inChannelLen=datagenParams['nbrInChannel'],
                                                 outChannelLen=datagenParams['nbrOutChannel'])

            # one model instance
            modelCompileAndFit(modelName=model, loss_name=loss, trainGen=trainGen, validGen=validGen, nbrEpochs=nbrEpochs, tfCallbacks=[callbackSummary1, callbackPred], modelParams=modelParams, gpuLst=gpuLst)

            tf.keras.backend.clear_session()

    elif not cv and not validInputDir:
        partitionInput, partitionTarget = createDatasets(inputDir, targetDir, trainFlag=True, cv=False)

        trainLst, testLst, others = createFullDataLst(partitionInput, partitionTarget, multiFactor, modelDir, modelParams, datagenParams, trainFlag=True, mode='partial', modelDirFlag=None, logDirFlag=None)

        curTrainFoldInput_train, curTrainFoldTarget_train = trainLst[0], trainLst[-1]
        curTrainFoldInput_valid, curTrainFoldTarget_valid = testLst[0], testLst[-1]
        modelDirFold, logDir = others[0], others[-1]

        # create the generators
        trainGen = DataGenerator(curTrainFoldInput_train, curTrainFoldTarget_train, **datagenParams)
        validGen = DataGenerator(curTrainFoldInput_valid, curTrainFoldTarget_valid, **datagenParamsValid)

        # create the datasets for plotting
        tmpTrainLst, tmpValidLst, tmpTestLst = wrapPrepare(multiFactor, curTrainFoldInput_train, curTrainFoldTarget_train, curTrainFoldInput_valid, curTrainFoldTarget_valid, datagenParams, cv=False, external=False, train=True)

        # callbacks
        callbackSummary1 = getCallback(modelDir=modelDirFold, logDir=logDir)

        print('External dataset is not provided, validation partition will be used for plotting')

        callbackPred = plotMetricsOnEpochEnd(train_data=[tmpTrainLst[0], tmpTrainLst[-1]],
                                             valid_data=[tmpValidLst[0], tmpValidLst[-1]],
                                             test_data=[tmpTestLst[0], tmpTestLst[-1]],
                                             dir2Save=str(modelDirFold + '/xai'),
                                             idx1Slice=60, idx2Slice=70, idx3Slice=80,
                                             patchFlag=datagenParams['patch'],
                                             batchSize=datagenParams['batchSize'],
                                             origSize=datagenParams['dim'][0],
                                             inChannelLen=datagenParams['nbrInChannel'],
                                             outChannelLen=datagenParams['nbrOutChannel'])

        # one model instance
        modelCompileAndFit(modelName=model, loss_name=loss, trainGen=trainGen, validGen=validGen, nbrEpochs=nbrEpochs, tfCallbacks=[callbackSummary1, callbackPred], modelParams=modelParams, gpuLst=gpuLst)

        tf.keras.backend.clear_session()

    elif not cv and validInputDir:
        partitionInput, partitionTarget = createDatasets(inputDir, targetDir, trainFlag=True, cv=False)
        partitionInputValid, partitionTargetValid = createDatasets(validInputDir, validTargetDir, trainFlag=True, cv=False)

        trainLst, others = createFullDataLst(partitionInput, partitionTarget, multiFactor, modelDir, modelParams, datagenParams, trainFlag=True, mode='full', modelDirFlag=None, logDirFlag=None)
        testLst, _ = createFullDataLst(partitionInputValid, partitionTargetValid, multiFactor, modelDir, modelParams, datagenParams, trainFlag=True, mode='full', modelDirFlag=others[0], logDirFlag=others[-1])

        curTrainFoldInput_train, curTrainFoldTarget_train = trainLst[0], trainLst[-1]
        curTrainFoldInput_valid, curTrainFoldTarget_valid = testLst[0], testLst[-1]
        modelDirFold, logDir = others[0], others[-1]

        # create the generators
        trainGen = DataGenerator(curTrainFoldInput_train, curTrainFoldTarget_train, **datagenParams)
        validGen = DataGenerator(curTrainFoldInput_valid, curTrainFoldTarget_valid, **datagenParamsValid)

        # create the datasets for plotting
        tmpTrainLst, tmpValidLst, tmpTestLst = wrapPrepare(multiFactor, curTrainFoldInput_train, curTrainFoldTarget_train, curTrainFoldInput_valid, curTrainFoldTarget_valid, datagenParams, cv=False, external=True, train=True)

        # callbacks
        callbackSummary1 = getCallback(modelDir=modelDirFold, logDir=logDir)

        callbackPred = plotMetricsOnEpochEnd(train_data=[tmpTrainLst[0], tmpTrainLst[-1]],
                                             valid_data=[tmpValidLst[0], tmpValidLst[-1]],
                                             test_data=[tmpValidLst[0], tmpValidLst[-1]],
                                             dir2Save=str(modelDirFold + '/xai'),
                                             idx1Slice=60, idx2Slice=70, idx3Slice=80,
                                             patchFlag=datagenParams['patch'],
                                             batchSize=datagenParams['batchSize'],
                                             origSize=datagenParams['dim'][0],
                                             inChannelLen=datagenParams['nbrInChannel'],
                                             outChannelLen=datagenParams['nbrOutChannel'])

        # one model instance
        modelCompileAndFit(modelName=model, loss_name=loss, trainGen=trainGen, validGen=validGen, nbrEpochs=nbrEpochs, tfCallbacks=[callbackSummary1, callbackPred], modelParams=modelParams, gpuLst=gpuLst)

        tf.keras.backend.clear_session()

def testModel3D(inputDir: list, targetDir:list, modelDir: str, foldLst: list = None, model: str = 'CNN', loss: str = 'BCE', cv: bool = False, validInputDir: list = None, validTargetDir: list = None, savePred: bool = True, datagenParams: dict = None, datagenParamsValid: dict = None, modelParams: dict = None, gpuLst: list = None):
    # update datagenparams and modelparams based on input
    datagenParams, modelParams = updateParams(datagenParams, modelParams, inputDir, targetDir)
    datagenParamsValid, _ = updateParams(datagenParamsValid, modelParams, inputDir, targetDir)

    # split your datasets - calculate folds
    multiFactor = max(len(inputDir), len(targetDir)) // min(len(inputDir), len(targetDir))

    modelDirPath = modelDir.split("checkpoint.hdf5")[0]

    if cv and not validInputDir:
        partitionInput, partitionTarget = createDatasets(inputDir, targetDir, trainFlag=False, cv=True)

        for itrFold in sorted(foldLst):
            gc.collect()

            # merge all the data from the several channels together
            partitionTestInputSum, partitionTestTargetSum = [], []

            for itrChannels in range(multiFactor):
                partitionTestInputSum.append(partitionInput[itrChannels][itrFold])
                partitionTestTargetSum.append(partitionTarget[itrChannels][itrFold])

            curTestFoldInput = [d['test'] for d in partitionTestInputSum]
            curTestFoldTarget = [d['test'] for d in partitionTestTargetSum]

            inputLst, targetLst = [], []

            for itrChannels in range(multiFactor):
                # Prepare the sets for model prediction
                testInput, testTarget = [], []

                for itrCase in range(len(curTestFoldInput[0])):
                    tempIn, tempTar = prepareData4Test(currentInput=curTestFoldInput[itrChannels][itrCase], currentTarget=curTestFoldTarget[itrChannels][itrCase], dim=datagenParams['dim'])

                    testInput.append(tempIn); testTarget.append(tempIn)
                    inputLst.append(testInput); targetLst.append(testTarget)

                # temp processing everything together
                inputArray = np.array(testInput)

                if inputArray.ndim == 4:
                    inputArray = np.expand_dims(inputArray, axis=-1)

                # if patch-enabled pass here
                if datagenParams['patch']:
                    inputArray = create_patches2(inputArray=inputArray, patchSize=datagenParams['patchSize'])

            # TODO finish this
            # compile the model and load the weights. Then make predictions
            testPred = loadWeightsAndPredict(dataToValidate=inputArray.copy(), modelPath=modelDirPath, modelName=model, loss_name=loss, modelParams=modelParams, gpuLst=gpuLst)

            gc.collect()

            if datagenParams['patch']:
                if testPred.ndim == 5:
                    testPred = np.expand_dims(testPred, axis=-1)

                if inputArray.ndim == 6:
                    image, pred = [], []

                    for itrBatch in range(inputArray.shape[0]):
                        curInput = inputArray[itrBatch]
                        curPred = testPred[itrBatch]

                        tempIn, tempPred = merge_patches2(patch_arr=curInput, patchClass=curPred, origSize=datagenParams['dim'][0], batchSize=curInput.shape[0], patchSize=curInput.shape[2], stride=curInput.shape[2], channel=curInput.shape[-1], modelReady=True)

                        image.append(np.squeeze(tempIn, axis=0))
                        pred.append(np.squeeze(tempPred, axis=0))

                        image = np.array(image)
                        pred = np.array(pred)

                        if savePred:

                            # creating directories for prediction
                            predDir = Path(modelDirPath, 'pred')
                            predDir.mkdir(mode=0o751, parents=True, exist_ok=True)

                            # save predictions
                            for itrBatch in range(image.shape[0]):
                                for itrChannels in range(image.shape[-1]):
                                    # write image and pred as nifti
                                    curDirName = os.path.dirname(curTestFoldInput[itrChannels][itrBatch])
                                    curFileName = curTestFoldInput[itrChannels][itrBatch].split(curDirName + '/')[-1]

                                    convertNumpyArray2Nifti(numpyArr=image[itrBatch, :, :, :, itrChannels], affineArrLst=np.eye(4), fileName=curFileName, dir2SaveData=os.path.join(modelDirPath, 'pred'))
                                    convertNumpyArray2Nifti(numpyArr=pred[itrBatch, :, :, :, itrChannels], affineArrLst=np.eye(4), fileName='pred_' + curFileName, dir2SaveData=os.path.join(modelDirPath, 'pred'))

    elif not cv and validInputDir:
        partitionTestInput, partitionTestTarget = createDatasets(validInputDir, validTargetDir, trainFlag=False, cv=False)
        curTrainFoldInput_valid, curTrainFoldTarget_valid = createFullDataLst(partitionTestInput, partitionTestTarget, multiFactor, modelDir, modelParams, datagenParamsValid, trainFlag=False, mode='full', modelDirFlag=None, logDirFlag=None)

        # merge all the data from the several channels together
        partitionTestInputSum, partitionTestTargetSum = [], []

        for itrChannels in range(multiFactor):
            partitionTestInputSum.append(partitionTestInput[itrChannels])
            partitionTestTargetSum.append(partitionTestTarget[itrChannels])

        curTestFoldInput = [d['test'] for d in partitionTestInputSum]
        curTestFoldTarget = [d['test'] for d in partitionTestTargetSum]

        inputLst, targetLst = [], []

        for itrChannels in range(multiFactor):
            # Prepare the sets for model prediction
            testInput, testTarget = [], []

            for itrCase in range(len(curTestFoldInput[0])):
                tempIn, tempTar = prepareData4Test(currentInput=curTestFoldInput[itrChannels][itrCase], currentTarget=curTestFoldTarget[itrChannels][itrCase], dim=datagenParams['dim'])

                testInput.append(tempIn); testTarget.append(tempIn)
                inputLst.append(testInput); targetLst.append(testTarget)

            # temp processing everything together
            inputArray = np.array(testInput)

            if inputArray.ndim == 4:
                inputArray = np.expand_dims(inputArray, axis=-1)

            # if patch-enabled pass here
            if datagenParams['patch']:
                inputArray = create_patches2(inputArray=inputArray, patchSize=datagenParams['patchSize'])

        # TODO finish this
        # compile the model and load the weights. Then make predictions
        testPred = loadWeightsAndPredict(dataToValidate=inputArray.copy(), modelPath=modelDirPath,
                                         modelName=model, loss_name=loss, modelParams=modelParams,
                                         gpuLst=gpuLst)

        gc.collect()

        if datagenParams['patch']:
            if testPred.ndim == 5:
                testPred = np.expand_dims(testPred, axis=-1)

            if inputArray.ndim == 6:
                image, pred = [], []

                for itrBatch in range(inputArray.shape[0]):
                    curInput = inputArray[itrBatch]
                    curPred = testPred[itrBatch]

                    tempIn, tempPred = merge_patches2(patch_arr=curInput, patchClass=curPred, origSize=datagenParams['dim'][0], batchSize=curInput.shape[0], patchSize=curInput.shape[2], stride=curInput.shape[2], channel=curInput.shape[-1], modelReady=True)

                    image.append(np.squeeze(tempIn, axis=0))
                    pred.append(np.squeeze(tempPred, axis=0))

                    image = np.array(image)
                    pred = np.array(pred)

                    if savePred:

                        # creating directories for prediction
                        predDir = Path(modelDirPath, 'pred')
                        predDir.mkdir(mode=0o751, parents=True, exist_ok=True)

                        # save predictions
                        for itrBatch in range(image.shape[0]):
                            for itrChannels in range(image.shape[-1]):
                                # write image and pred as nifti
                                curDirName = os.path.dirname(curTestFoldInput[itrChannels][itrBatch])
                                curFileName = curTestFoldInput[itrChannels][itrBatch].split(curDirName + '/')[-1]

                                convertNumpyArray2Nifti(numpyArr=image[itrBatch, :, :, :, itrChannels], affineArrLst=np.eye(4), fileName=curFileName, dir2SaveData=os.path.join(modelDirPath, 'pred'))
                                convertNumpyArray2Nifti(numpyArr=pred[itrBatch, :, :, :, itrChannels], affineArrLst=np.eye(4), fileName='pred_' + curFileName, dir2SaveData=os.path.join(modelDirPath, 'pred'))

    tf.keras.backend.clear_session()

def continueTrainModel3D(inputDir: list, targetDir:list, modelDir: str, foldLst: list = None, model: str = 'CNN', loss: str = 'BCE', nbrEpochs: int = 150, cv: bool = False, validInputDir: list = None, validTargetDir: list = None, datagenParams: dict = None, datagenParamsValid: dict = None, modelParams: dict = None, gpuLst: list = None):
    # update datagenparams and modelparams based on input
    datagenParams, modelParams = updateParams(datagenParams, modelParams, inputDir, targetDir)
    datagenParamsValid, _ = updateParams(datagenParamsValid, modelParams, inputDir, targetDir)

    # split your datasets - calculate folds
    multiFactor = max(len(inputDir), len(targetDir)) // min(len(inputDir), len(targetDir))

    modelDirPath = modelDir.split("checkpoint.hdf5")[0]
    modelDir = modelDirPath + modelDir.split("checkpoint.hdf5")[-1]

    if cv and not validInputDir:
        partitionInput, partitionTarget = createDatasets(inputDir, targetDir, trainFlag=True, cv=True)

        for itrFold in sorted(foldLst):
            gc.collect()

            # merge all the data from the several channels together
            partitionInputSum, partitionTargetSum = [], []

            for itrChannels in range(multiFactor):
                partitionInputSum.append(partitionInput[itrChannels][itrFold])
                partitionTargetSum.append(partitionTarget[itrChannels][itrFold])

            # Set the corresponding directories
            modelDirTemp, logDir, xaiDir = makeModelDir(modelDir, modelParams, datagenParams, partitionInputSum, (itrFold + 1))

            modelDirFold = os.path.join(modelDirTemp, 'Fold' + str(itrFold + 1))
            if not os.path.exists(modelDirFold):
                os.mkdir(modelDirFold)

            # create the datasets for plotting
            tmpTrainLst, tmpValidLst, tmpTestLst = wrapPrepare(multiFactor, partitionInputSum, partitionTargetSum, partitionInputSum, partitionTargetSum, datagenParams, cv=True, external=False, train=True)

            curTrainFoldInput_train = [d['train'] for d in partitionInputSum]
            curTrainFoldTarget_train = [d['train'] for d in partitionTargetSum]
            curTrainFoldInput_valid = [d['valid'] for d in partitionInputSum]
            curTrainFoldTarget_valid = [d['valid'] for d in partitionTargetSum]

            # create the generators
            trainGen = DataGenerator(curTrainFoldInput_train, curTrainFoldTarget_train, **datagenParams)
            validGen = DataGenerator(curTrainFoldInput_valid, curTrainFoldTarget_valid, **datagenParamsValid)

            # callbacks
            callbackSummary1 = getCallback(modelDir=modelDirFold, logDir=logDir)

            callbackPred = plotMetricsOnEpochEnd(train_data=[tmpTrainLst[0], tmpTrainLst[-1]],
                                                 valid_data=[tmpValidLst[0], tmpValidLst[-1]],
                                                 test_data=[tmpTestLst[0], tmpTestLst[-1]],
                                                 dir2Save=str(xaiDir),
                                                 idx1Slice=60, idx2Slice=70, idx3Slice=80,
                                                 patchFlag=datagenParams['patch'],
                                                 batchSize=datagenParams['batchSize'],
                                                 origSize=datagenParams['dim'][0],
                                                 inChannelLen=datagenParams['nbrInChannel'],
                                                 outChannelLen=datagenParams['nbrOutChannel'])

            # one model instance
            loadWeightsAndRetrain(modelPath=modelDirPath, modelDir=modelDir, modelName=model, loss_name=loss, trainGen=trainGen, validGen=validGen, nbrEpochs=nbrEpochs, tfCallbacks=[callbackSummary1, callbackPred], modelParams=modelParams, gpuLst=gpuLst)

            tf.keras.backend.clear_session()

    elif not cv and not validInputDir:
        partitionInput, partitionTarget = createDatasets(inputDir, targetDir, trainFlag=True, cv=False)

        trainLst, testLst, others = createFullDataLst(partitionInput, partitionTarget, multiFactor, modelDir, modelParams, datagenParams, trainFlag=True, mode='partial', modelDirFlag=None, logDirFlag=None)

        curTrainFoldInput_train, curTrainFoldTarget_train = trainLst[0], trainLst[-1]
        curTrainFoldInput_valid, curTrainFoldTarget_valid = testLst[0], testLst[-1]
        modelDirFold, logDir = others[0], others[-1]

        # create the generators
        trainGen = DataGenerator(curTrainFoldInput_train, curTrainFoldTarget_train, **datagenParams)
        validGen = DataGenerator(curTrainFoldInput_valid, curTrainFoldTarget_valid, **datagenParamsValid)

        # create the datasets for plotting
        tmpTrainLst, tmpValidLst, tmpTestLst = wrapPrepare(multiFactor, curTrainFoldInput_train, curTrainFoldTarget_train, curTrainFoldInput_valid, curTrainFoldTarget_valid, datagenParams, cv=False, external=False, train=True)

        # callbacks
        callbackSummary1 = getCallback(modelDir=modelDirFold, logDir=logDir)

        callbackPred = plotMetricsOnEpochEnd(train_data=[tmpTrainLst[0], tmpTrainLst[-1]],
                                             valid_data=[tmpValidLst[0], tmpValidLst[-1]],
                                             test_data=[tmpTestLst[0], tmpTestLst[-1]],
                                             dir2Save=str(modelDirFold + '/xai'),
                                             idx1Slice=60, idx2Slice=70, idx3Slice=80,
                                             patchFlag=datagenParams['patch'],
                                             batchSize=datagenParams['batchSize'],
                                             origSize=datagenParams['dim'][0],
                                             inChannelLen=datagenParams['nbrInChannel'],
                                             outChannelLen=datagenParams['nbrOutChannel'])

        # one model instance
        loadWeightsAndRetrain(modelPath=modelDirPath, modelDir=modelDir, modelName=model, loss_name=loss, trainGen=trainGen, validGen=validGen, nbrEpochs=nbrEpochs, tfCallbacks=[callbackSummary1, callbackPred], modelParams=modelParams, gpuLst=gpuLst)

        tf.keras.backend.clear_session()

    elif not cv and validInputDir:
        partitionInput, partitionTarget = createDatasets(inputDir, targetDir, trainFlag=True, cv=False)
        partitionInputValid, partitionTargetValid = createDatasets(validInputDir, validTargetDir, trainFlag=True, cv=False)

        trainLst, others = createFullDataLst(partitionInput, partitionTarget, multiFactor, modelDir, modelParams, datagenParams, trainFlag=True, mode='full', modelDirFlag=None, logDirFlag=None)
        testLst, _ = createFullDataLst(partitionInputValid, partitionTargetValid, multiFactor, modelDir, modelParams, datagenParams, trainFlag=True, mode='full', modelDirFlag=others[0], logDirFlag=others[-1])

        curTrainFoldInput_train, curTrainFoldTarget_train = trainLst[0], trainLst[-1]
        curTrainFoldInput_valid, curTrainFoldTarget_valid = testLst[0], testLst[-1]
        modelDirFold, logDir = others[0], others[-1]

        # create the generators
        trainGen = DataGenerator(curTrainFoldInput_train, curTrainFoldTarget_train, **datagenParams)
        validGen = DataGenerator(curTrainFoldInput_valid, curTrainFoldTarget_valid, **datagenParamsValid)

        # create the datasets for plotting
        tmpTrainLst, tmpValidLst, tmpTestLst = wrapPrepare(multiFactor, curTrainFoldInput_train, curTrainFoldTarget_train, curTrainFoldInput_valid, curTrainFoldTarget_valid, datagenParams, cv=False, external=True, train=True)

        # callbacks
        callbackSummary1 = getCallback(modelDir=modelDirFold, logDir=logDir)

        callbackPred = plotMetricsOnEpochEnd(train_data=[tmpTrainLst[0], tmpTrainLst[-1]],
                                             valid_data=[tmpValidLst[0], tmpValidLst[-1]],
                                             test_data=[tmpTestLst[0], tmpTestLst[-1]],
                                             dir2Save=str(modelDirFold + '/xai'),
                                             idx1Slice=60, idx2Slice=70, idx3Slice=80,
                                             patchFlag=datagenParams['patch'],
                                             batchSize=datagenParams['batchSize'],
                                             origSize=datagenParams['dim'][0],
                                             inChannelLen=datagenParams['nbrInChannel'],
                                             outChannelLen=datagenParams['nbrOutChannel'])

        # one model instance
        loadWeightsAndRetrain(modelPath=modelDirPath, modelDir=modelDir, modelName=model, loss_name=loss, trainGen=trainGen, validGen=validGen, nbrEpochs=nbrEpochs, tfCallbacks=[callbackSummary1, callbackPred], modelParams=modelParams, gpuLst=gpuLst)

        tf.keras.backend.clear_session()
