import os.path

from helpers.patchSampling import sample_patches
from postprocessing.utilsArrayProcessing import *
from postprocessing.utilsNiftiProcessing import *

import numpy as np

def create_patches(inputArray, patchSize, idxCropLst=None):
    x = []

    for itrChannel in range(inputArray.shape[-1]):
        curArray = inputArray[:, :, :, :, itrChannel]
        curArray = np.expand_dims(curArray, axis=-1)

        if itrChannel == 0 and idxCropLst is None:
            tempOut, idxCropLst = removeBackground_np(curArray, modelReady=True)

        else:
            tempOut, _ = removeBackground_np(curArray, modelReady=True, cropIdx=idxCropLst)

        x.append(tempOut)

    outputPatch = get_patches(img_arr=np.concatenate(x, axis=-1), size=patchSize, stride=patchSize, modelReady=True)

    return outputPatch, idxCropLst

def unitcheck_samplePatches(case: float, A: np.ndarray = None, B: np.ndarray = None):

    if case == 1.0:
        # input has pos and neg value - no augmentation
        inputArr = np.zeros(shape=(2, 64, 64, 64, 2), dtype=float)
        targerArr = np.zeros(shape=(2, 64, 64, 64, 1), dtype=float)
        targerArr[0, 10:20, 10:20, 10:20, :] = 1
        targerArr[0, 25:30, 25:30, 25:30, :] = 1 # second patch test
        nbrAug = None

    elif case == 1.1:
        # input has pos and neg value - and augmentation
        inputArr = np.zeros(shape=(2, 32, 32, 32, 2), dtype=float)
        targerArr = np.zeros(shape=(2, 32, 32, 32, 1), dtype=float)
        targerArr[:, 10:20, 10:20, 10:20, :] = 1
        nbrAug = 2

    elif case == 2.0:
        # input has only pos but no neg value - no augmentation
        inputArr = np.zeros(shape=(2, 32, 32, 32, 2), dtype=float)
        targerArr = np.zeros(shape=(2, 32, 32, 32, 1), dtype=float)
        targerArr[:, :, :, :, :] = 1
        nbrAug = None

    elif case == 2.1:
        # input has only pos but no neg value - and augmentation
        inputArr = np.zeros(shape=(2, 32, 32, 32, 2), dtype=float)
        targerArr = np.zeros(shape=(2, 32, 32, 32, 1), dtype=float)
        targerArr[:, :, :, :, :] = 1
        nbrAug = 2

    elif case == 3.0:
        # input has only neg but no pos value - no augmentation
        inputArr = np.zeros(shape=(2, 32, 32, 32, 2), dtype=float)
        targerArr = np.zeros(shape=(2, 32, 32, 32, 1), dtype=float)
        nbrAug = None

    elif case == 3.1:
        # input has only neg but no pos value - and augmentation
        inputArr = np.zeros(shape=(2, 32, 32, 32, 2), dtype=float)
        targerArr = np.zeros(shape=(2, 32, 32, 32, 1), dtype=float)
        nbrAug = 2

    elif case == 4.0:
        A = np.expand_dims(np.expand_dims(A.copy(), axis=0), axis=-1)
        B = np.expand_dims(np.expand_dims(B.copy(), axis=0), axis=-1)

        inputArr, cropLst = create_patches(inputArray=A.copy(), patchSize=32, idxCropLst=None)
        targetArr, _ = create_patches(inputArray=B.copy(), patchSize=32, idxCropLst=cropLst)

        nbrAug = None

    sample_patches(inputPatches=inputArr.copy(), targetPatches=targetArr.copy(), nbrAugmentations=nbrAug)

def main():
    # unitcheck_samplePatches(case=1.0)
    # unitcheck_samplePatches(case=1.1)
    # unitcheck_samplePatches(case=2.0)
    # unitcheck_samplePatches(case=2.1)
    # unitcheck_samplePatches(case=3.0)
    # unitcheck_samplePatches(case=3.1)

    dir1 = '/home/glappas/PycharmProjects/Projects/ACTIVITY_retro/data/Bochum/Summary/GadPos/FLAIR/'
    dir2 = '/home/glappas/PycharmProjects/Projects/ACTIVITY_retro/data/Bochum/Summary/GadPos/T1/'
    dir3 = '/home/glappas/PycharmProjects/Projects/ACTIVITY_retro/data/Bochum/Summary/GadPos/mask/'

    for itr in range(len(sorted(os.listdir(dir1)))):
        inArr, _ = readNifti2Numpy(os.path.join(dir1, sorted(os.listdir(dir1))[itr]))
        inArr, _ = fixedSize(currentArray=inArr.copy(), desiredSize=(256, 256, 256))
        tarArr, _ = readNifti2Numpy(os.path.join(dir3, sorted(os.listdir(dir3))[itr]))
        tarArr, _ = fixedSize(currentArray=tarArr.copy(), desiredSize=(256, 256, 256))

        unitcheck_samplePatches(case=4.0, A=inArr.copy(), B=tarArr.copy())

if __name__ == '__main__':
    main()