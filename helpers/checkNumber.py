def is_divisible_by_power_of_two(number):
    powers_of_two = [2 ** i for i in range(1, 11)]  # You can adjust the range of powers as needed

    for power in powers_of_two:
        if number % power == 0:
            return True, powers_of_two

    return False, powers_of_two

def times_is_divisible(number2Check, maxLimit):
    powers_of_two = [2 ** i for i in range(1, maxLimit)]  # You can adjust the range of powers as needed
    powers_of_two_check = []
    checkFlag = False

    for pwr2 in powers_of_two:
        if number2Check % pwr2 != 0:
            checkFlag = True

    if checkFlag:
        for number in range(1, 1001):  # Change the range as needed
            if all(number % divisor == 0 for divisor in powers_of_two):
                powers_of_two_check.append(number)

        # next available number
        number2Check = min(num for num in powers_of_two_check if num > number2Check)

    return number2Check

def find_largest_divisor_with_remainder(number, limit=10):
    for divisor in range(min(number - 1, limit), 0, -1):
        if number % divisor == 0 and divisor % 2 == 0:
            return divisor

    return 1  # No divisor found