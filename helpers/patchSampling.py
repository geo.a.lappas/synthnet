from postprocessing.utilsArrayProcessing import *

import gc
import numpy as np

# create balanced dataset - using patches
def sampleEqualPatches(inputPatches: np.ndarray, targetPatches:np.ndarray):
    # choose non-zeros class (works only for binary classification)
    # TODO add description
    # TODO for multiclass convert each class into a binary task
    nbrPositivePatches = np.count_nonzero(targetPatches[:, -1])
    nbrNegativePatches = np.count_nonzero(targetPatches[:, 0])

    if nbrPositivePatches < nbrNegativePatches:
        # input
        posPatchesInput = inputPatches[targetPatches[:, -1] > 0]
        negPatchesInput = inputPatches[targetPatches[:, 0] > 0][:nbrPositivePatches]
        mergedPatchesInput = np.concatenate(np.array([posPatchesInput, negPatchesInput]), axis=0)

        # target
        posPatchesTarget = targetPatches[targetPatches[:, -1] > 0]
        negPatchesTarget = targetPatches[targetPatches[:, 0] > 0][:nbrPositivePatches]
        mergedPatchesTarget = np.concatenate(np.array([posPatchesTarget, negPatchesTarget]), axis=0)

    if nbrPositivePatches >= nbrNegativePatches:
        # input
        posPatchesInput = inputPatches[targetPatches[:, -1] > 0][:nbrNegativePatches]
        negPatchesInput = inputPatches[targetPatches[:, 0] > 0]
        mergedPatchesInput = np.concatenate(np.array([posPatchesInput, negPatchesInput]), axis=0)

        # target
        posPatchesTarget = targetPatches[targetPatches[:, -1] > 0][:nbrNegativePatches]
        negPatchesTarget = targetPatches[targetPatches[:, 0] > 0]
        mergedPatchesTarget = np.concatenate(np.array([posPatchesTarget, negPatchesTarget]), axis=0)

    return mergedPatchesInput, mergedPatchesTarget

def sample_patches(inputPatches: np.ndarray, targetPatches: np.ndarray, nbrAugmentations: int = None):
        # choose non-zeros class (works only for binary classification - input channel is independent)
        # TODO add description
        if not isinstance(inputPatches, np.ndarray):
            raise AssertionError('inputPatches should be type np.ndarray')

        if not isinstance(targetPatches, np.ndarray):
            raise AssertionError('targetPatches should be type np.ndarray')

        posPatchesInput, posPatchesTarget, negPatchesInput, negPatchesTarget = [], [], [], []

        for itrPositiveClass in range(targetPatches.shape[0]):
            if targetPatches[itrPositiveClass].max() > 0:
                posPatchesInput.append(inputPatches[itrPositiveClass])
                posPatchesTarget.append(targetPatches[itrPositiveClass])

            else:
                negPatchesInput.append(inputPatches[itrPositiveClass])
                negPatchesTarget.append(targetPatches[itrPositiveClass])

        # convert everything into arrays
        posInputArr = np.array(posPatchesInput)
        posTargetArr = np.array(posPatchesTarget)
        negInputArr = np.array(negPatchesInput)
        negTargetArr = np.array(negPatchesTarget)

        del posPatchesInput, posPatchesTarget, negPatchesInput, negPatchesTarget
        gc.collect()

        # get an equal amount of negative patches randomly (nr_objects)
        nbrPosClass = posInputArr.shape[0]
        nbrNegClass = negInputArr.shape[0] # is this zero?

        # oversampled and undersampled class
        randomIndices = np.random.randint(low=0, high=max(nbrPosClass, nbrNegClass), size=min(nbrPosClass, nbrNegClass))
        itrRange = min(nbrPosClass, nbrNegClass)

        # take care of multi input channel and sample a random number of patches
        if nbrPosClass == min(nbrPosClass, nbrNegClass):
            equalSamplingInput_neg = []
            equalSamplingTarget_neg = []

            if nbrPosClass > 0:
                for nbrPatches in range(itrRange):
                    equalSamplingInput_neg.append(negInputArr[randomIndices[nbrPatches]])
                    equalSamplingTarget_neg.append(negTargetArr[randomIndices[nbrPatches]])

                negInputArr = np.array(equalSamplingInput_neg)
                negTargetArr = np.array(equalSamplingTarget_neg)

                del equalSamplingInput_neg, equalSamplingTarget_neg

            else:
                negInputArr = negInputArr[0]
                negTargetArr = negTargetArr[0]

        elif nbrNegClass == min(nbrPosClass, nbrNegClass) or nbrNegClass == nbrPosClass:
            equalSamplingInput_pos = []
            equalSamplingTarget_pos = []

            if nbrNegClass > 0:
                for nbrPatches in range(itrRange):
                    equalSamplingInput_pos.append(posInputArr[randomIndices[nbrPatches]])
                    equalSamplingTarget_pos.append(posTargetArr[randomIndices[nbrPatches]])

                posInputArr = np.array(equalSamplingInput_pos)
                posTargetArr = np.array(equalSamplingTarget_pos)

                del equalSamplingInput_pos, equalSamplingTarget_pos

            else:
                posInputArr = posInputArr[0]
                posTargetArr = posTargetArr[0]

        if nbrAugmentations is not None:
            if nbrPosClass > 0 and nbrNegClass > 0:
                # apply the augmentation here:
                augmInput = []
                augmTarget = []

                for itrAug in range(nbrAugmentations):
                    inputsTemp, targetsTemp = randomRotationPair(posInputArr.copy(), posTargetArr.copy(), angle_range=(-10, 10))
                    inputsTemp, targetsTemp = randomTranslationPair(inputsTemp.copy(), targetsTemp.copy(), translation_range=(-5, 5))

                    noise_factor = .05  # was .2

                    # Generate noise and add the noise to inputs
                    inputsTemp = inputsTemp + noise_factor * np.random.normal(size=inputsTemp.copy().shape)
                    inputsTemp = np.clip(inputsTemp.copy(), a_min=0., a_max=1.)

                    augmInput.append(inputsTemp)
                    augmTarget.append(targetsTemp)

                mergedPatchesInput = np.concatenate(np.array([np.concatenate(augmInput, axis=0), negInputArr]), axis=0)
                mergedPatchesTarget = np.concatenate(np.array([np.concatenate(augmTarget, axis=0), negTargetArr]), axis=0)

            elif nbrPosClass > 0 and nbrNegClass <= 0:
                mergedPatchesInput = posInputArr
                mergedPatchesTarget = posTargetArr

            elif nbrPosClass <= 0 and nbrNegClass > 0:
                mergedPatchesInput = negInputArr
                mergedPatchesTarget = negTargetArr

        else:
            if nbrPosClass > 0 and nbrNegClass > 0:
                # merge them
                mergedPatchesInput = np.concatenate(np.array([posInputArr, negInputArr]), axis=0)
                mergedPatchesTarget = np.concatenate(np.array([posTargetArr, negTargetArr]), axis=0)

            elif nbrPosClass > 0 and nbrNegClass <= 0:
                mergedPatchesInput = posInputArr
                mergedPatchesTarget = posTargetArr

            elif nbrPosClass <= 0 and nbrNegClass > 0:
                mergedPatchesInput = negInputArr
                mergedPatchesTarget = negTargetArr

        if mergedPatchesInput.ndim < 5:
            mergedPatchesInput = np.expand_dims(mergedPatchesInput.copy(), axis=0)
            mergedPatchesTarget = np.expand_dims(mergedPatchesTarget.copy(), axis=0)

        return mergedPatchesInput, mergedPatchesTarget

def create_patches(inputArray, patchSize, idxCropLst=None):
    x = []

    for itrChannel in range(inputArray.shape[-1]):
        curArray = inputArray[:, :, :, itrChannel]
        curArray = np.expand_dims(curArray, axis=0)  # batch dimension
        curArray = np.expand_dims(curArray, axis=-1)  # channel dimension

        if itrChannel == 0 and idxCropLst is None:
            tempOut, idxCropLst = removeBackground_np(curArray, modelReady=True)

        else:
            tempOut, _ = removeBackground_np(curArray, modelReady=True, cropIdx=idxCropLst)

        x.append(tempOut)

    outputPatch = get_patches(img_arr=np.concatenate(x, axis=-1), size=patchSize, stride=patchSize, modelReady=True)

    return outputPatch, idxCropLst

def create_patches2(inputArray, patchSize):

    if inputArray.ndim == 4:
        inputArray = np.expand_dims(inputArray, axis=0)

    outputPatch = []

    for itrBatch in range(inputArray.shape[0]):
        curArray = inputArray[itrBatch]

        if curArray.ndim == 4:
            curArray = np.expand_dims(curArray, axis=0)

        outputPatch.append(get_patches(img_arr=curArray, size=patchSize, stride=patchSize, modelReady=True))

    return np.array(outputPatch)

def merge_patches(patch_arr: np.ndarray, origSize: int, batchSize: int, patchSize: int, stride: int, channel: int, modelReady: bool, patchClass: np.ndarray = None):
    """
    Takes patches or array of images and merges them
    If stride < size it will do overlapping.

    Returns:
        numpy.ndarray: [description]
    """
    # check size and stride
    if patchSize % stride != 0:
        raise ValueError("size % stride must be equal 0")

    overlapping = 0
    if stride != patchSize:
        overlapping = (patchSize // stride) - 1

    # number of patches and iterator
    itr_max = (origSize // stride) - overlapping

    # reshape patches
    # patch_arr = np.reshape(patch_arr.copy(), (batchSize, origSize, origSize, origSize, channel))

    # Create an array to store the reconstructed image
    image_array = np.zeros(shape=(batchSize, origSize, origSize, origSize, channel))

    if patchClass is not None:
        heatmap_array = np.zeros(shape=image_array.shape)
        # Create arr2 by reshaping and broadcasting
        patchClassTemp = patchClass[:, np.newaxis, np.newaxis, np.newaxis, np.newaxis]

        # Expand dimensions for broadcasting
        patchClassTemp = np.repeat(patchClassTemp, patch_arr.shape[1], axis=1)
        patchClassTemp = np.repeat(patchClassTemp, patch_arr.shape[2], axis=2)
        patchClassTemp = np.repeat(patchClassTemp, patch_arr.shape[3], axis=3)

        # Expand last dimension to desired shape
        patchClassTemp = np.repeat(patchClassTemp, patch_arr.shape[-1], axis=-1)

    if patch_arr.ndim == 5 and modelReady:
        for itrBatch in range(batchSize):
            for itrChannel in range(channel):
                cnt = 0

                for i in range(itr_max):
                    for j in range(itr_max):
                        for k in range(itr_max):
                            image_array[itrBatch, i * stride: i * stride + patchSize, j * stride: j * stride + patchSize, k * stride: k * stride + patchSize, itrChannel] = patch_arr[cnt, :, :, :, itrChannel]

                            if patchClass is not None:
                                heatmap_array[itrBatch, i * stride: i * stride + patchSize, j * stride: j * stride + patchSize, k * stride: k * stride + patchSize, itrChannel] = patchClassTemp[cnt, :, :, :, itrChannel]

                            cnt += 1

    if patchClass is None:
        return image_array

    if patchClass is not None:
        return image_array, heatmap_array
#
# if __name__ == '__main__':
#     test1, test2 = merge_patches(patch_arr=np.ones(shape=(245, 32, 32, 32, 1)), origSize=256, batchSize=1, patchSize=32,
#                                  stride=32, channel=1, modelReady=True, patchClass=np.ones(shape=(245, )))