from concurrent.futures import ThreadPoolExecutor
from postprocessing.utilsArrayProcessing import normalizeArray, fillNanArray

import os
import numpy as np
import nibabel as nib

def find_balance_between_classes(directory_path):
    # Get a list of all NIfTI files in the directory
    nifti_files = [f for f in os.listdir(directory_path) if f.endswith('.nii') or f.endswith('.nii.gz')]

    cel_pos = 0
    cel_neg = 0

    # Loop through all NIfTI files
    for file_name in nifti_files:
        file_path = os.path.join(directory_path, file_name)

        # Load the NIfTI file as a NumPy array
        nifti_array = nib.load(file_path).get_fdata()

        if np.count_nonzero(nifti_array) > 0:
            cel_pos += 1
        else:
            cel_neg += 1

    print('CEL positive cases are', cel_pos)
    print('CEL negative cases are', cel_neg)

def call_find_min_max_indices(directory_path):
    nifti_files = [os.path.join(directory_path, f) for f in os.listdir(directory_path) if f.endswith('.nii') or f.endswith('.nii.gz')]

    min_indices = [np.inf, np.inf, np.inf]
    max_indices = [-np.inf, -np.inf, -np.inf]

    with ThreadPoolExecutor() as executor:
        results = list(executor.map(find_min_max_indices, nifti_files))

    for result in results:
        curXMin, curYMin, curZMin, curXMax, curYMax, curZMax = result

        if curXMin < min_indices[0]:
            min_indices[0] = curXMin

        if curYMin < min_indices[1]:
            min_indices[1] = curYMin

        if curZMin < min_indices[2]:
            min_indices[2] = curZMin

        if curXMax > max_indices[0]:
            max_indices[0] = curXMax

        if curYMax > max_indices[1]:
            max_indices[1] = curYMax

        if curZMax > max_indices[2]:
            max_indices[2] = curZMax

    maxSizeAxis1 = max_indices[0] - min_indices[0]
    maxSizeAxis2 = max_indices[1] - min_indices[1]
    maxSizeAxis3 = max_indices[2] - min_indices[2]
    maxAll = max(max_indices)

    return [maxSizeAxis1, maxSizeAxis2, maxSizeAxis3], maxAll
def find_min_max_indices(file_path):
    nifti_array = nib.load(file_path).get_fdata()

    currentArray = fillNanArray(nifti_array.copy())
    inputArray, _ = normalizeArray(currentArray.copy())

    # removing any pixels outside of brain region
    brainMask = np.where(inputArray > 0.05, 1.0, 0.0)

    X, Y, Z = np.nonzero(brainMask)

    return X.min(), Y.min(), Z.min(), X.max(), Y.max(), Z.max()
