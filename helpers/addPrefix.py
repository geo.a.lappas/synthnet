import os

def rename_files_with_prefix(directory_path, prefix):
    # Get a list of all files in the directory
    file_list = os.listdir(directory_path)

    # Iterate through each file and rename it with the prefix
    for filename in file_list:
        if not filename.startswith(prefix):
            # Construct the new filename with the prefix
            new_filename = prefix + filename

            # Get the full path of the original and new filenames
            original_path = os.path.join(directory_path, filename)
            new_path = os.path.join(directory_path, new_filename)

            # Rename the file
            os.rename(original_path, new_path)

def changeNameWith(directory_path, term2replace, term2add):
    for filename in os.listdir(directory_path):
        if filename.endswith(".nii") or filename.endswith(".nii.gz"):
            new_filename = filename.replace(term2replace, term2add)
            os.rename(os.path.join(directory_path, filename), os.path.join(directory_path, new_filename))

if __name__ == "__main__":
    # # Replace 'directory_path' with the path to your directory containing nifti files
    directory_path = '/home/glappas/PycharmProjects/Projects/ACTIVITY_retro/data/Basel/GadPos/T1/'
    # prefix = 'run01_'

    # Call the function to rename the files with the specified prefix
    # rename_files_with_prefix(directory_path, prefix)

    changeNameWith(directory_path, term2replace='2T1ce', term2add='2T1ce_')
