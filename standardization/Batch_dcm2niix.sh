#!/bin/bash

# remember to change the directory below
# the folders should have the following structure
# data-->
# 	pat01-->
# 		tp1, tp2, ..., tpN-->
# 					seq1, seq2, ..., seqN

dirStruct=$(find /home/glappas/Data/testFolder/ -mindepth 3 -maxdepth 3 -type d)

COUNTER=1
for dir in ''$dirStruct''
do
  echo ''$COUNTER''
  COUNTER=$[$COUNTER +1]
  dcm2niix ''$dir''
done
