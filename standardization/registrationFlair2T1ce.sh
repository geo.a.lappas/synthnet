#!/bin/bash

# 1. Recursively get the files under the directory
DATA=''/data/sbig/projects/activity/ACTIVITY_retro/data/Bochum/overfitData_BIDS/BET/FLAIR_N4BF/*.nii.gz''
DATA2=''/data/sbig/projects/activity/ACTIVITY_retro/data/Bochum/overfitData_BIDS/BET/T1ce_N4BF/*.nii.gz''

# 2. apply registration of T1c and corresponding mask in MNI template using FLIRT-FSL
COUNTER=1

STR_OUT=''_FLAIR2T1space.nii.gz''
STR_MAT=''_FLAIR2T1space.mat''

for dirIdx in ''${!DATA[@]}''
do
# dir nbr 1
FILENAME_DATA=${DATA[$dirIdx]}
FILENAME_DATA=($FILENAME_DATA)
# dir nbr 2
FILENAME_DATA2=${DATA2[$dirIdx]}
FILENAME_DATA2=($FILENAME_DATA2)

for itr1 in ''${!FILENAME_DATA[@]}''
  do
    # echo ''$COUNTER''
    COUNTER=$[$COUNTER +1]
    # split name to remove suffix
    NIFTI_NAME=(${FILENAME_DATA[itr1]//.nii.gz/ })
    STR_OUT_NAME=''${NIFTI_NAME[0]}$STR_OUT''
    STR_OUT_NAME_MAT=''${NIFTI_NAME[0]}$STR_MAT''

  # register FLAIR to T1w image
  flirt -in ''${FILENAME_DATA[itr1]}'' -ref ''${FILENAME_DATA2[itr1]}'' -omat ''$STR_OUT_NAME_MAT'' -out ''$STR_OUT_NAME'' -dof 12 -bins 256 -cost corratio -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -dof 12  -interp trilinear
  done
done



