from postprocessing.utilsNiftiProcessing import readNifti2Numpy
from pathlib import Path

import nibabel as nib
import os

def convertMydata2nnUnet(inputDir:str, outputDir: str, datatype:str, projectName: str, sequenceID: str):
    '''
        Converting data names to be compatible with nnUnet. Support one sequence at a time.

        Example:
        from: images/BHS_sub1 or labels/BHS_sub1
        to: images/BHS_001_0000 or labels/BHS_001

        Parameters
        ----------
        inputDir: directory where the input is saved
        outputDir: directory where the output will be saved
        datatype: string defining the type of input, either 'images' or 'labels'
        projectName: string defining the project name to be added as prefix
        sequenceID: string defining the sequence ID. E.g., T1 --> 0000, FLAIR --> 0001, T1ce --> 0002

        Returns: None
        -------
        '''

    if not isinstance(inputDir, str):
        raise AssertionError('The inputDir should be a string defining the directory of the input data.')

    if not isinstance(outputDir, str):
        raise AssertionError('The outputDir should be a string defining the directory of the output data.')

    if not isinstance(datatype, str):
        raise AssertionError('The datatype should be a string defining the type of the data, either images or labels')

    if not isinstance(projectName, str):
        raise AssertionError('The projectName should be a string defining the project name.')

    if not isinstance(sequenceID, str):
        raise AssertionError('The sequenceID should be a string defining the sequence ID. E.g., T1 --> 0000, FLAIR --> 0001, T1ce --> 0002')

    inputData = sorted(os.listdir(inputDir))

    workingDir = Path(outputDir)
    if not os.path.exists(workingDir):
        print('Directory does not exist but it will be created.')
        workingDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    for itrData in range(len(inputData)):
        namePrefixProj = inputData[itrData].split(projectName + '_sub')
        namePrefixFinal = namePrefixProj[-1].split('.nii.gz')
        nameFinal = int(namePrefixFinal[0])

        if nameFinal <= 99 and nameFinal <= 9:
            nameFinalOut = '00' + str(nameFinal)

        if nameFinal <= 99 and nameFinal > 9:
            nameFinalOut = '0' + str(nameFinal)

        if nameFinal > 99:
            nameFinalOut = str(nameFinal)

        curData, curAffineData = readNifti2Numpy(os.path.join(inputDir, inputData[itrData]))
        niftiCurFile = nib.Nifti2Image(curData, affine=curAffineData)

        if datatype == 'images':
            niftiCurFileSavePath = os.path.join(outputDir, projectName + '_' + nameFinalOut + '_' + sequenceID + '.nii.gz')

        if datatype == 'labels':
            niftiCurFileSavePath = os.path.join(outputDir, projectName + '_' + nameFinalOut + '.nii.gz')

        nib.save(niftiCurFile, niftiCurFileSavePath)

        print('Case', str(itrData+1), 'has been processed and saved.')


if __name__ == '__main__':

    convertMydata2nnUnet(inputDir='/home/glappas/Desktop/BlackHoleSeg/FinalDir/labels/',
                         outputDir='/home/glappas/Desktop/BlackHoleSeg/FinalDir/nnUnet_BHS_label/',
                         datatype='labels',
                         projectName='BHS',
                         sequenceID='0000')
