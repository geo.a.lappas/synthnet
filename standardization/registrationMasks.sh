#!/bin/bash

# 1. Recursively get the files under the directory

DATA=''/home/glappas/Data/TestClinicalData/Preprocessed/AdditionalData/labels/*.nii.gz''
DATA2=''/home/glappas/Data/TestClinicalData/Preprocessed/AdditionalData/imagesSkullStripped_Original/*.mat''

# 2. apply registration of T1c and corresponding mask in MNI template using FLIRT-FSL
# setup fsl first
FSLDIR=/home/appmin/apps/fsl-5.0.11;
PATH=${FSLDIR}/bin:${PATH};
export FSLDIR PATH;
source ${FSLDIR}/etc/fslconf/fsl.sh

COUNTER=1
STR_OUT=''_MNI.nii.gz''

for dirIdx in ''${!DATA[@]}''
do
  # dir nbr 1
  FILENAME_DATA=${DATA[$dirIdx]}
  FILENAME_DATA=($FILENAME_DATA)
  # dir nbr 2
  FILENAME_DATA2=${DATA2[$dirIdx]}
  FILENAME_DATA2=($FILENAME_DATA2)

  for itr1 in ''${!FILENAME_DATA[@]}''
  do
    echo ''$COUNTER''
    COUNTER=$[$COUNTER +1]
    # split name to remove suffix
    NIFTI_NAME=(${FILENAME_DATA[itr1]//.nii.gz/ })
    STR_OUT_NAME=''${NIFTI_NAME[0]}$STR_OUT''
    echo "${FILENAME_DATA2[itr1]}"
    # registering the label using the transformation matrix from T1w registration to MNI brain template
    flirt -in ''${FILENAME_DATA[itr1]}'' -ref /home/appmin/apps/fsl-5.0.11/data/standard/MNI152_T1_1mm_brain.nii.gz -out ''$STR_OUT_NAME'' -dof 7 -init ${FILENAME_DATA2[itr1]} -applyxfm -interp nearestneighbour
  done
done
