from postprocessing.utilsNiftiProcessing import readNifti2Numpy
from pathlib import Path
from multiprocessing import Pool
from itertools import repeat

import nibabel as nib
import numpy as np
import os

POOL_WORKERS = 10

def convertNumpyArray2Nifti(numpyArr: np.ndarray, affineArr: np.ndarray, fileName: str, dir2SaveData: str):

    if not os.path.exists(dir2SaveData):
        os.mkdir(dir2SaveData)

    niftiCurFile = nib.Nifti1Image(numpyArr, affine=affineArr)

    niftiCurFileSavePath = os.path.join(dir2SaveData, fileName)

    nib.save(niftiCurFile, niftiCurFileSavePath)

    print('File has been saved.')

def ImageSubtraction(filename1, filename2):
    '''
    This function will perform image subtraction.

    :param filename1: file in nifti or ndarray format to be subtracted
    :param filename2: file in nifti or ndarray format to subtract
    :return: subtracted image in ndarray and affine transformation matrix to convert it to nifti
    '''

    ## NOTE: expects to have shape (#channels, dim1, dim2, ..., dim3)

    if not(isinstance(filename1, str) or isinstance(filename1, np.ndarray)):
        raise AssertionError('The input is not provided in nifti or np.ndarray.')

    if (isinstance(filename1, str)):
        if filename1.endswith('.nii') or filename1.endswith('.nii.gz'):
            origImg1, affineMatrix1 = readNifti2Numpy(filename1)

    elif isinstance(filename1, np.ndarray):
        origImg1 = filename1

    if not(isinstance(filename2, str) or isinstance(filename2, np.ndarray)):
        raise AssertionError('The input is not provided in nifti or np.ndarray.')

    if (isinstance(filename2, str)):
        if filename2.endswith('.nii') or filename2.endswith('.nii.gz'):
            origImg2, affineMatrix2 = readNifti2Numpy(filename2)

    elif isinstance(filename2, np.ndarray):
        origImg2 = filename2

    # NOTE if affine tranformation matrices are different raise error
    if affineMatrix1.any() != affineMatrix2.any():
        raise AssertionError('The affine transformation matrices for the two input are not the same.')

    # applying image subtraction (T1ce - T1)
    subImg = np.subtract(origImg1, origImg2)

    # output is torch tensor and conversion needs to be done
    return subImg, affineMatrix1

def binarizeInput(input, threshold):
    '''
    This function will convert the input into a binary mask based on the set threshold

    :param input: file in ndarray format to be binazized
    :return: normalized intensity nifti file
    '''

    ## NOTE: expects to have shape (#channels, dim1, dim2, ..., dim3)

    if not isinstance(input, np.ndarray):
        raise AssertionError('The input is not provided in np.ndarray.')

    if not isinstance(threshold, float):
        raise AssertionError('The threshold is not provided in float.')


    binarizedTemp = np.where(input > threshold, 1, 0)

    return binarizedTemp

def wrapImagesubtraction(dir2Apply1: str, dir2Apply2: str, dir2Save: str):
    """
    This is a wraper function to call ImageSubtraction function
    Params:
    dir2Apply1: file path to the directory of the input1
    dir2Apply2: file path to the directory of the input2
    dir2Save: file path to the directory of the output
    projectName: project name for the naming convention
    Returns: None
    """
    if not isinstance(dir2Apply1, str):
        raise AssertionError('dir2Apply1 should be a file path of the directory of the input')

    if not isinstance(dir2Apply2, str):
        raise AssertionError('dir2Apply2 should be a file path of the directory of the input')

    if not os.path.exists(dir2Save):
        # creating parent and child directories if needed
        savingDir = Path(dir2Save)
        savingDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    onlyFileLst1, onlyFileLst2 = [], []

    for fileItr in os.listdir(dir2Apply1):
        if fileItr.endswith(".nii.gz"):
            onlyFileLst1.append(fileItr)

    for fileItr in os.listdir(dir2Apply2):
        if fileItr.endswith(".nii.gz"):
            onlyFileLst2.append(fileItr)

    return sorted(onlyFileLst1), sorted(onlyFileLst2)

def poolImagesubtraction(idxPool1, idxPool2, dir2Apply1, dir2Apply2, dir2Save, thresholdVal):
    path1 = os.path.join(dir2Apply1, idxPool1)
    path2 = os.path.join(dir2Apply2, idxPool2)

    subTemp, affineTemp = ImageSubtraction(path1, path2)
    binarizedTemp = binarizeInput(subTemp, thresholdVal)

    convertNumpyArray2Nifti(numpyArr=binarizedTemp, affineArr=affineTemp, fileName=idxPool1, dir2SaveData=dir2Save)

    print('Image subtraction is complete')

if __name__ == '__main__':

    dirToApply1 = '/data/sbig/projects/activity/ACTIVITY_retro/data/Bochum/target_post/'
    dirToApply2 = '/data/sbig/projects/activity/ACTIVITY_retro/data/Bochum/target_pre/'
    dirToSave = '/data/sbig/projects/activity/ACTIVITY_retro/data/Bochum/T1_sub/'
    delta_threshold = 0.15

    fileLst2Run1, fileLst2Run2 = wrapImagesubtraction(dir2Apply1=dirToApply1, dir2Apply2=dirToApply2, dir2Save=dirToSave)

    with Pool(POOL_WORKERS) as p:
        p.starmap(poolImagesubtraction, zip(fileLst2Run1, fileLst2Run2, repeat(dirToApply1), repeat(dirToApply2), repeat(dirToSave), repeat(delta_threshold)))
