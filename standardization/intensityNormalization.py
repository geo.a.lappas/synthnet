from monai.transforms import NormalizeIntensity
from postprocessing.utilsNiftiProcessing import convertNumpyArray2Nifti, readNifti2Numpy
from pathlib import Path
from multiprocessing import Pool
from itertools import repeat

import numpy as np
import os

POOL_WORKERS = 10

def IntensityNormalization(filename):
    '''
    :param filename: file in nifti or ndarray format to be normalized
    :return: normalized intensity nifti file
    '''

    ## NOTE: expects to have shape (#channels, dim1, dim2, ..., dim3)

    if not(isinstance(filename, str) or isinstance(filename, np.ndarray)):
        raise AssertionError('The input is not provided in nifti or np.ndarray.')

    if (isinstance(filename, str)):
        if filename.endswith('.nii') or filename.endswith('.nii.gz'):
            origImg, affineMatrix = readNifti2Numpy(filename)

    elif isinstance(filename, np.ndarray):
        origImg = filename

    # applying histogram normalization
    normalizationLayer = NormalizeIntensity()
    normalizedImg = normalizationLayer(origImg)

    # output is torch tensor and conversion needs to be done
    return normalizedImg.numpy(), affineMatrix

def wrapIntensityNormalization(dir2Apply: str, dir2Save: str):
    """
    This is a function to perform intensity normalization using monai module.
    Params:
    dir2Apply: file path to the directory of the input
    dir2Save: file path to the directory of the output
    projectName: project name for the naming convention
    Returns: None
    """
    if not isinstance(dir2Apply, str):
        raise AssertionError('dir2Apply should be a file path of the directory of the input')

    if not os.path.exists(dir2Save):
        # creating parent and child directories if needed
        savingDir = Path(dir2Save)
        savingDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    onlyFileLst = []

    for fileItr in os.listdir(dir2Apply):
        if fileItr.endswith(".nii.gz"):
            onlyFileLst.append(fileItr)

    return onlyFileLst

def poolIntensityNormalization(idxPool, dir2Apply, dir2Save):
    path = os.path.join(dir2Apply, idxPool)
    normInTemp, affineTemp = IntensityNormalization(path)

    convertNumpyArray2Nifti(numpyArr=normInTemp, affineArrLst=affineTemp, fileName=idxPool, dir2SaveData=dir2Save)

    print('Intensity normalization transformation is complete')

if __name__ == '__main__':

    dirToApply = '/home/glappas/PycharmProjects/Projects/ACTIVITY_retro/data/Bochum/Summary/GadNeg/pp/BET/FLAIR/'
    dirToSave = '/home/glappas/PycharmProjects/Projects/ACTIVITY_retro/data/Bochum/Summary/GadNeg/pp/IN/FLAIR/'

    fileLst2Run = wrapIntensityNormalization(dir2Apply=dirToApply, dir2Save=dirToSave)

    with Pool(POOL_WORKERS) as p:
        p.starmap(poolIntensityNormalization, zip(fileLst2Run, repeat(dirToApply), repeat(dirToSave)))