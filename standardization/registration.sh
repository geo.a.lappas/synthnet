#!/bin/bash

# 1. Recursively get the files under the directory

dirStruct=''/home/glappas/Data/TestClinicalData/Proprocessed/ModelData/imagesSkullStripped/*.nii.gz''

# 2. apply registration of T1c and corresponding mask in MNI template using FLIRT-FSL
# setup fsl first
FSLDIR=/home/appmin/apps/fsl-5.0.11;
PATH=${FSLDIR}/bin:${PATH};
export FSLDIR PATH;
source ${FSLDIR}/etc/fslconf/fsl.sh

COUNTER=1

STR_MAT=''_MNI.mat''
STR_OUT=''_MNI.nii.gz''

for dirIdx in ''$dirStruct''
do
  echo ''$COUNTER''
  COUNTER=$[$COUNTER +1]
  # echo ''$dirIdx''
  
  # split name to remove suffix
  NIFTI_NAME=(${dirIdx//.nii.gz/ })

  STR_OUT_NAME_MAT=''${NIFTI_NAME[0]}$STR_MAT''
  STR_OUT_NAME=''${NIFTI_NAME[0]}$STR_OUT''
	
  # for the t1w image
  flirt -in ''$dirIdx'' -ref /home/appmin/apps/fsl-5.0.11/data/standard/MNI152_T1_1mm_brain.nii.gz -omat ''$STR_OUT_NAME_MAT'' -out ''$STR_OUT_NAME'' -dof 7
done
