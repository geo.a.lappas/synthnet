from ants.registration import resample_image_to_target, resample_image, registration
from ants import image_read, get_ants_data
from ants import nifti_to_ants
from ants import apply_transforms
from utilsNiftiProcessing import readNifti2Numpy, convertNumpyArray2Nifti
from pathlib import Path
from multiprocessing import Pool
from itertools import repeat

import nibabel as nib
import numpy as np
import os

POOL_WORKERS = 10

def registerSequence(filenameInput, filenameTarget):
    '''
    :param
    filenameInput: file in nifti or ndarray format to be resampled
    filenameTarget: file in nifti or ndarray format serves as target
    :return: resampled image nifti file
    '''

    ## NOTE: expects to have shape (#channels, dim1, dim2, ..., dim3)
    # input image
    if not (isinstance(filenameInput, str) or isinstance(filenameInput, np.ndarray)):
        raise AssertionError('The input is not provided in nifti or np.ndarray.')

    if (isinstance(filenameInput, str)):
        if filenameInput.endswith('.nii') or filenameInput.endswith('.nii.gz'):
            _, affineMatrixIn = readNifti2Numpy(filenameInput)

    elif isinstance(filenameInput, np.ndarray):
        inImg = filenameInput

    # target image
    if not (isinstance(filenameTarget, str) or isinstance(filenameTarget, np.ndarray)):
        raise AssertionError('The target is not provided in nifti or np.ndarray.')

    if (isinstance(filenameTarget, str)):
        if filenameTarget.endswith('.nii') or filenameTarget.endswith('.nii.gz'):
            _, affineMatrixTar = readNifti2Numpy(filenameTarget)

    elif isinstance(filenameTarget, np.ndarray):
        tarImg = filenameTarget

    # applying N4 bias field correction - conversion in 2 steps is required
    origImg = nifti_to_ants(nib.load(filenameInput))
    tarImg = nifti_to_ants(nib.load(filenameTarget))

    # T1ce resampled according to FLAIR resolution
    resampledImg = resample_image_to_target(image=origImg, target=tarImg)
    # Registration of FLAIR to T1ce
    registrationLayer = registration(fixed=tarImg, moving=resampledImg, type_of_transform='Translation')
    registeredImg = apply_transforms(fixed=tarImg, moving=resampledImg, transformlist=registrationLayer['fwdtransforms'])

    # output is torch tensor and conversion needs to be done
    return registeredImg.numpy(), affineMatrixTar

def resampleSequence(filenameInput, filenameTarget):
    '''
    :param
    filenameInput: file in nifti or ndarray format to be resampled
    filenameTarget: file in nifti or ndarray format serves as target
    :return: resampled image nifti file
    '''

    ## NOTE: expects to have shape (#channels, dim1, dim2, ..., dim3)
    # input image
    if not (isinstance(filenameInput, str) or isinstance(filenameInput, np.ndarray)):
        raise AssertionError('The input is not provided in nifti or np.ndarray.')

    if (isinstance(filenameInput, str)):
        if filenameInput.endswith('.nii') or filenameInput.endswith('.nii.gz'):
            _, affineMatrixIn = readNifti2Numpy(filenameInput)

    elif isinstance(filenameInput, np.ndarray):
        inImg = filenameInput

    # target image
    if not (isinstance(filenameTarget, str) or isinstance(filenameTarget, np.ndarray)):
        raise AssertionError('The target is not provided in nifti or np.ndarray.')

    if (isinstance(filenameTarget, str)):
        if filenameTarget.endswith('.nii') or filenameTarget.endswith('.nii.gz'):
            _, affineMatrixTar = readNifti2Numpy(filenameTarget)

    elif isinstance(filenameTarget, np.ndarray):
        tarImg = filenameTarget

    # applying N4 bias field correction - conversion in 2 steps is required
    origImg = nifti_to_ants(nib.load(filenameInput))
    tarImg = nifti_to_ants(nib.load(filenameTarget))

    resampledImg = resample_image_to_target(image=origImg, target=tarImg)

    # output is torch tensor and conversion needs to be done
    return resampledImg.numpy(), affineMatrixIn


def wrapFunction(dir2ApplyIn: str, dir2ApplyTar: str, dir2Save: str):
    """
    This is a function to perform N4 bias field correction using ants module.
    Params:
    dir2ApplyIn: file path to the directory of the input
    dir2ApplyTar: file path to the directory of the target
    dir2Save: file path to the directory of the output
    projectName: project name for the naming convention
    Returns: None
    """
    if not isinstance(dir2ApplyIn, str):
        raise AssertionError('dir2ApplyIn should be a file path of the directory of the input')

    if not os.path.exists(dir2Save):
        # creating parent and child directories if needed
        savingDir = Path(dir2Save)
        savingDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    onlyFileLstIn = []
    onlyFileLstTar = []

    for fileItr in os.listdir(dir2ApplyIn):
        if fileItr.endswith(".nii.gz"):
            onlyFileLstIn.append(fileItr)

    for fileItr in os.listdir(dir2ApplyTar):
        if fileItr.endswith(".nii.gz"):
            onlyFileLstTar.append(fileItr)

    return onlyFileLstIn, onlyFileLstTar

def poolRegistration(idxPoolIn, idxPoolTar, dir2ApplyIn, dir2ApplyTar, dir2Save):

    pathIn = os.path.join(dir2ApplyIn, idxPoolIn)
    pathTar = os.path.join(dir2ApplyTar, idxPoolTar)

    registeredInTemp, affineTemp = registerSequence(pathIn, pathTar)

    convertNumpyArray2Nifti(numpyArrLst=registeredInTemp, affineArrLst=affineTemp, projectName=idxPoolIn, dir2SaveData=dir2Save)

    print('Registration transformation is complete')

def poolResampling(idxPoolIn, idxPoolTar, dir2ApplyIn, dir2ApplyTar, dir2Save):

    pathIn = os.path.join(dir2ApplyIn, idxPoolIn)
    pathTar = os.path.join(dir2ApplyTar, idxPoolTar)

    resampledInTemp, affineTemp = resampleSequence(pathIn, pathTar)

    convertNumpyArray2Nifti(numpyArrLst=resampledInTemp, affineArrLst=affineTemp, projectName=idxPoolIn, dir2SaveData=dir2Save)

    print('Resampled transformation is complete')

if __name__ == '__main__':
    dirToApplyIn = '/data/sbig/projects/activity/ACTIVITY_retro/data/Bochum/overfitData_BIDS/BET/T1ce_N4BF/'
    dirToApplyTar = '/data/sbig/projects/activity/ACTIVITY_retro/data/Bochum/overfitData_BIDS/BET/FLAIR_N4BF/'
    dirToSave = '/data/sbig/projects/activity/ACTIVITY_retro/data/Bochum/overfitData_BIDS/BET/T1ce_res/'

    fileLst2RunIn, fileLst2RunTar = wrapFunction(dir2ApplyIn=dirToApplyIn, dir2ApplyTar=dirToApplyTar, dir2Save=dirToSave)

    with Pool(POOL_WORKERS) as p:
        p.starmap(poolResampling, zip(fileLst2RunIn, fileLst2RunTar, repeat(dirToApplyIn), repeat(dirToApplyTar), repeat(dirToSave)))
        # p.starmap(poolRegistration, zip(fileLst2RunIn, fileLst2RunTar, repeat(dirToApplyIn), repeat(dirToApplyTar), repeat(dirToSave)))
