from postprocessing.utilsNiftiProcessing import readNifti2Numpy, convertNumpyArray2Nifti
from ants import n4_bias_field_correction as antsBiasFieldCorrection
from ants import nifti_to_ants
from pathlib import Path
from multiprocessing import Pool
from itertools import repeat

import nibabel as nib
import numpy as np
import os

POOL_WORKERS = 10

def N4BiasFieldCorrection(filename):
    '''
    :param filename: file in nifti or ndarray format to be corrected
    :return: normalized intensity nifti file
    '''

    ## NOTE: expects to have shape (#channels, dim1, dim2, ..., dim3)

    if not(isinstance(filename, str) or isinstance(filename, np.ndarray)):
        raise AssertionError('The input is not provided in nifti or np.ndarray.')

    if (isinstance(filename, str)):
        if filename.endswith('.nii') or filename.endswith('.nii.gz'):
            _, affineMatrix = readNifti2Numpy(filename)

    elif isinstance(filename, np.ndarray):
        origImg = filename

    # applying N4 bias field correction - conversion in 2 steps is required
    origImg = nifti_to_ants(nib.load(filename))
    correctedImg = antsBiasFieldCorrection(image=origImg)

    # output is torch tensor and conversion needs to be done
    return correctedImg.numpy(), affineMatrix

def wrapN4BiasFieldCorrection(dir2Apply: str, dir2Save: str):
    """
    This is a function to perform N4 bias field correction using ants module.
    Params:
    dir2Apply: file path to the directory of the input
    dir2Save: file path to the directory of the output
    projectName: project name for the naming convention
    Returns: None
    """
    if not isinstance(dir2Apply, str):
        raise AssertionError('dir2Apply should be a file path of the directory of the input')

    if not os.path.exists(dir2Save):
        # creating parent and child directories if needed
        savingDir = Path(dir2Save)
        savingDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    onlyFileLst = []

    for fileItr in os.listdir(dir2Apply):
        if fileItr.endswith(".nii.gz"):
            onlyFileLst.append(fileItr)

    return onlyFileLst

def poolBiasFieldCorrection(idxPool, dir2Apply, dir2Save):
    path = os.path.join(dir2Apply, idxPool)
    biasRemovedInTemp, affineTemp = N4BiasFieldCorrection(path)

    convertNumpyArray2Nifti(numpyArr=biasRemovedInTemp, affineArrLst=affineTemp, fileName=idxPool, dir2SaveData=dir2Save)

    print('N4BiasFieldCorrection transformation is complete')

if __name__ == '__main__':
    dirToApply = '/home/glappas/PycharmProjects/Projects/ACTIVITY_retro/data/Bochum/Summary/GadNeg/pp/IN/T1_run02/'
    dirToSave = '/home/glappas/PycharmProjects/Projects/ACTIVITY_retro/data/Bochum/Summary/GadNeg/pp/N4BF/T1_run02/'

    fileLst2Run = wrapN4BiasFieldCorrection(dir2Apply=dirToApply, dir2Save=dirToSave)

    with Pool(POOL_WORKERS) as p:
        p.starmap(poolBiasFieldCorrection, zip(fileLst2Run, repeat(dirToApply), repeat(dirToSave)))