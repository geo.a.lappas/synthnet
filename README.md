## Welcome to SynthNet!
End-to-end unified deep learning framework for medical image analysis.

## What can you do with this tool?
This is a repository for **medical image translation**.
The repo is organised into four sections, namely:
- Installation
- Usage
- More capabilities of the model
 
## Installation
We strongly recommend that you install SynthNet using a virtual environment. Conda-pack file (.tar.gz) is required to create the environment.

Steps:
1. install conda-pack: conda install conda-pack
2. download the activenet.tar.gz file of the framework can be performed executing the following code in terminal:
3. create the project folder: mkdir -p ActiveNet
4. untar download: tar -xzf activenet.tar.gz -C ActiveNet
5. activate the venv: source ./ActiveNet/bin/activate

## Usage
You can now train and test a model on your data! How to do that? There are two ways:

# 1a. Using GUI (plug-and-play)
1. Define the input and target directories
2. Define the task
3. Define the results directory
4. Run Train Model

# 1b. Using the python executables (customizable)
This will skip steps for finding optimal params and go with the desired choice.
1. Configure the getParams.json file (same as step 1a, plus model definition and hyper-param choice)
2. Select your model
3. select input method
4. Select the datasplit
5. Customize dataPrams.json for required mode and augmentations
6. Run the MLflow.py*

*Each sub-section can be called separately or perform ML lifecycle execution (deploy model, train it and test it).
The code of this repo was developed using Tensorflow backend while parts use ONNX for compatibility with Pytorch and MONAI.

# 2. See the results
Independent if you select 1a or 1b, results will be written in the provided directory, including quantitative outputs summarized in a local html and qualitative output in animation format. Additionally, in case of classification you will get an excel file with the results, for segmentation and translation you will get in the predictions directory the corresponding nifti or npz files.

# What's more?
You can test one of your trained models, fine tune it and convert it to ONNX format for further processing using Pytorch or Tensorflow backend. Nigthly version provides statistics (mean, median, standard deviation, min, max) + histogram of your input data per sequence. In case of multicenter data histogram matching can be performed. You can also activate the XAI module which uses ONNX and CAPTUM to provide the end user with some explainable outputs, such as Grad-CAM.


# TODO:
- urgent:
Installation instructions as a docker image will follow.
a) implementation of the argparser. It is ready but not stable with every model.


- not urgent:
a) implementation of XAI for tensorboard. Modules are available but not working with 3D data (have a look on developing branch).
b) implementation of Diffuser. 

## Support
For issues, please create an issue or contact geo.a.lappas@gmail.com. 

## Contributing
Would you like to contribute? Please contact geo.a.lappas@gmail.com. 

## License
Apache 2.0 license

## Project status
Active.