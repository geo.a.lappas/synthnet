from postprocessing.utilsNiftiProcessing import readNifti2Numpy
from postprocessing.utilsArrayProcessing import *

import numpy as np
import keras.utils.data_utils
import os
import gc

class DataGenerator(keras.utils.data_utils.Sequence):
    '''
    Datagenerator class, Tensorflow implementation using tf.keras.utils.Sequence class.

    Returns: tf.keras.utils.Sequence object.
    '''

    # constructor
    def __init__(self,
                 input=os.path.join(os.path.curdir, '/input'),
                 target=os.path.join(os.path.curdir, '/target'),
                 batchSize=32,
                 modelDim = 2,
                 dim=(256, 256),
                 nbrInChannel=1,
                 nbrOutChannel=1,
                 augmentation=True,
                 shuffling=False,
                 patch=False,
                 patchSize=96,
                 denoising=False,
    ):

        if not isinstance(input, list):
            raise AssertionError('input needs to be a list.')
        if not isinstance(target, list):
            raise AssertionError('target needs to be a list.')
        if not isinstance(batchSize, int):
            raise AssertionError('batchSize needs to be an integer.')
        if not isinstance(dim, tuple):
            raise AssertionError('dim needs to be either a 2D or 3D tuple.')
        if not isinstance(nbrInChannel, int):
            raise AssertionError('nbrInChannel needs to be an integer.')
        if not isinstance(nbrOutChannel, int):
            raise AssertionError('nbrOutChannel needs to be an integer.')
        if not isinstance(shuffling, bool):
            raise AssertionError('shuffle needs to be a boolean.')
        if not isinstance(patch, bool):
            raise AssertionError('patch needs to be a boolean.')
        if not isinstance(patchSize, int):
            raise AssertionError('patchSize needs to be an integer.')
        if not isinstance(denoising, int):
            raise AssertionError('denoising needs to be a boolean.')

        # initialization of the object properties
        self.input = input
        self.target = target
        self.batchSize = batchSize
        self.dim = dim
        self.modelDim = modelDim
        self.nci = nbrInChannel
        self.nco = nbrOutChannel
        self.augmentation = augmentation
        self.shuffling = shuffling
        self.patch = patch
        self.patchSize = patchSize
        self.denoising = denoising
        self.on_epoch_end()

    def __len__(self):
        '''Calculates the number of batches per epoch.'''
        # batchPerEpoch = np.math.ceil(len(self.input) / self.batchSize)

        if len(self.input[0]) % self.batchSize == 0:
            batchPerEpoch = len(self.input[0]) // self.batchSize
        else:
            batchPerEpoch = (len(self.input[0]) // self.batchSize) + 1

        # batchPerEpoch = int(np.floor(len(self.input[0]) / self.batchSize))

        return batchPerEpoch

    def applyPreprocessing(self, currentArray: np.ndarray, modelReady: bool):
        # standardization pipeline
        currentArray = fillNanArray(currentArray.copy())
        currentArray, _ = removeBackground_np(currentArray.copy(), modelReady=modelReady)
        currentArray, _ = fixedSize(currentArray.copy(), self.dim)
        currentArray, _ = normalizeArray(currentArray.copy())

        return currentArray

    def on_epoch_end(self):
        '''Updates indexes after each epoch; shuffling the input order'''
        if self.shuffling:
            np.random.shuffle(list(zip(self.input[0], self.target[0])))

        # removing the dummy duplicates
        self.input = self.input[:self.nci]
        self.target = self.target[:self.nco]

        # cleaning temporarily the memory
        gc.collect()

    def __getitem__(self, index):
        '''Generate one batch of data.'''
        idxInput = [itr[index * self.batchSize:(index + 1) * self.batchSize] for itr in self.input]
        idxOutput = [itr[index * self.batchSize:(index + 1) * self.batchSize] for itr in self.target]

        inputs = self.__get_data(inputLst=idxInput, nbrChannel=self.nci)
        targets = self.__get_data(inputLst=idxOutput, nbrChannel=self.nco)

        if self.augmentation:
            # will be performed on full image
            inputs, targets = randomRotationPair(input_array=inputs, target_array=targets, angle_range=(-50, 50))
            inputs, targets = randomTranslationPair(input_array=inputs, target_array=targets, translation_range=(-15, 15))

        if self.denoising:
            noise_factor = 0.05
            inputs = inputs + noise_factor * np.random.normal(size=inputs.shape)
            inputs = np.clip(inputs, a_min=0., a_max=1.)

        # create patches if enabled
        if self.patch:
            inputsTemp = []
            targetsTemp = []

            if self.batchSize == 1:
                curInputs, cropLst = self.create_patches(inputs=inputs, idxCropLst=None)
                curTargets, _ = self.create_patches(inputs=targets, idxCropLst=cropLst)

                inputsTemp.append(curInputs)
                targetsTemp.append(curTargets)

            elif self.batchSize > 1:
                for itrBatch in range(self.batchSize):
                    curInputs, cropLst = self.create_patches(inputs=inputs[itrBatch], idxCropLst=None)
                    curTargets, _ = self.create_patches(inputs=targets[itrBatch], idxCropLst=cropLst)

                    inputsTemp.append(curInputs)
                    targetsTemp.append(curTargets)

            inputs = np.concatenate(inputsTemp, axis=0)
            targets = np.concatenate(targetsTemp, axis=0)

        return inputs, targets
    def create_patches(self, inputs, idxCropLst=None):
        x = []

        if inputs.ndim == 4:
            inputs = inputs[np.newaxis, :, :, :, :]

        for itrChannel in range(inputs.shape[-1]):
            curArray = inputs[:, :, :, :, itrChannel].copy()
            curArray = curArray[:, :, :, :, np.newaxis]

            if itrChannel == 0 and idxCropLst is None:
                tempOut, idxCropLst = removeBackground_np(curArray, modelReady=True)

            else:
                tempOut, _ = removeBackground_np(curArray, modelReady=True, cropIdx=idxCropLst)

            x.append(tempOut)

            inputs = get_patches(img_arr=np.concatenate(x, axis=-1), size=self.patchSize, stride=self.patchSize, modelReady=True)

        return inputs.astype(float), idxCropLst

    def __get_data(self, inputLst, nbrChannel):
        '''Generates the batch while performing preprocessing steps at the same time containing batchSize samples'''

        inputs = np.zeros((self.batchSize, *self.dim, len(inputLst)), dtype="float32")

        for itrChannel in range(nbrChannel):
            curChannelLst = inputLst[itrChannel]

            for itrCase in range(len(curChannelLst)):
                # Process input channels
                currentInput, _ = readNifti2Numpy(curChannelLst[itrCase])
                currentInput = self.applyPreprocessing(currentArray=currentInput.copy(), modelReady=False)
                inputs[itrCase, :, :, :, itrChannel] = currentInput

        return inputs.astype(float)