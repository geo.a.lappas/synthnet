import numpy as np
import os
import sys
import matplotlib.pyplot as plt

from keras.callbacks import Callback
from pathlib import Path
from postprocessing.utilsArrayProcessing import merge_patches, normalizeArray, fixedSize, removeBackground_np
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from scipy.ndimage import binary_fill_holes

class plotMetricsOnEpochEnd(Callback):

    def __init__(self, train_data, valid_data, test_data, dir2Save, idx1Slice, idx2Slice, idx3Slice, patchFlag, batchSize, origSize, inChannelLen, outChannelLen):
        super().__init__()

        self.train_data = [np.concatenate([train_data[0][itr] for itr in range(inChannelLen)], axis=-1), np.concatenate([train_data[-1][itr] for itr in range(outChannelLen)], axis=-1)]
        self.valid_data = [np.concatenate([valid_data[0][itr] for itr in range(inChannelLen)], axis=-1), np.concatenate([valid_data[-1][itr] for itr in range(outChannelLen)], axis=-1)]
        self.test_data = [np.concatenate([test_data[0][itr] for itr in range(inChannelLen)], axis=-1), np.concatenate([test_data[-1][itr] for itr in range(outChannelLen)], axis=-1)]

        self.dir2Save = dir2Save
        self.idx1Slice = idx1Slice
        self.idx2Slice = idx2Slice
        self.idx3Slice = idx3Slice
        self.patchFlag = patchFlag
        self.batchSize = batchSize
        self.origSize = origSize
        self.inChannelLen = inChannelLen
        self.outChannelLen = outChannelLen

    def evaluateData(self, curData, mode=None):
        cur_x, cur_y = curData

        # Save the current standard output
        original_stdout = sys.stdout

        # Open a null device or a file to redirect the output
        with open(os.devnull, 'w') as devnull:
            # Redirect the standard output to the null device or file
            sys.stdout = devnull

            # Call model.predict without printing the output
            cur_loss = self.model.evaluate(cur_x, cur_y, verbose=0)

        # Restore the original standard output
        sys.stdout = original_stdout

        print(mode, 'set multi-loss: {}, MAE: {}, SSIM: {}'.format(round(cur_loss[0], 2), round(cur_loss[1], 2), round(cur_loss[-1], 2)))
        print('-------------------------------------------')

        # get only certain decimals
        return [round(itr, 2) for itr in cur_loss]
    def makePredictions(self, curData):
        cur_x, cur_y = curData

        # Save the current standard output
        original_stdout = sys.stdout

        # Open a null device or a file to redirect the output
        with open(os.devnull, 'w') as devnull:
            # Redirect the standard output to the null device or file
            sys.stdout = devnull

            # Call model.predict without printing the output
            cur_pred = self.model.predict(cur_x, verbose=0)

        # Restore the original standard output
        sys.stdout = original_stdout

        return cur_pred
    def getSlicesImage(self, curData, curPred, slice2plot):
        cur_x, cur_y = curData

        # curPredCropped = []
        #
        # # assume always one by one prediction
        # for itrchannel in range(curPred.shape[-1]):
        #     tempArr, _ = normalizeArray(curPred[0, :, :, :, itrchannel])
        #     tempArr, _ = removeBackground_np(tempArr, modelReady=False)
        #
        #     curPredCropped.append(tempArr)
        #
        # curPredCropped = np.array(curPredCropped)
        # curPredCropped = np.expand_dims(curPredCropped, axis=0)
        # curPred = curPredCropped
        # del curPredCropped

        # get the slices based on self.idxSlice - adjust this to select a different slice
        x_slice = np.zeros(shape=(cur_x.shape[1], cur_x.shape[2], cur_x.shape[-1]))

        for itrChannel in range(cur_x.shape[-1]):
            x_slice[:, :, itrChannel] = cur_x[0, :, :, slice2plot, itrChannel]

        y_slice = np.zeros(shape=(cur_y.shape[1], cur_y.shape[2], cur_y.shape[-1]))
        pred_slice = np.zeros(shape=(cur_y.shape[1], cur_y.shape[2], cur_y.shape[-1]))
        diff_slice = np.zeros(shape=(cur_y.shape[1], cur_y.shape[2], cur_y.shape[-1]))

        for itrChannel in range(cur_y.shape[-1]):
            y_slice[:, :, itrChannel] = cur_y[0, :, :, slice2plot, itrChannel]
            pred_slice[:, :, itrChannel] = curPred[0, :, :, slice2plot, itrChannel]

            diff_slice[:, :, itrChannel] = y_slice[:, :, itrChannel] - pred_slice[:, :, itrChannel]

        sumArray = np.concatenate((x_slice, y_slice, pred_slice, diff_slice), axis=-1)

        return sumArray
    def getSlicesPatch(self, cur_x, cur_y, curPred, slice2plot):
        # get the slices based on self.idxSlice - adjust this to select a different slice
        cur_x, cur_y = np.array(cur_x), np.array(cur_y)

        x_slice = np.zeros(shape=(cur_x.shape[1], cur_x.shape[2], cur_x.shape[-1]))

        for itrChannel in range(cur_x.shape[-1]):
            x_slice[:, :, itrChannel] = cur_x[0, :, :, slice2plot, itrChannel]

        y_slice = np.zeros(shape=(cur_y.shape[1], cur_y.shape[2], cur_y.shape[-1]))

        pred_slice = np.zeros(shape=(cur_y.shape[1], cur_y.shape[2], cur_y.shape[-1]))
        diff_slice = np.zeros(shape=(cur_y.shape[1], cur_y.shape[2], cur_y.shape[-1]))

        for itrChannel in range(cur_y.shape[-1]):
            y_slice[:, :, itrChannel] = cur_y[0, :, :, slice2plot, itrChannel]
            pred_slice[:, :, itrChannel] = curPred[0, :, :, slice2plot, itrChannel]
            diff_slice[:, :, itrChannel] = y_slice[:, :, itrChannel] - pred_slice[:, :, itrChannel]

        sumArray = np.concatenate((x_slice, y_slice, pred_slice, diff_slice), axis=-1)

        return sumArray

    def on_epoch_end(self, epoch, logs=None):
        # get the train, valid and test data; and make predictions
        y_train_pred = self.makePredictions(curData=self.train_data)
        y_valid_pred = self.makePredictions(curData=self.valid_data)
        y_test_pred = self.makePredictions(curData=self.test_data)

        # Compute metrics
        fname1 = (' epoch_{:03d}').format(epoch + 1)
        print('Stats for' + fname1)

        _ = self.evaluateData(curData=self.train_data, mode='training')
        _ = self.evaluateData(curData=self.valid_data, mode='validation')
        _ = self.evaluateData(curData=self.test_data, mode='test')

        trainLst, validLst, testLst = [], [], []

        if not self.patchFlag:  # if full image is given
            trainLst.append(self.getSlicesImage(curData=self.train_data, curPred=y_train_pred, slice2plot=self.idx1Slice))
            validLst.append(self.getSlicesImage(curData=self.valid_data, curPred=y_valid_pred, slice2plot=self.idx2Slice))
            testLst.append(self.getSlicesImage(curData=self.test_data, curPred=y_test_pred, slice2plot=self.idx3Slice))

        if self.patchFlag:  # if patch-based method are selected - merge all patches into an image and get a slice
            # training patches
            x_train = merge_patches(patch_arr=self.train_data[0], origSize=self.origSize, batchSize=self.batchSize, patchSize=self.train_data[0].shape[1], stride=self.train_data[0].shape[1], channel=self.inChannelLen, modelReady=True)
            y_train = merge_patches(patch_arr=self.train_data[-1], origSize=self.origSize, batchSize=self.batchSize, patchSize=self.train_data[-1].shape[1], stride=self.train_data[-1].shape[1], channel=self.outChannelLen, modelReady=True)
            pred_train = merge_patches(patch_arr=y_train_pred, origSize=self.origSize, batchSize=self.batchSize, patchSize=y_train_pred.shape[1], stride=y_train_pred.shape[1], channel=self.outChannelLen, modelReady=True)
            trainLst.append(self.getSlicesPatch(cur_x=x_train, cur_y=y_train, curPred=pred_train, slice2plot=self.idx1Slice))

            # test patches
            x_valid = merge_patches(patch_arr=self.valid_data[0], origSize=self.origSize, batchSize=self.batchSize, patchSize=self.valid_data[0].shape[1], stride=self.valid_data[0].shape[1], channel=self.inChannelLen, modelReady=True)
            y_valid = merge_patches(patch_arr=self.valid_data[-1], origSize=self.origSize, batchSize=self.batchSize, patchSize=self.valid_data[-1].shape[1], stride=self.valid_data[-1].shape[1], channel=self.outChannelLen, modelReady=True)
            pred_valid = merge_patches(patch_arr=y_valid_pred, origSize=self.origSize, batchSize=self.batchSize, patchSize=y_train_pred.shape[1], stride=y_train_pred.shape[1], channel=self.outChannelLen, modelReady=True)
            validLst.append(self.getSlicesPatch(cur_x=x_valid, cur_y=y_valid, curPred=pred_valid, slice2plot=self.idx2Slice))

            # test patches
            x_test = merge_patches(patch_arr=self.test_data[0], origSize=self.origSize, batchSize=self.batchSize, patchSize=self.test_data[0].shape[1], stride=self.test_data[0].shape[1], channel=self.inChannelLen, modelReady=True)
            y_test = merge_patches(patch_arr=self.test_data[-1], origSize=self.origSize, batchSize=self.batchSize, patchSize=self.test_data[-1].shape[1], stride=self.test_data[-1].shape[1], channel=self.outChannelLen, modelReady=True)
            pred_test = merge_patches(patch_arr=y_test_pred, origSize=self.origSize, batchSize=self.batchSize, patchSize=y_test_pred.shape[1], stride=y_test_pred.shape[1], channel=self.outChannelLen, modelReady=True)
            testLst.append(self.getSlicesPatch(cur_x=x_test, cur_y=y_test, curPred=pred_test, slice2plot=self.idx3Slice))

        # design the figure layout
        nbrRow = 3 # for train, valid and test
        nbrColumn = self.inChannelLen + (self.outChannelLen * 2 + 1)  # two times the output channels for the prediction(s) + 1 for the difference
        fig, axs = plt.subplots(nbrRow, nbrColumn)

        plt.subplots_adjust(hspace=0.1)

        # setting the titles
        inputV = [f"Input#{itrChannels+1}" for itrChannels in range(self.inChannelLen)]
        targetV = [f"Target#{itrChannels+1}" for itrChannels in range(self.outChannelLen)]
        predV = [f"Pred#{itrChannels+1}" for itrChannels in range(self.outChannelLen)]
        diffV = [f"Residuals#{itrChannels + 1}" for itrChannels in range(self.outChannelLen)]

        titleXaxis = [inputV, targetV, predV, diffV]
        titleXaxis = [itr for subList in titleXaxis for itr in subList]
        titleYaxis = ['TrainSet', 'ValidSet', 'TestSet']

        for itrX in range(nbrRow):
            for itrY in range(nbrColumn):
                axs[0][itrY].set_title(titleXaxis[itrY])

                if itrX == 0:
                    tempLst = np.squeeze(trainLst, axis=0)
                elif itrX == 1:
                    tempLst = np.squeeze(validLst, axis=0)
                elif itrX == (nbrRow - 1):
                    tempLst = np.squeeze(testLst, axis=0)
                else:
                    print('itrY is out of bounds')
                    tempLst = []

                if itrY < (nbrColumn-1):
                    axs[itrX][itrY].imshow(np.rot90(tempLst[:, :, itrY], 1), cmap='gray')  # select to plot only first patch
                else:
                    # axs[itrX][itrY].imshow(np.rot90(tempLst[:, :, itrY], 1), cmap='jet')  # select to plot only first patch
                    # Create a colorbar
                    im = axs[itrX][itrY].imshow(np.rot90(tempLst[:, :, itrY], 1), cmap='jet')

                    # Create a colorbar for the last column
                    divider = make_axes_locatable(axs[itrX][-1])  # Use the last subplot in the first row for colorbar positioning
                    cax = divider.append_axes("right", size="7%", pad="2%")
                    plt.colorbar(im, cax=cax) # Use any subplot in the grid for creating the colorbar

                axs[itrX][0].text(-0.3, 0.5, titleYaxis[itrX], transform=axs[itrX][0].transAxes, va='center', rotation='vertical')
                axs[itrX][itrY].set_axis_off()

        predDirPath = Path(self.dir2Save)

        if not os.path.exists(self.dir2Save):
            predDirPath.mkdir(mode=0o751, parents=True, exist_ok=True)


        fname2 = (self.dir2Save + '/pred@epoch_{:03d}.png').format(epoch + 1)
        fig.savefig(fname2)
        plt.close('all')

