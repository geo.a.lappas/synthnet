from glob import glob
import math

def dataSplit(inputDir: str, targetDir: str, trainFlag: bool, CV: bool, singleDatasetValid:bool):
    '''
    Split your datasets for training, validation and inference accordingly
    Parameters
    ----------
    inputDir: sequence directory
    targetDir: target directory (masks or domain)
    trainFlag: mode to define the split method. It performs either a split for training/validation or inference.
    CV: mode to define if 5-fold cross-validation will take place
    singleDatasetValid: if true only input dataset will be used for train/valid split. Otherwise, expect validation dataset.
    If True, then is considered as training and perform a split of the original dataset
    in 80% for training and 20% for validation. If False, the whole dataset will be kept for testing.

    Returns: two list consists of input, target filenames
    -------
    '''

    if not isinstance(inputDir, str):
        raise AssertionError('The inputDir should be a string defining the directory of input data.')
    if not isinstance(targetDir, str):
        raise AssertionError('The targetDir should be a string defining the directory of input data.')
    if not isinstance(trainFlag, bool):
        raise AssertionError('The trainFlag should be a boolean defining the mode, training or inference.')
    if not isinstance(CV, bool):
        raise AssertionError('The CV should be a boolean defining if 5-fold cross-validation will be applied.')

    input = sorted(glob(inputDir + '/*'))
    target = sorted(glob(targetDir + '/*'))

    if CV:
        folds = 5  # for now can do 5-fold CV but would be good if 10-fold is an option too

        partitionPosIn = len(input) // folds
        modPartitionPosIn = len(input) % folds

        partitionPosOut = len(target) // folds
        modPartitionPosOut = len(target) % folds

        if not modPartitionPosIn == 0 and isinstance(modPartitionPosIn, int):
            lastData2AddIn = modPartitionPosIn
        elif not modPartitionPosIn == 0 and isinstance(modPartitionPosIn, float):
            lastData2AddIn = math.floor(modPartitionPosIn)
        elif modPartitionPosIn == 0:
            lastData2AddIn = 0

        if not modPartitionPosOut == 0 and isinstance(modPartitionPosOut, int):
            lastData2AddOut = modPartitionPosOut
        elif not modPartitionPosIn == 0 and isinstance(modPartitionPosOut, float):
            lastData2AddOut = math.floor(modPartitionPosOut)
        elif modPartitionPosOut == 0:
            lastData2AddOut = 0

        cvPosLstStartIn, cvPosLstEndIn = [], []
        cvPosLstStartOut, cvPosLstEndOut = [], []
        foldInputTrain, foldTargetTrain = [], []
        foldInputTest, foldTargetTest = [], []

        for itrFolds in range(folds):
            # start position per fold
            startFoldIn = itrFolds*partitionPosIn
            startFoldOut = itrFolds*partitionPosOut

            cvPosLstStartIn.append(startFoldIn)
            cvPosLstStartOut.append(startFoldOut)

            if itrFolds < 4:  # not the final fold
                endFoldIn = itrFolds*partitionPosIn + partitionPosIn  # end position per fold
                endFoldOut = itrFolds * partitionPosOut + partitionPosOut

            elif itrFolds == 4:  # if this is the last fold
                endFoldIn = itrFolds * partitionPosIn + partitionPosIn + lastData2AddIn  # end position per fold
                endFoldOut = itrFolds * partitionPosOut + partitionPosOut + lastData2AddOut

            cvPosLstEndIn.append(endFoldIn)
            cvPosLstEndOut.append(endFoldOut)

            # get the first N-values. N is determined by endFoldIn-startFoldIn. Similar work for target
            foldInputTest.append(input[startFoldIn:endFoldIn])
            foldTargetTest.append(target[startFoldOut:endFoldOut])

            foldInputTrain.append([itr for itr in input if itr not in (foldInputTest[-1])])

            # unit-test
            if any(value in foldInputTest[itrFolds] for value in foldInputTrain[itrFolds]):
                raise AssertionError('Same input data is present in both the training and test set')

            foldTargetTrain.append([itr for itr in target if itr not in (foldTargetTest[-1])])

            # unit-test
            if any(value in foldTargetTest[itrFolds] for value in foldTargetTrain[itrFolds]):
                raise AssertionError('Same target data is present in both the training and test set')

        if trainFlag:
            # print('Only training and validation split will be returned.')
            trainPerc = .8 # training 90% and the rest 10% validation

            partitionInput = []
            partitionTarget = []

            for itrFolds in range(folds):
                partitionInput.append({'train': foldInputTrain[itrFolds][0:int(trainPerc * len(foldInputTrain[itrFolds]))],
                                       'valid': foldInputTrain[itrFolds][int(trainPerc * len(foldInputTrain[itrFolds])):len(foldInputTrain[itrFolds])]})

                # unit-test
                if any(value in partitionInput[itrFolds]['train'] for value in partitionInput[itrFolds]['valid']):
                    raise AssertionError('Input of the training and validation data has common data')

                partitionTarget.append({'train': foldTargetTrain[itrFolds][0: int(trainPerc * len(foldInputTrain[itrFolds]))],
                                        'valid': foldTargetTrain[itrFolds][int(trainPerc * len(foldInputTrain[itrFolds])): len(foldInputTrain[itrFolds])]})

                # unit-test
                if any(value in partitionTarget[itrFolds]['train'] for value in partitionTarget[itrFolds]['valid']):
                    raise AssertionError('Target of the training and validation data has common data')

        else:
            partitionInput = []
            partitionTarget = []

            for itrFolds in range(folds):
                # TODO multifactor here
                partitionInput.append({'test': foldInputTest[itrFolds]})
                partitionTarget.append({'test': foldTargetTest[itrFolds]})

    else:
        partitionInput = []
        partitionTarget = []

        if trainFlag:
            if singleDatasetValid:
                trainPerc = .8

                partitionInput.append({'train': input[:int(trainPerc * len(input))],
                                       'valid': input[int(trainPerc * len(input)):]})

                partitionTarget.append({'train': target[:int(trainPerc * len(input))],
                                        'valid': target[int(trainPerc * len(input)):]})

            else:
                partitionInput.append({'train': input})
                partitionTarget.append({'train': target})

        else:
            partitionInput.append({'test': input})
            partitionTarget.append({'test': target})

    return partitionInput, partitionTarget