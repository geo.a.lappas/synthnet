from keras.callbacks import *
import tensorflow as tf

class lossChangeOnline(tf.keras.callbacks.Callback):
    def __init__(self, model, switchAtEpoch, loss):
        super(lossChangeOnline, self).__init__()

        self.model = model
        self.switchAtEpoch = switchAtEpoch
        self.loss = loss

    def on_epoch_end(self, epoch, logs=None):
        if epoch == self.switchAtEpoch:
            print('Now switching losses')
            # Compile the model with the best set of hyperparameters
            self.model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=2e-2, epsilon=0.1), loss=self.loss)
            print('Model.compile is complete')

def getCallback(modelDir, logDir):
    # callbacks
    # TODO add those details on a json file

    # earlystopping = EarlyStopping(monitor='val_loss',
    #                               mode='min',
    #                               verbose=1,
    #                               patience=50)

    # save the best model with lower validation loss
    checkpointer = ModelCheckpoint(filepath=os.path.join(modelDir, "Unet3D_model.ckpt"),
                                   verbose=0,
                                   save_weights_only=True)

    reduce_lr = ReduceLROnPlateau(monitor='val_SSIM',
                                  mode='min',
                                  verbose=1,
                                  patience=50,
                                  min_delta=0.000001,
                                  factor=0.2)

    tensorboardCallback = tf.keras.callbacks.TensorBoard(log_dir=logDir, update_freq='epoch', histogram_freq=5)

    # callbackLst = [earlystopping, checkpointer, reduce_lr, tensorboardCallback]
    callbackLst = [checkpointer, reduce_lr, tensorboardCallback]

    return callbackLst