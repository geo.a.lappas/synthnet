from datetime import datetime
from pathlib import Path
from keras.callbacks import ModelCheckpoint, TensorBoard, ReduceLROnPlateau
from postprocessing.utilsNiftiProcessing import readNifti2Numpy
from postprocessing.utilsArrayProcessing import *

import ast
import os
import numpy as np
import random
import json
import glob
import argparse
import tensorflow as tf

def convertDictionary2Json(curDict, dir2save):

    # Convert the dictionary to a JSON string
    json_config = json.dumps(curDict, indent=4)

    # creating directories for model
    workingDir = Path(dir2save)
    workingDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    # Save the JSON string to a file
    with open(os.path.join(workingDir, 'config_train.json'), 'w') as f:
        f.write(json_config)

def standardizeArray4Model(curArray, desiredShape, mode='input'):
    curArray, _ = readNifti2Numpy(curArray)
    curArray = fillNanArray(curArray.copy())

    if mode == 'input':
        curArray, _ = normalizeArray(curArray.copy())

    curArray, _ = fixedSize(curArray.copy(), desiredShape)

    return curArray

def standardizeArray4Model2(currentTarget, mode):
    currentTarget, _ = readNifti2Numpy(currentTarget)
    currentTarget = fillNanArray(currentTarget.copy())

    ###
    # import nibabel as nib
    # # currentArray = currentTarget
    #
    # # removing any pixels outside of brain region
    # brainMask = np.where(ooi > 0.05, 1.0, 0.0)
    # X, Y, Z = np.nonzero(brainMask)
    # tempIn = currentTarget[X.min():X.max(), Y.min():Y.max(), Z.min():Z.max()]
    #
    # del currentTarget
    #
    # currentArray = tempIn.copy()
    # niftiCurFile = nib.Nifti1Image(currentArray, affine=np.eye(4))
    # nib.save(niftiCurFile, "test_arr1.nii.gz")
    ###

    if mode == "input":

        currentTarget, croppingList = removeBackground_np(currentTarget.copy(), modelReady=False)

        # removing any pixels outside of brain region
        # brainMask = np.where(currentTarget > 0.05, 1.0, 0.0)
        # X, Y, Z = np.nonzero(brainMask)
        # tempIn = currentTarget[X.min():X.max(), Y.min():Y.max(), Z.min():Z.max()]
        #
        # del currentTarget
        # currentTarget = tempIn.copy()

    else:
        croppingList = [0, 0, 0, 0, 0, 0]

    # currentTarget, _ = normalizeArray(currentTarget.copy())

    return currentTarget, croppingList

def prepareData4Test(currentInput, currentTarget, dim):

    # input
    currentInput = standardizeArray4Model(curArray=currentInput, desiredShape=dim, mode='input')

    # target
    currentTarget = standardizeArray4Model(curArray=currentTarget, desiredShape=dim, mode='target')

    return currentInput.astype(float), currentTarget.astype(float)

def prepareData4Test2(currentInput, currentTarget, dim, patch=False, patchSize=None, denoising=False):
    if patch and patchSize == None:
        raise AssertionError('patch-based method is enabled but no patchSize is given')

    # input
    currentInput, idxLst = standardizeArray4Model2(currentInput, mode='input')

    if denoising:
        noise_factor = 0.2
        currentInput = currentInput + noise_factor * np.random.normal(size=currentInput.shape)
        currentInput = np.clip(currentInput, a_min=0., a_max=1.)

    # target1
    currentTarget, _ = standardizeArray4Model2(currentTarget, mode='target')
    currentTarget, _ = removeBackground_np(inputArray=currentTarget, modelReady=False, cropIdx=idxLst)

    # obtaining a fixed size input
    currentInput, _ = fixedSize(currentInput.copy(), dim)
    currentTarget, _ = fixedSize(currentTarget.copy(), dim)

    # Normalize arrays
    currentInput, _ = normalizeArray(currentInput.copy())
    currentTarget, _ = normalizeArray(currentTarget.copy())

    # creating a shape of batchSize, X, Y, Z, channels
    currentInput = currentInput[np.newaxis, :, :, :, np.newaxis]
    currentTarget = currentTarget[np.newaxis, :, :, :, np.newaxis]

    if patch:
        patchesInput = get_patches(img_arr=currentInput, size=patchSize, stride=patchSize, modelReady=True)
        patchesTarget = get_patches(img_arr=currentTarget, size=patchSize, stride=patchSize, modelReady=True)

        return patchesInput.astype(float), patchesTarget.astype(float)
    else:
        return currentInput.astype(float), currentTarget.astype(float)

def setGlobalSeed():
    seed = 42
    random.seed, np.random.seed, tf.seed = seed, seed, seed

    return [random.seed, np.random.seed, tf.seed]

def deleteDirContent(dir2write):
    '''
    Helper function to delete or maintain contents when directory exists and user will write in it.
    Parameters
    ----------
    dir2write: directory to overwrite

    Returns:
    exitStatus: if 1 means deletion, else means maintain contents and make new directory
    '''

    if os.path.exists(dir2write):
        overwriteModel = input('Do you want to overwrite previous model?')
        if overwriteModel == 'y' or overwriteModel == 'Y':
            filesInPath = glob.glob(os.path.join(dir2write, '*'))
            for fileIdx in filesInPath:
                os.remove(fileIdx)
            exitStatus = 1

        elif overwriteModel == 'n' or overwriteModel == 'N':
            print('Directory might exist but will not be deleted')
            newLogDir = os.path.join(dir2write, '_updatedVersion')
            os.mkdir(newLogDir)
            exitStatus = 0

    return exitStatus

def makeModelDir(workingDir, getModelParams, getDatagenParams, dataSummary, foldNbr):
    '''
    Creating the directory to work
    Parameters
    ----------
    workingDir: directory the model will be saved
    getModelParams: dictionary of model parameters
    getDatagenParams: dictionary of datagen parameters
    dataSummary: list with training and validation or test data
    Returns: directory of the model, directory of the xai part and directory of logs
    -------
    '''

    if not isinstance(workingDir, str):
        raise AssertionError('The workingDir should be a string defining the directory to save your model parameters.')

    if not isinstance(getModelParams, dict):
        raise AssertionError('The getModelParams should be a dictionary defining the model parameters.')

    if not isinstance(getDatagenParams, dict):
        raise AssertionError('The getDatagenParams should be a dictionary defining the datagenerator parameters.')

    if not (isinstance(dataSummary, list) or isinstance(dataSummary, dict)):
        raise AssertionError('The dataSummary should be a dictionary defining the data summary.')

    # create model name based on current date and time
    now = datetime.now()  # current date and time
    time = now.strftime("%H-%M-%S")
    date = datetime.today().strftime('%y-%m-%d')

    # creating directories for model
    workingDir = Path(os.path.join(workingDir, date + '_' + time))
    workingDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    # creating directories for logs
    logDir = Path(os.path.join(workingDir, 'logs'))
    logDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    # creating directories for xai
    xaiDir = Path(os.path.join(workingDir, 'xai'))
    xaiDir.mkdir(mode=0o751, parents=True, exist_ok=True)

    # create json file for model params
    modelParamsFileLoc = os.path.join(workingDir, 'modelParams.json')
    getModelParams2 = {key: str(value) for key, value in getModelParams.items()}

    with open(modelParamsFileLoc, 'w+') as outputfileModelParams: # will update the json contents if exists each time
        jsonFileModel = json.dump(getModelParams2, outputfileModelParams)
        print('Model params have been saved in', str(modelParamsFileLoc))

    # create json file for datagen params
    dataParamsFileLoc = os.path.join(workingDir, 'datagenParams.json') # create json file with datagen params
    getDatagenParams2 = {key: str(value) for key, value in getDatagenParams.items()}

    with open(dataParamsFileLoc, 'w+') as outputfileDatagenParams:
        jsonFileDatagen = json.dump(getDatagenParams2, outputfileDatagenParams)
        print('Datagen params have been saved in', str(dataParamsFileLoc))

    # create json file for dataset summary params
    dataSummaryFileLoc = os.path.join(workingDir, 'fold' + str(foldNbr) + '_datagenSummary.json')

    with open(dataSummaryFileLoc, 'w+') as outputfileData: # will update the json contents if exists each time
        jsonDataModel = json.dump(dataSummary, outputfileData)
        print('Data summary params have been saved in', str(dataSummaryFileLoc))

    return str(workingDir), logDir, xaiDir

def modelAdjustments(indexItr, modelDir, logDir):
    '''
    Take care of tensorflow callbacks and tensorboard profilers.
    Params:
    itr: indexItr in case of a cross-validation, it determines the fold number in the checkpoint
    modelDir: directory of the model checkpoint/params and datagen params to be saved
    logDir: directory of model logging

    Return: list of model callback, tensorboard callback and callback scheduler to pass to the fitting method
    '''

    if not isinstance(indexItr, int):
        raise AssertionError('indexItr should be an integer.')
    if not isinstance(modelDir, str):
        raise AssertionError('modelDir should be a string. This corresponds to the directory of model to be saved.')

    modelChkPnt = os.path.join(modelDir, 'fold_' + str(indexItr+1), 'checkpoint.ckpt')

    modelCallback = ModelCheckpoint(modelChkPnt, verbose=1, monitor='val_loss', save_weights_only=True)
    tensorboardCallback = TensorBoard(log_dir=logDir, histogram_freq=1)
    callbackScheduler = ReduceLROnPlateau(monitor='val_dice_coef', factor=0.5, patience=5, min_lr=0.00001)

    return modelCallback, tensorboardCallback, callbackScheduler

def Parser():
    '''
    Parser for framework parameters.
    Returns the argparser with the filled parameters
    -------
    '''
    # generalParser
    parser = argparse.ArgumentParser(description='Parser for Sypet framework params')

    # general arguments
    parser.add_argument("-v", "--verbosity", type=bool, help="verbosity option", default=False)
    parser.add_argument("-d", "--distributedTraining", type=bool, help="distributed training option", default=False)
    parser.add_argument("-i", "--inputDataDir", type=str, help="directory name of the input data", default=os.getcwd())
    parser.add_argument("-t", "--targetDataDir", type=str, help="directory name of the target data", default=os.getcwd())
    parser.add_argument("-m", "--modelDir", type=str, help="directory name of the model storage", default=os.getcwd())
    parser.add_argument("-f", "--modeFlag", type=int, help="mode: training(0), validation(1) and inference(2)", default=1)

    # modelParser
    parser.add_argument("-e", "--epoch", type=int, help="epochs to be trained", default=200)
    parser.add_argument("-s", "--stepsPerEpoch", type=int, help="steps per epoch to be trained", default=200)
    parser.add_argument("-l", "--layers", type=int, help="layers of the model", default=4)
    parser.add_argument("-kN", "--kernelNbr", type=int, help="number of convolution kernels on first layer", default=24)
    parser.add_argument("-kS", "--kernelSize", type=ast.literal_eval, help="convolution kernel size", default=(3, 3, 3))
    parser.add_argument("-k", "--kernelInitialization", type=str, help="kernel initialization method", default='tf.keras.initializers.GlorotUniform()')
    parser.add_argument("-dR", "--dropoutRate", type=float, help="drop out rate", default=0.5)
    parser.add_argument("-bN", "--batchNormalization", type=bool, help="batch normalization option", default=True)
    parser.add_argument("-lT", "--lossType", type=str, help="loss function", default='tf.keras.losses.MeanSquareError()')
    parser.add_argument("-o", "--optimizer", type=str, help="optimizer method", default='tf.keras.optimizers.Adam')
    parser.add_argument("-lR", "--learningRate", type=float, help="learning method", default=0.001)
    parser.add_argument("-lS", "--learningRateScheduler", type=bool, help="learning rate scheduler option", default=False)
    parser.add_argument("-aF", "--outputActivation", type=str, help="last layer activation function", default='Sigmoid')
    parser.add_argument("-aG", "--attentionGate", type=bool, help="attention gate", default=True)

    # datagenParser
    parser.add_argument("-iS", "--inputSize", type=ast.literal_eval, help="input size to be converted", default=(128, 128, 128))
    parser.add_argument("-iC", "--nbrInChannel", type=int, help="number of input channels", default=1)
    parser.add_argument("-oS", "--outputSize", type=ast.literal_eval, help="out shape", default=(128, 128, 128, 1))
    parser.add_argument("-bZ", "--batchSize", type=int, help="batch size for the Data generator", default=2)
    parser.add_argument("-n", "--normalization", type=bool, help="normalization option", default=True)
    parser.add_argument("-p", "--patching", type=bool, help="patching option", default=False)
    parser.add_argument("-nP", "--nbrPatch", type=int, help="number of patches", default=2)
    parser.add_argument("-a", "--augmentation", type=bool, help="augmentation option", default=False)
    # parser.add_argument("-p", "--preprocessing", type=bool, help="preprocessing option", default=False)

    args = parser.parse_args()

    if not args.verbosity:
        print('Verbosity is off')
    if not args.distributedTraining:
        print('Model will be trained on a single GPU.')
    if not args.inputDataDir:
        print('Input data dir is not defined. Default will be chosen')
    if not args.targetDataDir:
        print('Target data dir is not defined. Default will be chosen')
    if not args.modelDir:
        print('Model dir not defined. Default will be chosen')
    if not args.modeFlag:
        print('Mode is not defined. Default mode (training) will be chosen.')
    if not args.epoch:
        print('Option epochs is mandatory and it has not been set.')
    if not args.stepsPerEpoch:
        print('Steps per epoch are not defined, the default one will be chosen (#samples/batch_size).')
    if not args.layers:
        print('Layers is not defined, the default one will be chosen (3).')
    if not args.kernelNbr:
        print('Number of kernel is not defined, the default one will be chosen (24).')
    if not args.kernelSize:
        print('Kernel size is not defined, the default one will be chosen (3, 3, 3).')
    if not args.kernelInitialization:
        print('Kernel initialization is not defined, the default one will be chosen (Xavier Uniform).')
    if not args.dropoutRate:
        print('Dropout rate size is not defined, the default one will be chosen (0.5).')
    if not args.batchNormalization:
        print('Batch normalization is not defined, the default one will be chosen (True).')
    if not args.lossType:
        print('Type of loss is not defined, the default one will be chosen (MSE).')
    if not args.learningRate:
        print('Learning rate is not defined, the default one will be chosen (0.001).')
    if not args.learningRateScheduler:
        print('Learning rate scheduler is not defined, the default one will be chosen (False).')
    if not args.outputActivation:
        print('Output activation function is not defined, the default one will be chosen (Sigmoid).')
    if not args.inputSize:
        print('Input size is not defined, the default one will be chosen (128, 128, 128).')
    if not args.nbrInChannel:
        print('Input channel is not defined, the default one will be chosen (1).')
    if not args.batchSize:
        print('Batch size is not defined, the default one will be chosen (2).')
    if not args.augmentation:
        print('Augmentation option is not defined, the default one will be chosen (False).')
    # if not args.preprocessing:
    #     print('Preprocessing option is not defined, the default one will be chosen (False).')

    return args
