from scipy.ndimage import zoom, binary_fill_holes
from postprocessing.utilsNiftiProcessing import readNifti2Numpy

import os
import numpy as np
import scipy

def merge_patches(patch_arr: np.ndarray, origSize: int, batchSize: int, patchSize: int, stride: int, channel: int, modelReady: bool):
    """
    Takes patches or array of images and merges them
    If stride < size it will do overlapping.

    Returns:
        numpy.ndarray: [description]
    """
    # check size and stride
    if patchSize % stride != 0:
        raise ValueError("size % stride must be equal 0")

    overlapping = 0
    if stride != patchSize:
        overlapping = (patchSize // stride) - 1

    # number of patches and iterator
    itr_max = (origSize // stride) - overlapping

    # reshape patches
    # patch_arr = np.reshape(patch_arr.copy(), (batchSize, origSize, origSize, origSize, channel))

    # Create an array to store the reconstructed image
    image_array = np.zeros(shape=(batchSize, origSize, origSize, origSize, channel))

    if patch_arr.ndim == 5 and modelReady:
        for itrBatch in range(batchSize):
            for itrChannel in range(channel):
                cnt = 0

                for i in range(itr_max):
                    for j in range(itr_max):
                        for k in range(itr_max):
                            image_array[itrBatch,
                                        i * stride: i * stride + patchSize,
                                        j * stride: j * stride + patchSize,
                                        k * stride: k * stride + patchSize,
                                        itrChannel] = patch_arr[cnt, :, :, :, itrChannel]

                            cnt += 1

    return image_array

def merge_patches2(patch_arr: np.ndarray, origSize: int, batchSize: int, patchSize: int, stride: int, channel: int, modelReady: bool, patchClass: np.ndarray = None):
    """
    Takes patches or array of images and merges them
    If stride < size it will do overlapping.

    Returns:
        numpy.ndarray: [description]
    """
    # check size and stride
    if patchSize % stride != 0:
        raise ValueError("size % stride must be equal 0")

    overlapping = 0
    if stride != patchSize:
        overlapping = (patchSize // stride) - 1

    # number of patches and iterator
    itr_max = (origSize // stride) - overlapping

    # reshape patches
    # patch_arr = np.reshape(patch_arr.copy(), (batchSize, origSize, origSize, origSize, channel))

    # Create an array to store the reconstructed image
    image_array = np.zeros(shape=(batchSize, origSize, origSize, origSize, channel))

    if patchClass is not None:
        heatmap_array = np.zeros(shape=image_array.shape)
        # Create arr2 by reshaping and broadcasting
        patchClassTemp = patchClass[:, np.newaxis, np.newaxis, np.newaxis, np.newaxis]

        # Expand dimensions for broadcasting
        patchClassTemp = np.repeat(patchClassTemp, patch_arr.shape[1], axis=1)
        patchClassTemp = np.repeat(patchClassTemp, patch_arr.shape[2], axis=2)
        patchClassTemp = np.repeat(patchClassTemp, patch_arr.shape[3], axis=3)

        # Expand last dimension to desired shape
        patchClassTemp = np.repeat(patchClassTemp, patch_arr.shape[-1], axis=-1)

    if patch_arr.ndim == 5 and modelReady:
        for itrBatch in range(batchSize):
            for itrChannel in range(channel):
                cnt = 0

                for i in range(itr_max):
                    for j in range(itr_max):
                        for k in range(itr_max):
                            image_array[itrBatch, i * stride: i * stride + patchSize, j * stride: j * stride + patchSize, k * stride: k * stride + patchSize, itrChannel] = patch_arr[cnt, :, :, :, itrChannel]

                            if patchClass is not None:
                                heatmap_array[itrBatch, i * stride: i * stride + patchSize, j * stride: j * stride + patchSize, k * stride: k * stride + patchSize, itrChannel] = patchClassTemp[cnt, :, :, :, itrChannel]

                            cnt += 1

    if patchClass is None:
        return image_array

    if patchClass is not None:
        return image_array, heatmap_array

def removeDims(curArray: np.ndarray):
    # remove extra dims
    curArray = np.squeeze(curArray.copy(), axis=0)
    curArray = np.squeeze(curArray.copy(), axis=-1)

    return curArray

def get_patches(img_arr: np.ndarray, size: int, stride: int, modelReady: bool):
    """
    Takes single image or array of images and returns
    crops using sliding window method.
    If stride < size it will do overlapping.

    Args:
        img_arr (numpy.ndarray): [description]
        size (int, optional): [description]. Defaults to 256.
        stride (int, optional): [description]. Defaults to 256.

    Raises:
        ValueError: [description]
        ValueError: [description]

    Returns:
        numpy.ndarray: [description]
    """
    # check size and stride
    if size % stride != 0:
        raise ValueError("size % stride must be equal 0")

    patches_list = []
    overlapping = 0
    if stride != size:
        overlapping = (size // stride) - 1

    if img_arr.ndim == 2:
        i_max = img_arr.shape[0] // stride - overlapping
        j_max = img_arr.shape[1] // stride - overlapping

        for i, j in zip(range(i_max), range(j_max)):
            patches_list.append(img_arr[i * stride: i * stride + size, j * stride: j * stride + size])

    if img_arr.ndim == 3:
        i_max = (img_arr.shape[0] // stride) - overlapping
        j_max = (img_arr.shape[1] // stride) - overlapping
        k_max = (img_arr.shape[2] // stride) - overlapping

        for i in range(i_max):
            for j in range(j_max):
                for k in range(k_max):
                    patches_list.append(img_arr[i * stride: (i * stride) + size, j * stride: (j * stride) + size, k * stride: (k * stride) + size])

    if img_arr.ndim == 5 and modelReady:
        i_max = (img_arr.shape[1] // stride) - overlapping
        j_max = (img_arr.shape[2] // stride) - overlapping
        k_max = (img_arr.shape[3] // stride) - overlapping

        for i in range(i_max):
            for j in range(j_max):
                for k in range(k_max):
                    patches_list.append(img_arr[:, i * stride: (i * stride) + size, j * stride: (j * stride) + size, k * stride: (k * stride) + size, :])

    # assumes batch and channel dimensions are always one - so you pass each channel separately
    outArr = np.array(patches_list)

    if outArr.ndim == 6 and modelReady:
        outArr = np.squeeze(outArr.copy(), axis=1)

    return outArr

def safeCastArrays(origVol:np.ndarray):
    '''
    Safe casting int ndarray to logical
    :param vol1: nd-array

    :returns: the logical array
    -------

    '''
    if not (isinstance(origVol, np.ndarray)):
        raise AssertionError('Input argument is not numpy nd-arrays.')

    lowerThresh = 0.5
    upperThresh = 1

    logicalVol = origVol.copy()

    logicalVol[logicalVol < lowerThresh] = False
    logicalVol[logicalVol >= upperThresh] = True

def compareDimension(dim1: int, dim2: int, diffFlag: bool):
    '''
    Performs check for dimensionality equality between the inputs.
    Params:
    dim1: dimension number one to check against dim2
    dim2: dimension number two to check against dim1
    diffFlag: flag to return the difference of the two dimensions

    Returns:
        dimensionStatus
    '''
    #
    # if not isinstance(dim1, int):
    #     raise AssertionError('dim1 should be an integer.')
    # if not isinstance(dim2, int):
    #     raise AssertionError('dim2 should be an integer.')

    if dim1 < 1 or dim2 < 1:
        raise ValueError('dim1 and/or dim2 have value lower or equal to zero.')

    if dim1 == dim2:
        dimensionStatus = 0

    elif dim1 > dim2:
        dimensionStatus = -1

    elif dim1 < dim2:
        dimensionStatus = 1
    else:
        raise ValueError('Something went with the dimensionality comparison')

    if diffFlag:
        diffDim = abs(dim1 > dim2)
    else:
        diffDim = 0

    return dimensionStatus, diffDim

def compareArrays(arr1: np.ndarray, arr2: np.ndarray, *args):
    '''
    Unit check for comparison of arrays size.

    :param arr1: nd-array #1
    :param arr2: nd-array #2
    :param args: support multiple arrays comparison
    :return: boolean flag if all arrays have same length
    '''

    if not(isinstance(arr1, np.ndarray)) or not(isinstance(arr2, np.ndarray)):
        raise AssertionError('Input arguments are not numpy nd-arrays.')

    if arr1.shape == arr2.shape:
        print('Arrays have same length.')
        flagCheck = True
    else:
        print('Arrays have different length.')
        print('Visualization disabled.')
        flagCheck = False

    for argItr in args:
        if arr1.shape == argItr.shape:
            print('Arrays have same length.')
            flagCheck = True
        else:
            # not tracking which array has different length
            print('Arrays have different length.')
            print('Visualization disabled.')
            flagCheck = False

    return flagCheck

def padArray(origVolume: np.ndarray, targetShape: tuple):
    '''
    Helper function for padding an array to a desired shaped using constant zeros.
    Currently, supports 3D and 3D arrays with shape (ArrWidth, ArrLength, ArrDepth, channels).
    Param:
    origVolume: array to be padded
    targetShape: shape to be padded

    Returns: padded Volume
    '''

    if not (isinstance(origVolume, np.ndarray)):
        raise AssertionError('origVolume should be type np.ndarray.')

    if not isinstance(targetShape, tuple):
        raise AssertionError('targetShape should be type tuple.')

    if not origVolume.ndim == len(targetShape):
        raise AssertionError('origVolume and targetShape should have the same shape.')

    # it should never reach that if statement but just in case
    if not targetShape:
        if origVolume.ndim == 2 or (origVolume.ndim == 3 and origVolume.shape[-1] == 1):
            targetShape = (256, 256)
        if origVolume.ndim == 3 and origVolume.shape[-1] > 5: # currently supports 2D and 3D arrays up to 5 channels each
            targetShape = (256, 256, 256)

    # currently only isotropic and fixed size input is supported
    diff_Axis1 = ((targetShape[0] - origVolume.shape[0]) // 2, (targetShape[0] - origVolume.shape[0]) // 2)
    diff_Axis2 = ((targetShape[1] - origVolume.shape[1]) // 2, (targetShape[1] - origVolume.shape[1]) // 2)

    if diff_Axis1[0] < 0 or diff_Axis2[0] < 0:
        if diff_Axis1[0] < 0: # perform cropping in axis 1
            crop_Axis1 = (0 + abs(diff_Axis1[0]), origVolume.shape[0] + diff_Axis1[0])
        else:
            crop_Axis1 = (0, origVolume.shape[0])

        if diff_Axis2[0] < 0: # perform cropping in axis 2
            crop_Axis2 = (0 + abs(diff_Axis2[0]), origVolume.shape[0] + diff_Axis2[0])
        else:
            crop_Axis2 = (0, origVolume.shape[1])

        padImg = origVolume[crop_Axis1[0]:crop_Axis1[1], crop_Axis2[0]:crop_Axis2[1]]

        if origVolume.ndim == 3 and origVolume.shape[-1] > 1:
            diff_Axis3 = ((targetShape[2] - origVolume.shape[2]) // 2, (targetShape[2] - origVolume.shape[2]) // 2)

            if diff_Axis3[0] < 0:  # perform cropping in axis 3
                crop_Axis3 = (0 + abs(diff_Axis3[0]), origVolume.shape[0] + diff_Axis3[0])
            else:
                crop_Axis3 = (0, origVolume.shape[2])

            padImg = origVolume[crop_Axis1[0]:crop_Axis1[1], crop_Axis2[0]:crop_Axis2[1], crop_Axis3[0]:crop_Axis3[1]]

    else:
        # when padding window will be divided by an even number and generates cropped padding output
        finalSize_Axis1 = diff_Axis1[0] + diff_Axis1[1] + origVolume.shape[0]

        if finalSize_Axis1 < targetShape[0]:
            additionalPad = targetShape[0] - finalSize_Axis1
            diff_Axis1 = ((targetShape[0] - origVolume.shape[0]) // 2 + additionalPad, (targetShape[0] - origVolume.shape[0]) // 2)

        # cropping if exceeding the target shape - should not fall under this statement but serves as double check
        elif finalSize_Axis1 > targetShape[0]:
            additionalPad = targetShape[0] - finalSize_Axis1
            diff_Axis1 = ((targetShape[0] - origVolume.shape[0]) // 2 - additionalPad, (targetShape[0] - origVolume.shape[0]) // 2)

            del additionalPad

        finalSize_Axis2 = diff_Axis2[0] + diff_Axis2[1] + origVolume.shape[1]

        if finalSize_Axis2 < targetShape[1]:
            additionalPad = targetShape[1] - finalSize_Axis2
            diff_Axis2 = ((targetShape[1] - origVolume.shape[1]) // 2 + additionalPad, (targetShape[1] - origVolume.shape[1]) // 2)

        # cropping if exceeding the target shape - should not fall under this statement but serves as double check
        elif finalSize_Axis2 > targetShape[1]:
            additionalPad = targetShape[1] - finalSize_Axis2
            diff_Axis2 = ((targetShape[1] - origVolume.shape[1]) // 2 - additionalPad, (targetShape[1] - origVolume.shape[1]) // 2)

            del additionalPad

        padShape = (diff_Axis1, diff_Axis2)

        if origVolume.ndim == 3 and origVolume.shape[-1] > 1:
            diff_Axis3 = ((targetShape[2] - origVolume.shape[2]) // 2, (targetShape[2] - origVolume.shape[2]) // 2)
            finalSize_Axis3 = diff_Axis3[0] + diff_Axis3[1] + origVolume.shape[2]

            if finalSize_Axis3 < targetShape[2]:
                additionalPad = targetShape[2] - finalSize_Axis3
                diff_Axis3 = ((targetShape[2] - origVolume.shape[2]) // 2 + additionalPad, (targetShape[2] - origVolume.shape[2]) // 2)

            # cropping if exceeding the target shape - should not fall under this statement but serves as double check
            elif finalSize_Axis3 > targetShape[2]:
                additionalPad = targetShape[2] - finalSize_Axis3
                diff_Axis3 = ((targetShape[2] - origVolume.shape[2]) // 2 - additionalPad, (targetShape[2] - origVolume.shape[2]) // 2)

                del additionalPad

            padShape = (diff_Axis1, diff_Axis2, diff_Axis3)

        padImg = np.pad(array=origVolume, pad_width=padShape, mode="constant", constant_values=0)

    return padImg

def resizeArray(inputArray: np.ndarray, resizeWeight: float):
    '''
    Helper function for resizing an array.
    :param inputArray: array to be resized
    :param resizeWeight: shape to be resized
    :return: resized input
    '''

    if not (isinstance(inputArray, np.ndarray)):
        raise AssertionError('Volume is not numpy array.')

    if inputArray.ndim < 3:
        resizedArray = zoom(inputArray, (resizeWeight, resizeWeight))
    elif inputArray.ndim == 3:
        resizedArray = zoom(inputArray, (resizeWeight, resizeWeight, resizeWeight))

    return resizedArray

def cropArray(inputArray: np.ndarray, desiredShape: tuple):
    '''
    Cropping a numpy array to the given shape
    :param inputArray
    :param desiredShape
    :return: cropped array
    '''

    if not (isinstance(inputArray, np.ndarray)):
        raise AssertionError('inputArray is not a numpy array.')

    if not (isinstance(desiredShape, tuple)):
        raise AssertionError('desiredShape is not a tuple.')

    class cropDetails():
        # dummy object for storing cropping info. NOTE: it will apply a cropping before and after similar to np.pad function padding
        def __init__(self):
            self.before = 0
            self.after = 0

    inputShape = inputArray.shape

    if inputArray.ndim == 2:
        cropAxis1 = cropDetails()
        cropAxis1.before, cropAxis1.after = desiredShape[0][0], desiredShape[0][0]
        cropAxis2 = cropDetails()
        cropAxis2.before, cropAxis2.after = desiredShape[1][0], desiredShape[1][0]

        croppedArray = inputArray[0 + cropAxis1.before:inputArray.shape[0] - cropAxis1.after,
                                  0 + cropAxis2.before:inputArray.shape[1] - cropAxis2.after]

        if inputArray.ndim == 3:
            cropAxis3 = cropDetails()
            cropAxis3.before, cropAxis3.after = desiredShape[2][0], desiredShape[2][0]

            croppedArray = croppedArray[:, :, 0 + cropAxis3.before:inputArray.shape[2] - cropAxis3.after]

        print('From shape', inputShape, 'and after cropping is', croppedArray.shape)

        return croppedArray

def normalizeArray(inputArray: np.ndarray):
    '''
    Normalizing a numpy array between [0,1]
    :param inputArray
    :return: normalized array and state if division by 0 or nan
    '''

    if not (isinstance(inputArray, np.ndarray)):
        raise AssertionError('inputArray is not a numpy array.')

    if np.max(inputArray) <= 0:
        normalizedArray = inputArray
        divisionState = 0
    else:
        normalizedArray = (inputArray - np.min(inputArray))/(np.max(inputArray) - np.min(inputArray))
        divisionState = 1

    return normalizedArray, divisionState

def standardizeArray(inputArray: np.ndarray):
    '''
    Standardizing a numpy array based on mean and std
    :param inputArray
    :return: standardized array
    '''

    if not (isinstance(inputArray, np.ndarray)):
        raise AssertionError('inputArray is not a numpy array.')

    standardizedArray = (inputArray - inputArray.mean())/inputArray.std()

    return standardizedArray

def isNanArray(inputArray: np.ndarray):
    '''
    Unit check if inputArray contains NaN values.
    params: np.nd-array
    Return: 1 if input contains NaN values, otherwise 0.
    '''

    if not (isinstance(inputArray, np.ndarray)):
        raise AssertionError('inputArray is not a numpy array.')

    checkNan = np.unique(np.ravel(np.isnan(inputArray)))

    if checkNan.shape[0] == 2:  exitStatus = 1 # this is if it has None among other values
    if bool(checkNan.shape[0] == 1 and checkNan):  exitStatus = 2 # this is if only NaN found
    if bool(checkNan.shape[0] == 1 and not checkNan):   exitStatus = 0 # this is if there is not NaN

    return exitStatus

def checkDirNan(dirPath: str):

    nbrFiles = os.listdir(dirPath)

    for itr in range(len(nbrFiles)):
        curFile, _ = readNifti2Numpy(os.path.join(dirPath, nbrFiles[itr]))

        if isNanArray(curFile):
            print('File', nbrFiles[itr], 'has NaN values.')

def fillNanArray(inputArray: np.ndarray):
    '''
    Filling with NaN values the inputArray.
    params: np.nd-array
    Return: Numpy array without NaN values
    '''

    if not (isinstance(inputArray, np.ndarray)):
        raise AssertionError('inputArray is not a numpy array.')

    nanStatus = isNanArray(inputArray)

    if nanStatus:
        filledArray = np.nan_to_num(inputArray.copy())
        # print('Input found with NaN values and those have been filled.')
    else:
        filledArray = inputArray.copy()

    return filledArray

def fixedSize(currentArray:np.ndarray, desiredSize: tuple):
    '''
    Obtain a fixed size array based on the provided size from self.dim.
    Params:
    currentArray: np.ndarray, the original input array

    Returns:
        Array size with a fixed size
    '''

    '''
    1. check dimensionality - 3D or 4D (inc. channel dimension) - d
        1.1 if 3D do this 3D pipeline
        1.2 if 4D do this 4D pipeline
    2. work per dimension, excluding last one which stands for channels
        2.1 if equal, then leave it as it is  - d
        2.2 if larger crop? or split into two parts?
        2.3 if smaller pad - d
    3. Obtain the fixed size array
    '''

    class compareArrStatus():
        # dummy object for comparing dimensions between objects
        def __init__(self):

            self.dimStatus = 0
            self.dimLst = 0
            self.action = 'none'

    if not isinstance(currentArray, np.ndarray):
        raise AssertionError('currentArray should be type np.ndarray.')

    if not isinstance(desiredSize, tuple):
        raise AssertionError('desiredSize should be a tuple.')

    if currentArray.ndim == 2 or (currentArray.ndim == 3 and currentArray.shape[-1] == 1):
        if currentArray.ndim == 3: # remove last channel with size 1 for clarity reasons
            currentArray = np.squeeze(currentArray.copy(), axis=-1)

        if not currentArray.ndim == len(desiredSize):
            raise ValueError('currentArray and desiredSize should have same dimensions')

    # it supports 2D or 3D data with single or multiple channels
    # add a unit check here - dummy object keeping track of differences
    arrStatus_Axis1 = compareArrStatus()
    arrStatus_Axis2 = compareArrStatus()

    if len(desiredSize) == 3:
        arrStatus_Axis3 = compareArrStatus()

    # check dim1, dim2, ..., n
    for itrDim in range(len(desiredSize)):
        dimStatusTemp, diffLstTemp = compareDimension(dim1=currentArray.shape[itrDim], dim2=desiredSize[itrDim], diffFlag=True)

        if itrDim == 0:
            arrStatus_Axis1.dimStatus = dimStatusTemp
            arrStatus_Axis1.dimLst = diffLstTemp

        if itrDim == 1:
            arrStatus_Axis2.dimStatus = dimStatusTemp
            arrStatus_Axis2.dimLst = diffLstTemp

        if len(desiredSize) == 3:
            if itrDim == 2:
                arrStatus_Axis3.dimStatus = dimStatusTemp
                arrStatus_Axis3.dimLst = diffLstTemp

    if arrStatus_Axis1.dimStatus > 0:
        arrStatus_Axis1.action = 'pad'
    elif not arrStatus_Axis1.dimStatus <= 0:
        pass

    if arrStatus_Axis2.dimStatus > 0:
        arrStatus_Axis2.action = 'pad'
    elif not arrStatus_Axis2.dimStatus <= 0:
        pass

    if len(desiredSize) == 3:
        if arrStatus_Axis3.dimStatus > 0:
            arrStatus_Axis3.action = 'pad'
        elif not arrStatus_Axis3.dimStatus <= 0:
            pass

    if len(desiredSize) == 2:
        if arrStatus_Axis1.action == 'pad' or arrStatus_Axis2.action == 'pad':
            currentArray = padArray(currentArray.copy(), desiredSize)
        else:
            currentArrayPadshape = ((0, 0), (0, 0))
            # print('Padding ignored. Image had already desired shape.')

    if len(desiredSize) == 3:
        if arrStatus_Axis1.action == 'pad' or arrStatus_Axis2.action == 'pad' or arrStatus_Axis3.action == 'pad':
            currentArray = padArray(currentArray.copy(), desiredSize)
        else:
            currentArrayPadshape = ((0, 0, 0), (0, 0, 0))
            # print('Padding ignored. Image had already desired shape.')

    currentArrayPadshape = ((0, 0, 0), (0, 0, 0))

    return currentArray, currentArrayPadshape

def convertPred2OriginalShape(predictionsArrLst: list, gtArrLst: list, validGenerator, datagenParams):
    # go back to original size
    predictionsArrOrigShape = []

    for itrPred in range(len(predictionsArrLst)):
        _, padShapeTemp = fixedSize(gtArrLst[itrPred], datagenParams['dim'])
        tempPredArr = np.squeeze(predictionsArrLst[itrPred], axis=-1)
        predictionsArrOrigShape.append(cropArray(tempPredArr, padShapeTemp))

    return predictionsArrOrigShape

def removeBackgroundPair_np(inputArray: np.ndarray, targetArray: np.ndarray, modelReady=False):
    '''
    :param
    inputArray and targetArray: file in ndarray format to remove background
    modelReady: if True expects ndim == 5, if False expects ndim == 3
    :return: np.ndarray without background pixels
    '''

    # removing any pixels outside of brain region
    brainMask = np.where(inputArray > 0, 1, 0)

    if modelReady:
        _, X, Y, Z, _ = np.nonzero(brainMask)
    else:
        X, Y, Z = np.nonzero(brainMask)

    if X.min() == X.max() or Y.min() == Y.max() or Z.min() == Z.max():
        raise ValueError('inputArray is empty')

    if modelReady:
        return inputArray[:, X.min():X.max(), Y.min():Y.max(), Z.min():Z.max(), :], targetArray[:, X.min():X.max(), Y.min():Y.max(), Z.min():Z.max(), :]

    else:
        return inputArray[X.min():X.max(), Y.min():Y.max(), Z.min():Z.max()], targetArray[X.min():X.max(), Y.min():Y.max(), Z.min():Z.max()]

def removeBackground_np(inputArray: np.ndarray, modelReady=False, cropIdx=None):
        '''
        :param
        inputArray: file in ndarray format to remove background
        modelReady: if True expects ndim == 5, if False expects ndim == 3
        cropIdx: indexes to be masked
        :return: np.ndarray without background pixels
        '''

        if cropIdx is not None:
            idx = 0

            if modelReady:
                return inputArray[:, cropIdx[idx]:cropIdx[idx+1], cropIdx[idx+2]:cropIdx[idx+3],
                                  cropIdx[idx+4]:cropIdx[-1], :], cropIdx

            else:
                return inputArray[cropIdx[idx]:cropIdx[idx+1], cropIdx[idx+2]:cropIdx[idx+3],
                                  cropIdx[idx+4]:cropIdx[-1]], cropIdx

        else:

            inputArray, _ = normalizeArray(inputArray.copy())

            # removing any pixels outside of brain region
            brainMask = np.where(inputArray > 0.05, 1.0, 0.0)

            # Find the indices where the mask equals 1
            brainMask2 = []
            for itrZ in range(brainMask.shape[-1]):
                brainMask2.append(binary_fill_holes(brainMask[:, :, itrZ]).astype(int))

            brainMask2 = np.array(brainMask2).transpose(1, 2, 0)

            brainMask = brainMask2
            del brainMask2

            if modelReady:
                _, X, Y, Z, _ = np.nonzero(brainMask)
            else:
                X, Y, Z = np.nonzero(brainMask)

            if X.min() == X.max() or Y.min() == Y.max() or Z.min() == Z.max():
                raise ValueError('inputArray is empty')

            idxLst = [X.min(), X.max(), Y.min(), Y.max(), Z.min(), Z.max()]

            if modelReady:
                return inputArray[:, X.min():X.max(), Y.min():Y.max(), Z.min():Z.max(), :], idxLst

            else:
                return inputArray[X.min():X.max(), Y.min():Y.max(), Z.min():Z.max()], idxLst

def randomRotation(input_array, angle_range=(-30, 30)):
    batch_size = input_array.shape[0]
    angles = np.random.uniform(low=angle_range[0], high=angle_range[1], size=batch_size)

    # Rotate input array
    rotated_input = np.empty_like(input_array)
    for idx in range(batch_size):
        for channel in range(input_array.shape[-1]):
            rotated_input[idx, ..., channel] = scipy.ndimage.rotate(input_array[idx, ..., channel], angle=angles[idx], reshape=False)

    return rotated_input

def randomRotationPair(input_array, target_array, angle_range=(-30, 30)):
    batch_size = input_array.shape[0]
    angles = np.random.uniform(low=angle_range[0], high=angle_range[1], size=batch_size)

    # Rotate input array
    rotated_input = np.empty_like(input_array)
    for idx in range(batch_size):
        for channel in range(input_array.shape[-1]):
            rotated_input[idx, ..., channel] = scipy.ndimage.rotate(input_array[idx, ..., channel], angle=angles[idx], reshape=False)

    # Rotate target array
    rotated_target = np.empty_like(target_array)
    for idx in range(batch_size):
        for channel in range(target_array.shape[-1]):
            rotated_target[idx, ..., channel] = scipy.ndimage.rotate(target_array[idx, ..., channel], angle=angles[idx],reshape=False)

    return rotated_input, rotated_target

def rotate_3d(array, angle):
    """
    Rotate a 3D array by a given angle.

    Args:
        array (np.ndarray): Input array with shape (dim1, dim2, dim3).
        angle (float): Rotation angle in degrees.

    Returns:
        Rotated array.
    """
    rotated_array = np.rot90(array, k=int(angle / 90), axes=(0, 1))
    rotated_array = np.rot90(rotated_array, k=int(angle / 90), axes=(0, 2))
    rotated_array = np.rot90(rotated_array, k=int(angle / 90), axes=(1, 2))

    return rotated_array

def randomTranslation(input_array, translation_range=(-10, 10)):
    """
    Perform random translation of 5D arrays.

    Args:
        input_array (np.ndarray): Input array with shape (batch, dim1, dim2, dim3, channels).
        target_array (np.ndarray): Target array with shape (batch, dim1, dim2, dim3, channels).
        translation_range (tuple): Range of translation offsets. Default is (-10, 10).

    Returns:
        Tuple of translated input array.
    """
    batch_size = input_array.shape[0]
    translations = np.random.uniform(low=translation_range[0], high=translation_range[1], size=(batch_size, 3))

    # Translate input array
    translated_input = np.empty_like(input_array)
    for idx in range(batch_size):
        for channel in range(input_array.shape[-1]):
            translated_input[idx, ..., channel] = np.roll(input_array[idx, ..., channel],
                                                          shift=(int(translations[idx, 0]), int(translations[idx, 1]), int(translations[idx, 2])),
                                                          axis=(0, 1, 2))

    return translated_input

def randomTranslationPair(input_array, target_array, translation_range=(-10, 10)):
    """
    Perform random translation on pairs of 5D arrays.

    Args:
        input_array (np.ndarray): Input array with shape (batch, dim1, dim2, dim3, channels).
        target_array (np.ndarray): Target array with shape (batch, dim1, dim2, dim3, channels).
        translation_range (tuple): Range of translation offsets. Default is (-10, 10).

    Returns:
        Tuple of translated input array and translated target array.
    """
    batch_size = input_array.shape[0]
    translations = np.random.uniform(low=translation_range[0], high=translation_range[1], size=(batch_size, 3))

    # Translate input array
    translated_input = np.empty_like(input_array)
    for idx in range(batch_size):
        for channel in range(input_array.shape[-1]):
            translated_input[idx, ..., channel] = np.roll(input_array[idx, ..., channel],
                                                          shift=(int(translations[idx, 0]), int(translations[idx, 1]), int(translations[idx, 2])),
                                                          axis=(0, 1, 2))

    # Translate target array
    translated_target = np.empty_like(target_array)
    for idx in range(batch_size):
        for channel in range(target_array.shape[-1]):
            translated_target[idx, ..., channel] = np.roll(target_array[idx, ..., channel],
                                                           shift=(int(translations[idx, 0]), int(translations[idx, 1]),
                                                                  int(translations[idx, 2])), axis=(0, 1, 2))

    return translated_input, translated_target
