import nibabel as nib
import numpy as np
import os

def getAffineMatrix(numpyArrLst: list, niftiDir: str = None):
    if niftiDir is not None:
        if isinstance(numpyArrLst[0], list):
            # get only first channel - this depends on user choice
            numpyArrLst = [os.path.join(niftiDir, x[0]) for x in numpyArrLst]
        else:
            numpyArrLst = [os.path.join(niftiDir, x) for x in numpyArrLst]

    affineArrLst = []
    currArrLst = []

    for itrLst in range(len(numpyArrLst)):
        currArr, affineArrLstTemp = readNifti2Numpy(numpyArrLst[itrLst])

        affineArrLst.append(affineArrLstTemp)
        currArrLst.append(currArr)

    return affineArrLst, currArrLst

def convertNumpyArray2Nifti(numpyArr: np.ndarray, affineArrLst: np.ndarray, fileName: str, dir2SaveData: str):

    if not os.path.exists(dir2SaveData):
        os.mkdir(dir2SaveData)

    niftiCurFile = nib.Nifti1Image(numpyArr, affine=affineArrLst)

    niftiCurFileSavePath = os.path.join(dir2SaveData, fileName)

    nib.save(niftiCurFile, niftiCurFileSavePath)

    print('Array converted into nifti file and saved.')


def readNiftiPair2Numpy(niftiSequencePathFile: str, niftiLabelPathFile: str):
    '''
    Helper function to read nifti files (sequence and label) and converting it to numpy arrays
    :param dataPath: path to read nifti file
    :return: numpy nd-arrays
    '''

    if not isinstance(niftiSequencePathFile, str):
        raise AssertionError('The niftiSequencePathFile should be a string defining the name of the sequence nifti file.')

    if not isinstance(niftiLabelPathFile, str):
        raise AssertionError('The niftiLabelPathFile should be a string defining the name of the label nifti file.')

    niSequence, _ = readNifti2Numpy(niftiSequencePathFile)
    niLabel, _ = readNifti2Numpy(niftiLabelPathFile)

    return niSequence, niLabel

def readNifti2Numpy(dataPath: str):
    '''
    Helper function to read nifti file (sequence or label) and converting it to numpy array
    :param dataPath: path to read nifti file
    :return: numpy nd-array of nifti file and corresponding affine matrix
    '''

    if not isinstance(dataPath, str):
        raise AssertionError('The dataPath should be a string defining the name of the nifti file.')

    niData = nib.load(dataPath)
    affineData = niData.affine
    niData = np.array(niData.get_fdata())

    return niData, affineData

def readNiftiAndMask2Numpy(input_dir: list, target_dir: list):
    input_data = []
    target_data = []
    mask_data = []

    # TODO: change code from str to list for input arguments

    for filename in sorted(os.listdir(input_dir)):
        if filename.endswith(".nii") or filename.endswith(".nii.gz"):
            input_file = os.path.join(input_dir, filename)
            input_data.append(input_file)

    for filename in sorted(os.listdir(target_dir)):
        if filename.endswith(".nii") or filename.endswith(".nii.gz"):
            target_file = os.path.join(target_dir, filename)
            if filename.endswith('mask.gz') or filename.endswith("mask.nii.gz"):

                mask_data.append(target_file)
            else:
                target_data.append(target_file)

    return input_data, target_data, mask_data

def readNifti2NumpyAndHeader(dataPath: str):
    '''
    Helper function to read nifti file (sequence or label) and converting it to numpy array
    :param dataPath: path to read nifti file
    :return: numpy nd-array of nifti file and corresponding voxel size
    '''

    if not isinstance(dataPath, str):
        raise AssertionError('The dataPath should be a string defining the name of the nifti file.')

    niData = nib.load(dataPath)

    sizeFirstAxis, sizeSecondAxis, sizeThirdAxis = niData.header.get_zooms()
    voxelSize = (sizeFirstAxis, sizeSecondAxis, sizeThirdAxis)

    niData = np.array(niData.get_fdata())

    return niData, voxelSize
