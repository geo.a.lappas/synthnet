from postprocessing.utilsArrayProcessing import *
from postprocessing.utilsNiftiProcessing import *
from scipy import ndimage
from keras.utils.image_utils import array_to_img
from pathlib import Path

import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.axes
import matplotlib.animation as animation

matplotlib.use('TKAgg')

def contourOutlineInit(volume):
    '''
    Extracting the boundary indices. It does not matter processing whole volume cause most values are zeros.

    Parameters
    ----------
    volume: binary volume of lesion labels / it works for single class data

    Returns: Outlines of the contours found, slice-wise
    -------
    '''

    # this is in 3D
    croppedVolume = volume.copy()

    croppedVolumeDilated = ndimage.morphology.binary_dilation(croppedVolume, structure=np.ones(shape=(5, 5, 1)))
    croppedVolumeDiff = croppedVolumeDilated.astype(np.float) - croppedVolume

    return croppedVolumeDiff

def orientationMask(maskShape: tuple, plane: str):
    '''
    Label mask containing the directions (A:anterior, P:posterior, S:superior, I:inferior, L:left, R:right) of brain in 3D.
    Parameters
    ----------
    maskShape: shape of the mask to be created
    plane: plane. different plane views in 2d, they show different directions

    Returns: mask with labels
    -------

    '''
    if not(isinstance(maskShape, tuple)):
        raise AssertionError('Shape of the mask volume is not given in the correct format.')

    maskArr = np.zeros(shape=maskShape)
    offset = 10 # offset value - arbitrary selection

    # adding three voxels spacing
    class tempObj:
        pass

    if plane == 'axial':
        textLeft = tempObj()
        textRight = tempObj()
        textPosterior = tempObj()
        textAnterior = tempObj()

        textLeft.pos = (maskArr.shape[0] // 2, 0+offset)
        textLeft.name = 'anterior'
        textRight.pos = (maskArr.shape[0] // 2, maskArr.shape[1]-offset)
        textRight.name = 'posterior'

        textAnterior.pos = (0+offset, maskArr.shape[1] // 2)
        textAnterior.name = 'right'
        textPosterior.pos = (maskArr.shape[0]-offset, maskArr.shape[1] // 2)
        textPosterior.name = 'left'

        positionLst = list([textLeft, textRight, textPosterior, textAnterior])

    if plane == 'sagittal':
        textLeft = tempObj()
        textRight = tempObj()
        textSuperior = tempObj()
        textInferior = tempObj()

        textLeft.pos = (maskArr.shape[0] // 2, 0 + offset)
        textLeft.name = 'superior'
        textRight.pos = (maskArr.shape[0] // 2, maskArr.shape[1] - offset)
        textRight.name = 'inferior'

        textSuperior.pos = (0 + offset, maskArr.shape[1] // 2)
        textSuperior.name = 'left'
        textInferior.pos = (maskArr.shape[0] - offset, maskArr.shape[1] // 2)
        textInferior.name = 'right'

        positionLst = list([textLeft, textRight, textSuperior, textInferior])

    if plane == 'coronal':
        textAnterior = tempObj()
        textPosterior = tempObj()
        textSuperior = tempObj()
        textInferior = tempObj()

        textAnterior.pos = (maskArr.shape[1] // 2, 0 + offset)
        textAnterior.name = 'superior'
        textPosterior.pos = (maskArr.shape[1] // 2, maskArr.shape[0] - offset)
        textPosterior.name = 'inferior'

        textSuperior.pos = (0 + offset, maskArr.shape[1] // 2)
        textSuperior.name = 'anterior'
        textInferior.pos = (maskArr.shape[0] - offset, maskArr.shape[1] // 2)
        textInferior.name = 'posterior'

        positionLst = list([textAnterior, textPosterior, textSuperior, textInferior])

    return positionLst



class slicerPlot:
    '''
    Visualizing volume into 2D slices.
    '''

    def __init__(self, getAxes: list, volumeSeq: np.ndarray, fillingFlag: bool, multiplaneFlag: bool, *args):
        '''
        Parameters
        ----------
        getAxis: tuple of plt.axes, this needs to be declared in the object initialization
        volumeSeq: volume to be plotted, currently supports only nd.arrays
        fillingFlag: boolean var, if True and masks are provided, the contours will be visualized as filled cavities
        multiplane: boolean var, if True, figure will plot all planes, if False figure will plot the axial plane
        args: if masks are available, they will be added
        '''

        if isinstance(getAxes, list):
            if not(isinstance(all(getAxes), matplotlib.axes._subplots.Axes)):
                raise AssertionError('One or more of the axes passed is not plt.axes type.')

        if not (isinstance(volumeSeq, np.ndarray)):
            raise AssertionError('Volume is not numpy array.')

        if not('fillingFlag' in locals()):
            fillingFlag = False
            if not(isinstance(fillingFlag, bool)):
                raise AssertionError('Option for filling is non-boolean.')

        if not(isinstance(multiplaneFlag, bool)):
            raise AssertionError('Option for multiplane view is non-boolean.')

        self.volumeSeq = np.array(volumeSeq)
        self.fillingFlag = fillingFlag
        self.multiplaneFlag = multiplaneFlag
        self.rotNbr = 3

        if self.multiplaneFlag:
            self.axis_1 = getAxes[0]
            self.axis_2 = getAxes[1]
            self.axis_3 = getAxes[2]
            axesItr = 3
        else:
            self.axis_1 = getAxes
            axesItr = 1

        textPos = []
        subplotNames = []
        planeName = ['axial', 'sagittal', 'coronal']

        for itr in range(axesItr):

            textPos.append(orientationMask(self.volumeSeq.shape, planeName[itr]))
            subplotNames.append([textPos[itr][0].name, textPos[itr][1].name, textPos[itr][2].name, textPos[itr][3].name])

            if 'left' in subplotNames[itr]:
                tmpIdx = subplotNames[itr].index('left')

                if planeName[itr] == 'axial':
                    self.axis_1.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'L', color=[1, 0, 0])

                elif (planeName[itr] == 'sagittal') and self.multiplaneFlag:
                    self.axis_2.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'L', color=[1, 0, 0])

            if 'right' in subplotNames[itr]:
                tmpIdx = subplotNames[itr].index('right')

                if planeName[itr] == 'axial':
                    self.axis_1.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'R', color=[1, 0, 0])

                elif (planeName[itr] == 'sagittal') and self.multiplaneFlag:
                    self.axis_2.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'R', color=[1, 0, 0])

            if 'anterior' in subplotNames[itr]:
                tmpIdx = subplotNames[itr].index('anterior')

                if planeName[itr] == 'axial':
                    self.axis_1.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'A', color=[1, 0, 0])

                if (planeName[itr] == 'coronal') and self.multiplaneFlag:
                    self.axis_3.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'A', color=[1, 0, 0])

            if 'posterior' in subplotNames[itr]:
                tmpIdx = subplotNames[itr].index('posterior')

                if planeName[itr] == 'axial':
                    self.axis_1.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'P', color=[1, 0, 0])
                elif (planeName[itr] == 'coronal') and self.multiplaneFlag:
                    self.axis_3.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'P', color=[1, 0, 0])

            if 'superior' in subplotNames[itr]:
                tmpIdx = subplotNames[itr].index('superior')

                if self.multiplaneFlag:
                    if planeName[itr] == 'sagittal':
                        self.axis_2.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'S', color=[1, 0, 0])
                    elif planeName[itr] == 'coronal':
                        self.axis_3.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'S', color=[1, 0, 0])

            if 'inferior' in subplotNames[itr]:
                tmpIdx = subplotNames[itr].index('inferior')

                if self.multiplaneFlag:
                    if planeName[itr] == 'sagittal':
                        self.axis_2.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'I', color=[1, 0, 0])
                    elif planeName[itr] == 'coronal':
                        self.axis_3.text(textPos[itr][tmpIdx].pos[0], textPos[itr][tmpIdx].pos[1], 'I', color=[1, 0, 0])
            if not multiplaneFlag:
                break

        argNames = []
        for argNameItr in range(len(args)):
            argNames.append('maskAnn'+str(argNameItr))

        if len(args)==0:
            self.axis_1.set_title('Visualization of sequence without annotations-axial plane')
            # self.axis_1.set_axis_off()
            if multiplaneFlag:
                self.axis_2.set_title('Visualization of sequence with annotation-sagittal plane')
                # self.axis_2.set_axis_off()
                self.axis_3.set_title('Visualization of sequence with annotation-coronal plane')
                # self.axis_3.set_axis_off()

        self.firstAxis, self.secondAxis, self.thirdAxis = self.volumeSeq.shape

        if len(args) == 0:
            fCheck = False

        if len(args)>0:
            self.maskAnn0 = args[0]

            if not(self.fillingFlag):
                self.maskAnn0 = contourOutlineInit(self.maskAnn0.copy(), False)
            self.maskAnn0 = np.ma.masked_where(self.maskAnn0 == 0, self.maskAnn0)

            self.firstAxisMask0, self.secondAxisMask0, self.thirdAxisMask0 = self.maskAnn0.shape
            fCheck = compareArrays(self.volumeSeq, self.maskAnn0)

            if len(args) == 2:
                self.maskAnn1 = args[1]

                if not (self.fillingFlag):
                    self.maskAnn1 = contourOutlineInit(self.maskAnn1.copy(), False)
                self.maskAnn1 = np.ma.masked_where(self.maskAnn1 == 0, self.maskAnn1)

                self.firstAxisMask1, self.secondAxisMask1, self.thirdAxisMask1 = self.maskAnn1.shape
                fCheck = compareArrays(self.volumeSeq, self.maskAnn1)

        # in case any of label(s) and sequence have different length - the sequence attribute will pass anyway
        self.idx_1 = self.thirdAxis//2  # starting from middle slice (will fail if you start from start/end)
        self.idx_2 = self.secondAxis//2
        self.idx_3 = self.firstAxis//2

        self.img1 = self.axis_1.imshow(np.rot90(self.volumeSeq[:, :, self.idx_1], k=self.rotNbr*3), cmap='gray')

        if self.multiplaneFlag:
            self.img2 = self.axis_2.imshow(np.rot90(self.volumeSeq[:, self.idx_2, :], k=self.rotNbr), cmap='gray')
            self.img3 = self.axis_3.imshow(np.rot90(self.volumeSeq[self.idx_3, :, :], k=self.rotNbr), cmap='gray')

        if not(self.fillingFlag):
            # values selected for enhanced vision on the contours
            alphaVal = 0.95
        else:
            alphaVal = 0.5

        if len(args)>0:
            self.mask0_axis1 = self.axis_1.imshow(np.rot90(self.maskAnn0[:, :, self.idx_1], k=self.rotNbr*3), cmap='Reds',
                                                  vmin=0, vmax=0.5, alpha=alphaVal, label='Test label #1')
            if len(args) == 2:
                self.mask1_axis1 = self.axis_1.imshow(np.rot90(self.maskAnn1[:, :, self.idx_1], k=self.rotNbr * 3),
                                                      cmap='Greens', vmin=0, vmax=0.5, alpha=alphaVal, label='Test label #1')

        if self.multiplaneFlag and len(args)>0:
            self.mask0_axis2 = self.axis_2.imshow(np.rot90(self.maskAnn0[:, self.idx_2, :], k=self.rotNbr), cmap='Reds',
                                                  vmin=0, vmax=0.5, alpha=alphaVal, label='Test label #1')
            self.mask0_axis3 = self.axis_3.imshow(np.rot90(self.maskAnn0[self.idx_3, :, :], k=self.rotNbr), cmap='Reds',
                                                  vmin=0, vmax=0.5, alpha=alphaVal, label='Test label #1')

            if len(args) == 2:
                self.mask1_axis2 = self.axis_2.imshow(np.rot90(self.maskAnn1[:, self.idx_2, :], k=self.rotNbr),
                                                      cmap='Greens',
                                                      vmin=0, vmax=0.5, alpha=alphaVal, label='Test label #1')
                self.mask1_axis3 = self.axis_3.imshow(np.rot90(self.maskAnn1[self.idx_3, :, :], k=self.rotNbr),
                                                      cmap='Greens',
                                                      vmin=0, vmax=0.5, alpha=alphaVal, label='Test label #1')

        self.updateSlice()

    def updateSlice(self):
        # update function for first and second dimension
        self.img1.set_data(np.rot90(self.volumeSeq[:, :, self.idx_1], k=self.rotNbr*3))
        # now is deactivated because of set axis off option
        self.axis_1.set_ylabel('Slice # %s' % self.idx_1)
        self.img1.axes.figure.canvas.draw()  # drawing the axes

        # Skimming through the labels
        if hasattr(self, 'mask0_axis1'):
            self.mask0_axis1.set_data(np.rot90(self.maskAnn0[:, :, self.idx_1], k=self.rotNbr*3))

        if hasattr(self, 'mask1_axis1'):
            self.mask1_axis1.set_data(np.rot90(self.maskAnn1[:, :, self.idx_1], k=self.rotNbr * 3))

        if self.multiplaneFlag:
            self.img2.set_data(np.rot90(self.volumeSeq[:, self.idx_2, :], k=self.rotNbr))
            self.img3.set_data(np.rot90(self.volumeSeq[self.idx_3, :, :], k=self.rotNbr))
            # now is deactivated because of set axis off option
            self.axis_2.set_ylabel('Slice # %s' % self.idx_2)
            self.img2.axes.figure.canvas.draw()  # drawing the axes
            # now is deactivated because of set axis off option
            self.axis_3.set_ylabel('Slice # %s' % self.idx_3)
            self.img3.axes.figure.canvas.draw()  # drawing the axes

            if hasattr(self, 'mask0_axis2'):
                self.mask0_axis2.set_data(np.rot90(self.maskAnn0[:, self.idx_2, :], k=self.rotNbr))
            if hasattr(self, 'mask0_axis3'):
                self.mask0_axis3.set_data(np.rot90(self.maskAnn0[self.idx_3, :, :], k=self.rotNbr))

            if hasattr(self, 'mask1_axis2'):
                self.mask1_axis2.set_data(np.rot90(self.maskAnn1[:, self.idx_2, :], k=self.rotNbr))

            if hasattr(self, 'mask1_axis3'):
                self.mask1_axis3.set_data(np.rot90(self.maskAnn1[self.idx_3, :, :], k=self.rotNbr))

    def scrollSlice(self, event):
        # update function for scrolling
        # Note: term 'event' is wildcard - do not change
        if event.button == 'up':  # scrolling forward
            self.idx_1 = (self.idx_1 + 1) % self.thirdAxis

            if hasattr(self, 'mask0_axis3'):
                self.idx_1 = (self.idx_1 + 1) % self.maskAnn0.shape[2]

            if hasattr(self, 'mask1_axis3'):
                self.idx_1 = (self.idx_1 + 1) % self.maskAnn1.shape[2]

            if self.multiplaneFlag:
                self.idx_2 = (self.idx_2 + 1) % self.secondAxis
                self.idx_3 = (self.idx_3 + 1) % self.firstAxis

                if hasattr(self, 'mask0_axis2'):
                    self.idx_2 = (self.idx_2 + 1) % self.maskAnn0.shape[1]
                if hasattr(self, 'mask0_axis1'):
                    self.idx_3 = (self.idx_3 + 1) % self.maskAnn0.shape[0]

                if hasattr(self, 'mask1_axis2'):
                    self.idx_2 = (self.idx_2 + 1) % self.maskAnn1.shape[1]
                if hasattr(self, 'mask1_axis1'):
                    self.idx_3 = (self.idx_3 + 1) % self.maskAnn1.shape[0]

        elif event.button == 'down':  # scrolling backward
            self.idx_1 = (self.idx_1 - 1) % self.thirdAxis

            if hasattr(self, 'mask0_axis3'):
                self.idx_1 = (self.idx_1 - 1) % self.maskAnn0.shape[2]

            if hasattr(self, 'mask1_axis3'):
                self.idx_1 = (self.idx_1 - 1) % self.maskAnn1.shape[2]

            if self.multiplaneFlag:
                self.idx_2 = (self.idx_2 - 1) % self.secondAxis
                self.idx_3 = (self.idx_3 - 1) % self.firstAxis

                if hasattr(self, 'mask0_axis2'):
                    self.idx_2 = (self.idx_2 - 1) % self.maskAnn0.shape[1]
                if hasattr(self, 'mask0_axis1'):
                    self.idx_3 = (self.idx_3 - 1) % self.maskAnn0.shape[0]

                if hasattr(self, 'mask1_axis2'):
                    self.idx_2 = (self.idx_2 - 1) % self.maskAnn1.shape[1]
                if hasattr(self, 'mask1_axis1'):
                    self.idx_3 = (self.idx_3 - 1) % self.maskAnn1.shape[0]

        self.updateSlice()

def createGifFasterV(volumePath: str, maskPath: str, dirToSaveGif: str):
    '''
    Function to create a GIF animation from 3D volume. The animation will be created in axial plane
    Parameters
    ----------
    volumePath: path to look for the volume
    maskPath: path to look for the mask
    dirToSaveGif: directory to save the GIF file
    Returns: True if successful or False if process is incomplete
    -------
    '''

    fileNameCheck = True

    if not isinstance(volumePath, str):
        raise AssertionError('The volume is not a path to file')
    else:
        volume = readNifti2Numpy(volumePath)

    if not isinstance(maskPath, str):
        raise AssertionError('The mask is not a path to file')
    else:
        mask = readNifti2Numpy(maskPath)

    volume = fixedSize(currentArray=volume.copy(), desiredSize=(256, 256, 64))
    mask = fixedSize(currentArray=mask.copy(), desiredSize=(256, 256, 64))

    firstAxisLimit, secondAxisLimit, thirdAxisLimit = np.nonzero(volume)

    # getting only slices with non-zero values
    croppedVolume = volume[firstAxisLimit.min(): firstAxisLimit.max(),
                            secondAxisLimit.min(): secondAxisLimit.max(),
                            thirdAxisLimit.min():thirdAxisLimit.max()]

    croppedMask = mask[firstAxisLimit.min(): firstAxisLimit.max(),
                            secondAxisLimit.min(): secondAxisLimit.max(),
                            thirdAxisLimit.min():thirdAxisLimit.max()]

    # plot the image
    fig, ax = plt.subplots(1, 1)
    slicesAxis = []

    for idxThirdAxis in range(croppedVolume.shape[2]):
        ax.imshow(np.rot90(croppedVolume[:, :, idxThirdAxis], 3), animated=True, cmap='gray')
        # ax.contour(np.rot90(croppedMask[:, :, idxThirdAxis], 3), colors='red', levels=[0.5])
        # ax.set_axis_off()

        slicesAxis.append([ax])

    animationVar = animation.ArtistAnimation(fig, slicesAxis, interval=50, blit=True, repeat_delay=100)

    if dirToSaveGif:
        if fileNameCheck:
            tempTextVar = volumePath.split('.nii.gz')
            animationVar.save(tempTextVar[0] + '.gif')
            print('Gif saved in', tempTextVar[0])
        else:
            animationVar.save(dirToSaveGif + '_exampleName.gif')

    else:
        animationVar.save('./exampleName.gif')

    return True

class compare3DVolumes:
    '''
    Visualizing volume into 2D slices.
    '''

    def __init__(self, getAxes: list, volume1: np.ndarray, volume2: np.ndarray):
        '''
        Parameters
        ----------
        getAxes: tuple of plt.axes, this needs to be declared in the object initialization
        volume1: volume #1 to be plotted, currently supports only nd.arrays
        volume2: volume #2 to be plotted, currently supports only nd.arrays
        '''

        if isinstance(getAxes, list):
            if not(isinstance(all(getAxes), matplotlib.axes._subplots.Axes)):
                raise AssertionError('One or more of the axes passed is not plt.axes type.')

        if not (isinstance(volume1, np.ndarray)):
            raise AssertionError('Volume #1 is not numpy array.')

        if not (isinstance(volume2, np.ndarray)):
            raise AssertionError('Volume #2 is not numpy array.')

        self.volume1 = np.array(volume1)
        self.volume2 = np.array(volume2)
        self.rotNbr = 3

        # always fixed planar view
        self.axis_1 = getAxes[0]
        self.axis_2 = getAxes[1]

        self.axis_1.set_title('Visualization of volume #1 without annotations-axial plane')
        self.axis_2.set_title('Visualization of volume #2 without annotations-axial plane')

        self.firstAxis1, self.secondAxis1, self.thirdAxis1 = self.volume1.shape
        self.firstAxis2, self.secondAxis2, self.thirdAxis2 = self.volume2.shape

        # in case any of label(s) and sequence have different length - the sequence attribute will pass anyway
        # volume 1
        self.idx_1_1 = self.thirdAxis1 //2  # starting from middle slice (will fail if you start from start/end)
        self.idx_2_1 = self.secondAxis1 //2
        self.idx_3_1 = self.firstAxis1 //2

        # volume 2
        self.idx_1_2 = self.thirdAxis2 // 2  # starting from middle slice (will fail if you start from start/end)
        self.idx_2_2 = self.secondAxis2 // 2
        self.idx_3_2 = self.firstAxis2 // 2

        # plotting volume 1
        self.img1 = self.axis_1.imshow(np.rot90(self.volume1[:, :, self.idx_1_1], k=self.rotNbr * 3), cmap='gray')
        self.img2 = self.axis_2.imshow(np.rot90(self.volume2[:, :, self.idx_1_2], k=self.rotNbr * 3), cmap='gray')

        self.updateSlice()

    def updateSlice(self):
        # update function for first and second dimension
        # volume #1
        self.img1.set_data(np.rot90(self.volume1[:, :, self.idx_1_1], k=self.rotNbr * 3))
        self.axis_1.set_ylabel('Slice # %s' % self.idx_1_1)
        self.img1.axes.figure.canvas.draw()  # drawing the axes

        # volume #2
        self.img2.set_data(np.rot90(self.volume2[:, :, self.idx_1_2], k=self.rotNbr * 3))
        self.axis_2.set_ylabel('Slice # %s' % self.idx_1_2)
        self.img2.axes.figure.canvas.draw()  # drawing the axes

    def scrollSlice(self, event):
        # update function for scrolling
        # Note: term 'event' is wildcard - do not change
        if event.button == 'up':  # scrolling forward
            self.idx_1_1 = (self.idx_1_1 + 1) % self.thirdAxis1
            self.idx_1_2 = (self.idx_1_2 + 1) % self.thirdAxis2

        elif event.button == 'down':  # scrolling backward
            self.idx_1_1 = (self.idx_1_1 - 1) % self.thirdAxis1
            self.idx_1_2 = (self.idx_1_2 - 1) % self.thirdAxis2

        self.updateSlice()

def visualExamplePairAndSave(data2plot, dir2Save, itr):
    title = ['Input', 'Overlay']

    fig, ax = plt.subplots(1, len(data2plot))

    if data2plot[0].ndim == 2:
        ax[0].imshow(np.rot90(data2plot[0], 4), cmap='gray')
        ax[0].set_title(title[0])
        ax[0].set_axis_off()

        ax[1].imshow(np.rot90(data2plot[0], 4), cmap='gray')
        ax[1].contour(np.rot90(data2plot[-1], 4), colors='red', levels=[0.5])
        ax[1].set_title(title[1])
        ax[1].set_axis_off()

    if data2plot[0].ndim == 3:
        ax[0].imshow(np.rot90(data2plot[0][:, :, 0], 4), cmap='gray')
        ax[0].set_title(title[0])
        ax[0].set_axis_off()

        ax[1].imshow(np.rot90(data2plot[0][:, :, 0], 4), cmap='gray')
        ax[1].contour(np.rot90(np.squeeze(data2plot[-1], axis=-1), 4), colors='red', levels=[0.5])
        ax[1].set_title(title[1])
        ax[1].set_axis_off()

    # plt.figure()
    nameTemp = os.path.join(dir2Save, str(itr) + '_example.png')
    plt.savefig(nameTemp, bbox_inches='tight')

def visualLabelAndPredictionMultiPatches(input1, input2, input3, target, prediction, dir2save, itr):
    title = ['Input', 'Input+GT', 'Input+Pred']

    nbrFigures = input1.shape[0]*3 # 3 for the 3 inputs

    data2plotSummary = np.array([input1, input2, input3])

    fig, ax = plt.subplots(6, 3)

    ax[0][0].set_title(title[0])
    ax[0][1].set_title(title[1])
    ax[0][2].set_title(title[2])

    ax[0][0].imshow(np.rot90(data2plotSummary[0][0]), cmap='gray')
    ax[0][0].set_axis_off()
    ax[0][1].imshow(np.rot90(data2plotSummary[0][0]), cmap='gray')
    ax[0][1].contour(np.rot90(target[0]), colors='red', levels=[0.5])
    ax[0][1].set_axis_off()
    ax[0][2].imshow(np.rot90(data2plotSummary[0][0]), cmap='gray')
    ax[0][2].contour(np.rot90(prediction[0][:, :, 0]), colors='green', levels=[0.5])
    ax[0][2].set_axis_off()

    ax[1][0].imshow(np.rot90(data2plotSummary[1][0]), cmap='gray')
    ax[1][0].set_axis_off()
    ax[1][1].imshow(np.rot90(data2plotSummary[1][0]), cmap='gray')
    ax[1][1].contour(np.rot90(target[0]), colors='red', levels=[0.5])
    ax[1][1].set_axis_off()
    ax[1][2].imshow(np.rot90(data2plotSummary[1][0]), cmap='gray')
    ax[1][2].contour(np.rot90(prediction[0][:, :, 1]), colors='green', levels=[0.5])
    ax[1][2].set_axis_off()

    ax[2][0].imshow(np.rot90(data2plotSummary[2][0]), cmap='gray')
    ax[2][0].set_axis_off()
    ax[2][1].imshow(np.rot90(data2plotSummary[2][0]), cmap='gray')
    ax[2][1].contour(np.rot90(target[0]), colors='red', levels=[0.5])
    ax[2][1].set_axis_off()
    ax[2][2].imshow(np.rot90(data2plotSummary[2][0]), cmap='gray')
    ax[2][2].contour(np.rot90(prediction[0][:, :, -1]), colors='green', levels=[0.5])
    ax[2][2].set_axis_off()
    ####################################################################################
    ax[3][0].imshow(np.rot90(data2plotSummary[0][1]), cmap='gray')
    ax[3][0].set_axis_off()
    ax[3][1].imshow(np.rot90(data2plotSummary[0][1]), cmap='gray')
    ax[3][1].contour(np.rot90(target[1]), colors='red', levels=[0.5])
    ax[3][1].set_axis_off()
    ax[3][2].imshow(np.rot90(data2plotSummary[0][1]), cmap='gray')
    ax[3][2].contour(np.rot90(prediction[1][:, :, 0]), colors='green', levels=[0.5])
    ax[3][2].set_axis_off()

    ax[4][0].imshow(np.rot90(data2plotSummary[1][1]), cmap='gray')
    ax[4][0].set_axis_off()
    ax[4][1].imshow(np.rot90(data2plotSummary[1][1]), cmap='gray')
    ax[4][1].contour(np.rot90(target[1]), colors='red', levels=[0.5])
    ax[4][1].set_axis_off()
    ax[4][2].imshow(np.rot90(data2plotSummary[1][1]), cmap='gray')
    ax[4][2].contour(np.rot90(prediction[1][:, :, 1]), colors='green', levels=[0.5])
    ax[4][2].set_axis_off()

    ax[5][0].imshow(np.rot90(data2plotSummary[2][1]), cmap='gray')
    ax[5][0].set_axis_off()
    ax[5][1].imshow(np.rot90(data2plotSummary[2][1]), cmap='gray')
    ax[5][1].contour(np.rot90(target[1]), colors='red', levels=[0.5])
    ax[5][1].set_axis_off()
    ax[5][2].imshow(np.rot90(data2plotSummary[2][1]), cmap='gray')
    ax[5][2].contour(np.rot90(prediction[1][:, :, -1]), colors='green', levels=[0.5])
    ax[5][2].set_axis_off()
    ##################################################################################################################

    # plt.show()
    nameTemp = os.path.join(dir2save, str(itr) + '_example.png')
    plt.savefig(nameTemp, bbox_inches='tight')

def visualLabelAndPredictionMulti(input1, input2, input3, target, prediction, dir2save, itr):
    title = ['Input', 'Input+GT', 'Input+Pred']

    nbrFigures = 3

    data2plotSummary = [input1, input2, input3]

    fig, ax = plt.subplots(nbrFigures, 3)

    for itrPatches in range(nbrFigures):
        ax[itrPatches][0].imshow(np.rot90(data2plotSummary[itrPatches]), cmap='gray')
        if itrPatches == 0:
            ax[itrPatches][0].set_title(title[0])
        ax[itrPatches][0].set_axis_off()

        ax[itrPatches][1].imshow(np.rot90(data2plotSummary[itrPatches]), cmap='gray')
        ax[itrPatches][1].contour(np.rot90(target), colors='red', levels=[0.5])
        if itrPatches == 0:
            ax[itrPatches][1].set_title(title[1])
        ax[itrPatches][1].set_axis_off()

        ax[itrPatches][2].imshow(np.rot90(data2plotSummary[itrPatches]), cmap='gray')
        ax[itrPatches][2].contour(np.rot90(prediction), colors='green', levels=[0.5])
        if itrPatches == 0:
            ax[itrPatches][2].set_title(title[2])
        ax[itrPatches][2].set_axis_off()

    # plt.show()
    nameTemp = os.path.join(dir2save, str(itr) + '_example.png')
    plt.savefig(nameTemp, bbox_inches='tight')

def visualLabelAndPredictionPatches(patchesInput, patchesTarget, patchesPrediction, dir2save=None, itr=None):
    title = ['Input', 'Target', 'Prediction']
    visualSingle = False

    nbrPatches = patchesInput.shape[0]

    if nbrPatches > 4:
        nbrPatches = 4

    # if patchesInput.shape[0] > 4:
    #     # this means first axis does not store patches but is part of the image dimension
    #     patchesInput = np.expand_dims(patchesInput, axis=0)
    #     patchesTarget = np.expand_dims(patchesTarget, axis=0)
    #     patchesPrediction = np.expand_dims(patchesPrediction, axis=0)
    # data2plotSummary = [patchesInput[:, :, :, :, 0], patchesTarget[:, :, :, :, 0], patchesPrediction[:, :, :, :, 0]]

    # else:
    # assume single channel input
    if patchesInput.shape[0] == 1:
        nbrPatches = 1
        visualSingle = True
        patchesInput = np.squeeze(patchesInput, axis=0)

    if patchesTarget.shape[0] == 1:
        nbrPatches = 1
        visualSingle = True
        patchesTarget = np.squeeze(patchesTarget, axis=0)

    if patchesPrediction.shape[0] == 1:
        nbrPatches = 1
        visualSingle = True
        patchesPrediction = np.squeeze(patchesPrediction, axis=0)

    data2plotSummary = [patchesInput[:, :, :, 0], patchesTarget[:, :, :, 0], patchesPrediction[:, :, :, 0]]

    fig, ax = plt.subplots(nbrPatches, len(data2plotSummary))

    for itrPatches in range(nbrPatches):
        if visualSingle:
            ax[0].imshow(np.rot90(data2plotSummary[0][100]), cmap='gray')
        else:
            ax[itrPatches][0].imshow(np.rot90(data2plotSummary[0][itrPatches]), cmap='gray')

        if itrPatches == 0:
            if visualSingle:
                ax[0].set_title(title[0])
                ax[0].set_axis_off()
            else:
                ax[itrPatches][0].set_title(title[0])
                ax[itrPatches][0].set_axis_off()

        if visualSingle:
            ax[1].imshow(np.rot90(data2plotSummary[1][100]), cmap='gray')
        else:
            ax[itrPatches][1].imshow(np.rot90(data2plotSummary[1][itrPatches]), cmap='gray')

        if itrPatches == 0:
            if visualSingle:
                ax[1].set_title(title[1])
                ax[1].set_axis_off()
            else:
                ax[itrPatches][1].set_title(title[1])
                ax[itrPatches][1].set_axis_off()

        if visualSingle:
            ax[-1].imshow(np.rot90(data2plotSummary[-1][100]), cmap='gray')
        else:
            ax[itrPatches][-1].imshow(np.rot90(data2plotSummary[-1][itrPatches]), cmap='gray')

        if itrPatches == 0:
            if visualSingle:
                ax[-1].set_title(title[-1])
                ax[-1].set_axis_off()
            else:
                ax[itrPatches][-1].set_title(title[-1])
                ax[itrPatches][-1].set_axis_off()

    # plt.show()
    nameTemp = os.path.join(dir2save, str(itr) + '_example.png')
    plt.savefig(nameTemp, bbox_inches='tight')

def visualLabelAndPredictionPatchesGradCam(patchesInput, patchesPrediction, dir2save=None, itr=None):
    title = ['Input', 'GradCAM']

    nbrPatches = patchesInput.shape[0]

    if nbrPatches > 4:
        nbrPatches = 4

    data2plotSummary = [patchesInput[:nbrPatches], patchesPrediction[:nbrPatches]]

    fig, ax = plt.subplots(nbrPatches, len(data2plotSummary))

    for itrPatches in range(nbrPatches):
        ax[itrPatches][0].imshow(np.rot90(data2plotSummary[0][itrPatches]), cmap='gray')
        if itrPatches == 0:
            ax[itrPatches][0].set_title(title[0])
        ax[itrPatches][0].set_axis_off()

        ax[itrPatches][1].imshow(np.rot90(data2plotSummary[0][itrPatches]), cmap='gray')
        # ax[itrPatches][1].imshow(np.rot90(data2plotSummary[1][itrPatches]), cmap='gray')
        ax[itrPatches][1].contour(np.rot90(data2plotSummary[1][itrPatches]), colors='red', levels=[0.5])
        if itrPatches == 0:
            ax[itrPatches][1].set_title(title[1])
        ax[itrPatches][1].set_axis_off()

        ax[itrPatches][2].imshow(np.rot90(data2plotSummary[0][itrPatches]), cmap='gray')
        # ax[itrPatches][2].imshow(np.rot90(data2plotSummary[2][itrPatches]), cmap='gray')
        ax[itrPatches][2].contour(np.rot90(data2plotSummary[2][itrPatches]), colors='green', levels=[0.5])
        if itrPatches == 0:
            ax[itrPatches][2].set_title(title[2])
        ax[itrPatches][2].set_axis_off()

    # plt.show()
    nameTemp = os.path.join(dir2save, str(itr) + '_example.png')
    plt.savefig(nameTemp, bbox_inches='tight')

def visualLabelAndPredictionPatchesClass(patchesInput, patchesTarget, patchesPrediction, dir2save=None, itr=None):
    title = ['Input', 'Input+GT', 'Input+Pred']

    nbrPatches = patchesInput.shape[0]

    if nbrPatches > 4:
        nbrPatches = 4

    data2plotSummary = [patchesInput[:nbrPatches], patchesTarget[:nbrPatches], patchesPrediction[:nbrPatches]]

    fig, ax = plt.subplots(nbrPatches, len(data2plotSummary))

    for itrPatches in range(nbrPatches):
        ax[itrPatches][0].imshow(np.rot90(data2plotSummary[0][itrPatches]), cmap='gray')
        if itrPatches == 0:
            ax[itrPatches][0].set_title(title[0])
        ax[itrPatches][0].set_axis_off()

        if int(data2plotSummary[1][itrPatches]) == 0:
            ax[itrPatches][1].imshow(np.zeros_like(a=data2plotSummary[0][itrPatches]), cmap='gray')
        if int(data2plotSummary[1][itrPatches]) == 1:
            ax[itrPatches][1].imshow(np.ones_like(a=data2plotSummary[0][itrPatches]), cmap='gray')
        if itrPatches == 0:
            ax[itrPatches][1].set_title(title[1])
        ax[itrPatches][1].set_axis_off()

        if int(data2plotSummary[2][itrPatches]) == 0:
            ax[itrPatches][2].imshow(np.zeros_like(a=data2plotSummary[0][itrPatches]), cmap='gray')
        if int(data2plotSummary[1][itrPatches]) == 1:
            ax[itrPatches][2].imshow(np.ones_like(a=data2plotSummary[0][itrPatches]), cmap='gray')
        if itrPatches == 0:
            ax[itrPatches][2].set_title(title[2])
        ax[itrPatches][2].set_axis_off()

    # plt.show()
    nameTemp = os.path.join(dir2save, str(itr) + '_example.png')
    plt.savefig(nameTemp, bbox_inches='tight')

def nifti2image(input, target, dir2save, name):
    title = ['Input', 'Input+GT']

    if not os.path.exists(dir2save):
        dir2save = Path(dir2save)
        dir2save.mkdir(mode=0o751, parents=True, exist_ok=True)

    data2plotSummary = [input, target]
    nbrPatches = 1

    fig, ax = plt.subplots(nbrPatches, len(data2plotSummary))

    for itrPatches in range(nbrPatches):
        ax[0].imshow(np.rot90(data2plotSummary[0]), cmap='gray')
        if itrPatches == 0:
            ax[0].set_title(title[0])
        ax[0].set_axis_off()

        ax[1].imshow(np.rot90(data2plotSummary[0]), cmap='gray')
        ax[1].contour(np.rot90(data2plotSummary[1]), colors='red', levels=[0.5])
        if itrPatches == 0:
            ax[1].set_title(title[1])
        ax[1].set_axis_off()

    nameTemp = os.path.join(dir2save, name + '.png')
    plt.savefig(nameTemp, bbox_inches='tight')

def visualLabelAndPrediction(data2plotLabel, data2plotPrediction):
    title = ['Input', 'Input+GT', 'Input+Pred']

    data2plotSummary = data2plotLabel + data2plotPrediction

    hasMaskLabel = data2plotSummary[1].max() > 0
    hasMaskPred = data2plotSummary[2].max() > 0

    fig, ax = plt.subplots(1, len(data2plotSummary))

    ax[0].imshow(np.rot90(array_to_img(data2plotSummary[0])), cmap='gray')
    ax[0].set_title(title[0])
    ax[0].set_axis_off()

    ax[1].imshow(np.rot90(array_to_img(data2plotSummary[0])), cmap='gray')
    if hasMaskLabel:
        ax[1].contour(np.rot90(array_to_img(data2plotSummary[1])), colors='red', levels=[0.5])
    ax[1].set_title(title[1])
    ax[1].set_axis_off()

    ax[2].imshow(np.rot90(array_to_img(data2plotSummary[0])), cmap='gray')
    if hasMaskPred:
        ax[2].contour(np.rot90(array_to_img(data2plotSummary[2])), colors='green', levels=[0.5])
    ax[2].set_title(title[2])
    ax[2].set_axis_off()

    plt.show()

def visualDifferenceLabelAndPrediction(data2plotLabel, data2plotPrediction):
    title = ['Input', 'Input+GT', 'Input+Pred', 'Input+Diff']

    data2plotSummary = data2plotLabel + data2plotPrediction

    hasMaskLabel = data2plotSummary[1].max() > 0
    hasMaskPred = data2plotSummary[2].max() > 0
    data2plotSummary.append(np.abs(np.subtract(data2plotSummary[1], data2plotSummary[2])))

    fig, ax = plt.subplots(1, len(data2plotSummary))

    ax[0].imshow(np.rot90(array_to_img(data2plotSummary[0])), cmap='gray')
    ax[0].set_title(title[0])
    ax[0].set_axis_off()

    ax[1].imshow(np.rot90(array_to_img(data2plotSummary[0])), cmap='gray')
    if hasMaskLabel:
        ax[1].contour(np.rot90(array_to_img(data2plotSummary[1])), colors='red', levels=[0.5])
    ax[1].set_title(title[1])
    ax[1].set_axis_off()

    ax[2].imshow(np.rot90(array_to_img(data2plotSummary[0])), cmap='gray')
    if hasMaskPred:
        ax[2].contour(np.rot90(array_to_img(data2plotSummary[2])), colors='green', levels=[0.5])
    ax[2].set_title(title[2])
    ax[2].set_axis_off()

    ax[3].imshow(np.rot90(array_to_img(data2plotSummary[0])), cmap='gray')
    ax[3].contour(np.rot90(array_to_img(data2plotSummary[-1])), colors='yellow', levels=[0.5])
    ax[3].set_title(title[3])
    ax[3].set_axis_off()

    plt.show()

def visualizePreprocessing(data2plotLabel, voxelSize):
    titleXaxis = ['Original', 'Skull stripped', 'Intensity normalization', 'N4 Bias Field Correction']

    plt.style.use('dark_background')

    _, ax = plt.subplots(1, len(data2plotLabel))

    for itrAxis in range(len(ax)):
        im = ax[itrAxis].imshow(np.rot90(data2plotLabel[itrAxis][:, 128, :]), cmap='gray')
        ax[itrAxis].get_xaxis().set_ticks([])
        ax[itrAxis].get_yaxis().set_ticks([])
        ax[itrAxis].spines['top'].set_visible(False)
        ax[itrAxis].spines['right'].set_visible(False)
        ax[itrAxis].spines['bottom'].set_visible(False)
        ax[itrAxis].spines['left'].set_visible(False)
        ax[itrAxis].title.set_text(titleXaxis[itrAxis])
        plt.colorbar(im, ax=ax[itrAxis], fraction=0.046, pad=0.04)

    plt.show()

def wrapVisualizePreprocessing(rawDataPath: str, betDataPath: str, intNormDataPath: str, n4bDataPath: str, fixedSizeShape=(256, 256, 64)):
    rawData, voxelSizeData = readNifti2NumpyAndHeader(rawDataPath)
    betData, _ = readNifti2NumpyAndHeader(betDataPath)
    intNormData, _ = readNifti2NumpyAndHeader(intNormDataPath)
    n4bData, _ = readNifti2NumpyAndHeader(n4bDataPath)

    rawData, _ = fixedSize(rawData.copy(), fixedSizeShape)
    betData, _ = fixedSize(betData.copy(), fixedSizeShape)
    intNormData, _ = fixedSize(intNormData.copy(), fixedSizeShape)
    n4bData, _ = fixedSize(n4bData.copy(), fixedSizeShape)

    dataLst = [rawData, betData, intNormData, n4bData]
    visualizePreprocessing(data2plotLabel=dataLst, voxelSize=voxelSizeData)
